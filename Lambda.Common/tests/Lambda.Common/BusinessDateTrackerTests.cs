using Amazon.DynamoDBv2;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Xunit;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Brink.DataLake.InStoreData.Lambda.Common.IntegrationTests
{
    public class BusinessDateTrackerTests
    {
        private readonly BusinessDateTracker _classUnderTest;

        public BusinessDateTrackerTests(ITestOutputHelper outputHelper)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            var awsOptions = configuration.GetAWSOptions();

            var serviceProvider = new ServiceCollection()
                .AddDefaultAWSOptions(awsOptions)
                .AddAWSService<IAmazonDynamoDB>()
                .AddLogging(i => i.AddProvider(new XunitLoggerProvider(outputHelper)))
                .Configure<BusinessDateTrackerOptions>(i => i.TableName = configuration["AWS:TableName"])
                .AddSingleton<IBusinessDateTracker, BusinessDateTracker>()
                .BuildServiceProvider();

            _classUnderTest = (BusinessDateTracker)serviceProvider.GetRequiredService<IBusinessDateTracker>();
        }

        [Fact]
        public async Task TrackingValuesShouldRoundTrip()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;
            var now = DateTimeOffset.Now;
            await _classUnderTest.UpdateLocationBusinessDateTrackingAsync(locationId, businessDate, now);

            // Act
            var locationBusinessDates = await _classUnderTest.GetLocationBusinessDateTrackingAsync(now.AddMinutes(-1));

            // Assert
            var matches = locationBusinessDates.Where(i => i.LocationId == locationId).ToArray();
            Assert.Single(matches);
            Assert.Equal(businessDate, matches[0].BusinessDate);
        }

        [Fact]
        public async Task MultipleUpdatesForSameBusinessDate_ShoudlResultInOneReturnedValue()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;
            var now = DateTimeOffset.Now;
            await _classUnderTest.UpdateLocationBusinessDateTrackingAsync(locationId, businessDate, now);
            await _classUnderTest.UpdateLocationBusinessDateTrackingAsync(locationId, businessDate, now.AddMinutes(1));

            // Act
            var locationBusinessDates = await _classUnderTest.GetLocationBusinessDateTrackingAsync(now.AddMinutes(-1));

            // Assert
            var matches = locationBusinessDates.Where(i => i.LocationId == locationId).ToArray();
            Assert.Single(matches);
            Assert.Equal(businessDate, matches[0].BusinessDate);
        }

        [Fact]
        public async Task UpdatesForMultipleBusinessDates_ShoudlResultInMultipleReturnedValue()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate1 = DateTime.Now.Date;
            var businessDate2 = DateTime.Now.Date.AddDays(1);
            var now = DateTimeOffset.Now;
            await _classUnderTest.UpdateLocationBusinessDateTrackingAsync(locationId, businessDate1, now);
            await _classUnderTest.UpdateLocationBusinessDateTrackingAsync(locationId, businessDate2, now);

            // Act
            var locationBusinessDates = await _classUnderTest.GetLocationBusinessDateTrackingAsync(now.AddMinutes(-1));

            // Assert
            var matches = locationBusinessDates.Where(i => i.LocationId == locationId).ToArray();
            Assert.Equal(2, matches.Length);
            Assert.Single(matches, i => i.BusinessDate == businessDate1);
            Assert.Single(matches, i => i.BusinessDate == businessDate2);
        }
    }
}
