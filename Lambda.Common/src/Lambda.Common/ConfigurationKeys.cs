﻿namespace Brink.DataLake.InStoreData.Lambda.Common
{
    public static class ConfigurationKeys
    {
        public const string CuratedBucketName = "CuratedBucketName";

        public const string DynamoDbTableName = "DynamoDbTableName";

        public const string IngestBucketName = "IngestBucketName";

        public const string Tier2CommandQueueName = "Tier2CommandQueueName";
    }
}
