﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.Common.Services
{
    public class LocationBusinessDate
    {
        public DateTime BusinessDate { get; set; }

        public Guid LocationId { get; set; }
    }
}
