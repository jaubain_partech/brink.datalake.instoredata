﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.Common.Services
{
    public interface IBusinessDateTracker
    {
        Task<IList<LocationBusinessDate>> GetLocationBusinessDateTrackingAsync(DateTimeOffset minLastSeenTime);

        Task UpdateLocationBusinessDateTrackingAsync(Guid locationId, DateTime businessDate,
            DateTimeOffset lastSeenTime);
    }
}
