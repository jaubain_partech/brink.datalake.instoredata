﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.Lambda.Common.Services.DynamoDbConstants;

namespace Brink.DataLake.InStoreData.Lambda.Common.Services
{
    public class BusinessDateTracker : DynamoRepository, IBusinessDateTracker
    {
        private const string LastSeenAttribute = "last-seen";

        private BusinessDateTrackerOptions _options;

        protected override string TableName => _options.TableName;

        public BusinessDateTracker(IOptions<BusinessDateTrackerOptions> options, IAmazonDynamoDB dynamoDb,
            ILogger<BusinessDateTracker> logger)
            : base(dynamoDb, logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        private Dictionary<string, AttributeValue> GetKeyDictionary(Guid locationId, DateTime businessDate)
        {
            return GetKeyDictionary($"BusinessDateTracker_{locationId:N}", businessDate.ToString("yyyy-MM-dd"));
        }

        private Dictionary<string, AttributeValue> GetKeyDictionary(Guid locationId, DateTime businessDate,
            DateTimeOffset modifiedTime)
        {
            return GetKeyDictionary("BusinessDateTracker", $"{modifiedTime:O}_{locationId:N}_{businessDate:yyyy-MM-dd}");
        }

        public async Task<IList<LocationBusinessDate>> GetLocationBusinessDateTrackingAsync(DateTimeOffset minLastSeenTime)
        {
            var keyConditionExpression = $"#pk = :pk AND #sk >= :lastSeen";

            var request = new QueryRequest
            {
                KeyConditionExpression = keyConditionExpression,
                ExpressionAttributeNames = new Dictionary<string, string>
                {
                    ["#pk"] = PartitionKey,
                    ["#sk"] = SortKey
                },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                {

                    [":lastSeen"] = new AttributeValue(minLastSeenTime.ToUniversalTime().ToString("O")),
                    [":pk"] = new AttributeValue("BusinessDateTracker")
                },
                TableName = TableName
            };

            var items = new List<Dictionary<string, AttributeValue>>();

            QueryResponse response;

            do
            {
                response = await DynamoDb.QueryAsync(request);

                items.AddRange(response.Items);

                request.ExclusiveStartKey = response.LastEvaluatedKey;

            }
            while (response.LastEvaluatedKey.Count != 0);

            var locationBusinessDateLookup = new Dictionary<string, LocationBusinessDate>();

            foreach (var item in items)
            {
                var tokens = item[SortKey].S.Split(new[] { '_' }, 3);
                var locationId = Guid.Parse(tokens[1]);
                var businessDate = DateTime.Parse(tokens[2]);
                var lookupKey = $"{tokens[1]}_{tokens[2]}";

                if (!locationBusinessDateLookup.TryGetValue(lookupKey, out var locationBusinessDate))
                {
                    locationBusinessDate = new LocationBusinessDate
                    {
                        BusinessDate = businessDate,
                        LocationId = locationId

                    };

                    locationBusinessDateLookup[lookupKey] = locationBusinessDate;
                }
            }

            return locationBusinessDateLookup.Values.ToList();
        }

        public async Task UpdateLocationBusinessDateTrackingAsync(Guid locationId, DateTime businessDate,
            DateTimeOffset lastSeenTime)
        {
            lastSeenTime = lastSeenTime.ToUniversalTime();

            var item1 = GetKeyDictionary(locationId, businessDate);
            item1[LastSeenAttribute] = new AttributeValue(lastSeenTime.ToString("O"));

            var item2 = GetKeyDictionary(locationId, businessDate, lastSeenTime);

            var request = new TransactWriteItemsRequest
            {
                TransactItems = new List<TransactWriteItem>
                {
                    new TransactWriteItem
                    {
                        Put = new Put
                        {
                            Item = item1,
                            TableName = TableName
                        }
                    },
                    new TransactWriteItem
                    {
                        Put = new Put
                        {
                            Item = item2,
                            TableName = TableName
                        }
                    }
                }
            };

            await DynamoDb.TransactWriteItemsAsync(request);
        }
    }
}
