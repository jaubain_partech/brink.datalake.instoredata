﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.Lambda.Common.Services.DynamoDbConstants;

namespace Brink.DataLake.InStoreData.Lambda.Common.Services
{
    public abstract class DynamoRepository
    {
        protected IAmazonDynamoDB DynamoDb { get; }

        protected ILogger Logger { get; }

        protected abstract string TableName { get; }

        protected DynamoRepository(IAmazonDynamoDB dynamoDb, ILogger logger)
        {
            DynamoDb = dynamoDb ?? throw new ArgumentNullException(nameof(dynamoDb));
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected async Task<Dictionary<string, AttributeValue>> GetItemAsync(Dictionary<string, AttributeValue> key)
        {
            var response = await DynamoDb.GetItemAsync(new GetItemRequest(TableName, key));

            return response.IsItemSet ? response.Item : null;
        }

        protected Dictionary<string, AttributeValue> GetKeyDictionary(string partitionKey, string sortKey)
        {
            return new Dictionary<string, AttributeValue>
            {
                [PartitionKey] = new AttributeValue(partitionKey),
                [SortKey] = new AttributeValue(sortKey)
            };
        }
    }
}
