﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.Common.Services
{
    public static class DynamoDbConstants
    {
        public const string PartitionKey = "partition-key";

        public const string SortKey = "sort-key";
    }
}
