﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.Common.Services
{
    public interface ITier2KeyPrefixProvider
    {
        string GetKeyPrefix(Guid locationId, DateTime businessDate);
    }
}
