﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.Common.Services
{
    public class Tier2KeyPrefixProvider : ITier2KeyPrefixProvider
    {
        public string GetKeyPrefix(Guid locationId, DateTime businessDate)
        {
            return $"tier2/{locationId:N}/{businessDate:yyyy-MM-dd}";
        }
    }
}
