﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.Common
{
    public class ProcessLocationDataCommand
    {
        public DateTime BusinessDate { get; set; }

        public Guid LocationId { get; set; }
    }
}
