using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Brink.Aws.Lambda;
using Brink.DataLake.InStoreData.Lambda.IngestProcessor;
using Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services;
using Brink.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Brink.DataLake.Lambda
{
    public class Function
    {
        public async Task FunctionHandler(S3Event @event, ILambdaContext context)
        {
            var entity = @event.Records?[0].S3;

            if (entity == null)
            {
                return;
            }

            try
            {
                var host = await LambdaHost.GetOrCreateAsync(
                    () => LambdaHost.CreateBuilder().UseStartup<Startup>().BuildAsync(context));

                host.SetContext(context);

                host.ServiceProvider.GetRequiredService<IGlobalLoggerScope>()
                    .Add("ObjectKey", entity.Object.Key);

                var processor = host.ServiceProvider.GetRequiredService<IIngestObjectProcessor>();

                await processor.ProcessS3EntityAsync(entity);
            }
            catch (Exception e)
            {
                context.Logger.LogLine(e.ToString());
            }
        }
    }
}
