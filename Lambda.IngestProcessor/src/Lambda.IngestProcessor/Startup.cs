﻿using Amazon.DynamoDBv2;
using Amazon.S3;
using Brink.Aws.Lambda;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services;
using Brink.Extensions.Configuration.Bootstrap;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.Lambda.Common.ConfigurationKeys;


namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor
{
    public class Startup : ILambdaStartup
    {
        public void ConfigureServices(IServiceCollection services, BootstrapConfiguration bootstrapConfiguration,
            IConfiguration configuration)
        {
            services
                .Configure<BusinessDateTrackerOptions>(i => i.TableName = configuration[DynamoDbTableName])
                .PostConfigure<BusinessDateTrackerOptions>(i => i.Validate())
                .AddAWSService<IAmazonDynamoDB>()
                .AddAWSService<IAmazonS3>()
                .AddSingleton<IBusinessDateTracker, BusinessDateTracker>()
                .AddSingleton<IIngestObjectProcessor, IngestObjectProcessor>()
                .AddSingleton<ITier2KeyPrefixProvider, Tier2KeyPrefixProvider>()
                .AddSingleton<IIngestObjectParser, IngestObjectParser>()
                .AddSingleton<ILocationBusinessDateObjectBuilder, LocationBusinessDateObjectBuilder>()
                .AddSingleton<ILocationBusinessDateObjectWriter, LocationBusinessDateObjectWriter>();
        }

        public async Task<IDictionary<string, string>> GetAdditionalConfigurationAsync(LambdaStartupConfigurationContext context)
        {
            var parameters = new ParameterStoreConfigurationParameters()
                .AddConfigurationKey(context.EnvironmentPrefix, DynamoDbTableName);

            return await context.ParameterStoreConfigurationProvider.GetConfigurationAsync(parameters);
        }
    }
}
