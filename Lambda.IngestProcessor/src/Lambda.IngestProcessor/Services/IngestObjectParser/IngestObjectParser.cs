﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static Amazon.S3.Util.S3EventNotification;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public class IngestObjectParser : IIngestObjectParser
    {
        private readonly ILogger<IngestObjectParser> _logger;
        private readonly IAmazonS3 _s3;

        public IngestObjectParser(IAmazonS3 s3, ILogger<IngestObjectParser> logger)
        {
            _s3 = s3 ?? throw new ArgumentNullException(nameof(s3));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IList<IngestObjectEntry>> ParseObjectAsync(S3Entity entity)
        {
            var request = new GetObjectRequest
            {
                BucketName = entity.Bucket.Name,
                Key = entity.Object.Key
            };

            var response = await _s3.GetObjectAsync(request);

            var entries = new List<IngestObjectEntry>();

            using (response.ResponseStream)
            using (var reader = new StreamReader(response.ResponseStream))
            {
                string line = null;
                int lineCount = 0;

                do
                {
                    line = await reader.ReadLineAsync();

                    if (line != null)
                    {
                        lineCount++;

                        if (TryParseLine(line, lineCount, out var entry))
                        {
                            entries.Add(entry);
                        }
                    }

                }
                while (line != null);
            }

            _logger.LogInformation($"Successfully parsed {entries.Count} lines.");

            return entries;
        }

        /// <remarks>
        /// Minimum viable line consists of a Guid, date, date time offset, and data - delimited by comma
        /// Example: e2c3cc3143034bdaa087a9e63e47ffc4,2019-01-01,2019-05-14T17:05:19.3704382+09:00,1
        /// </remarks>
        public bool TryParseLine(string line, int lineNumber, out IngestObjectEntry entry)
        {
            entry = null;

            var span = line.AsSpan();

            if (span.Length < 79)
            {
                _logger.LogError($"Line does not meet minimum line length (line {lineNumber}).");

                return false;
            }

            try
            {
                if (!Guid.TryParse(span.Slice(0, 32), out var locationId))
                {
                    _logger.LogError($"Failed to parse location ID (line {lineNumber}).");

                    return false;
                }

                if (!DateTime.TryParse(span.Slice(33, 10), out var businessDate))
                {
                    _logger.LogError($"Failed to parse business date (line {lineNumber}).");

                    return false;
                }

                if (!DateTimeOffset.TryParse(span.Slice(44, 33), out var createdTime))
                {
                    _logger.LogError($"Failed to parse created time (line {lineNumber}).");

                    return false;
                }

                entry = new IngestObjectEntry
                {
                    BusinessDate = businessDate,
                    CreatedTime = createdTime,
                    Data = span.Slice(78).ToString(),
                    LocationId = locationId
                };

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Failed to parse line (line {lineNumber}).");

                return false;
            }
        }
    }
}
