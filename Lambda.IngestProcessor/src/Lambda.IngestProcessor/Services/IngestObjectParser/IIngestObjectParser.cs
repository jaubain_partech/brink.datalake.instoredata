﻿using System.Collections.Generic;
using System.Threading.Tasks;
using static Amazon.S3.Util.S3EventNotification;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public interface IIngestObjectParser
    {
        Task<IList<IngestObjectEntry>> ParseObjectAsync(S3Entity entity);
    }
}
