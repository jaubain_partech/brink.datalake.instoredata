﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public class IngestObjectEntry
    {
        public DateTime BusinessDate { get; set; }

        public DateTimeOffset CreatedTime { get; set; }

        public string Data { get; set; }

        public Guid LocationId { get; set; }
    }
}
