﻿using System.Threading.Tasks;
using static Amazon.S3.Util.S3EventNotification;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public interface IIngestObjectProcessor
    {
        Task ProcessS3EntityAsync(S3Entity entity);
    }
}
