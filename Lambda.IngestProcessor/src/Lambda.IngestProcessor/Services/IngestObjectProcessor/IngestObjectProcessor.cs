﻿using Amazon.S3.Util;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public class IngestObjectProcessor : IIngestObjectProcessor
    {
        private readonly IBusinessDateTracker _businessDateTracker;
        private readonly ILogger<IngestObjectProcessor> _logger;
        private readonly ILocationBusinessDateObjectBuilder _objectBuilder;
        private readonly IIngestObjectParser _objectParser;
        private readonly ILocationBusinessDateObjectWriter _objectWriter;

        public IngestObjectProcessor(ILocationBusinessDateObjectBuilder objectBuilder,
            IIngestObjectParser objectParser, ILocationBusinessDateObjectWriter objectWriter,
            IBusinessDateTracker businessDateTracker, ILogger<IngestObjectProcessor> logger)
        {
            _objectBuilder = objectBuilder ?? throw new ArgumentNullException(nameof(objectBuilder));
            _objectParser = objectParser ?? throw new ArgumentNullException(nameof(objectParser));
            _objectWriter = objectWriter ?? throw new ArgumentNullException(nameof(objectWriter));
            _businessDateTracker = businessDateTracker ?? throw new ArgumentNullException(nameof(businessDateTracker));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task ProcessS3EntityAsync(S3EventNotification.S3Entity entity)
        {
            _logger.LogInformation("Object processing started.");

            try
            {
                var tier1ObjectEntries = await _objectParser.ParseObjectAsync(entity);

                if (tier1ObjectEntries.Count == 0)
                {
                    _logger.LogWarning("No entries were parsed from object.");

                    return;
                }

                var tier2Objects = _objectBuilder.BuildLocationBusinessDateObjects(tier1ObjectEntries);

                await _objectWriter.WriteObjectsAsync(entity.Bucket.Name, tier2Objects);

                var now = DateTimeOffset.Now;

                var trackerTasks = tier2Objects
                    .Select(i => _businessDateTracker.UpdateLocationBusinessDateTrackingAsync(i.LocationId, i.BusinessDate, now))
                    .ToArray();

                await Task.WhenAll(trackerTasks);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Unhandled exception processing object.");
            }
        }
    }
}
