﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public interface ILocationBusinessDateObjectWriter
    {
        Task WriteObjectsAsync(string bucketName, IList<LocationBusinessDateObject> objects);
    }
}
