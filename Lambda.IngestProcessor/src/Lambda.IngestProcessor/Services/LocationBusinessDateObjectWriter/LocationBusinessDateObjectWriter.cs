﻿using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public class LocationBusinessDateObjectWriter : ILocationBusinessDateObjectWriter
    {
        private readonly ILogger<LocationBusinessDateObjectWriter> _logger;
        private readonly IAmazonS3 _s3;
        private readonly ITier2KeyPrefixProvider _tier2KeyPrefixProvider;

        public LocationBusinessDateObjectWriter(IAmazonS3 s3, ITier2KeyPrefixProvider tier2KeyPrefixProvider,
            ILogger<LocationBusinessDateObjectWriter> logger)
        {
            _s3 = s3 ?? throw new ArgumentNullException(nameof(s3));
            _tier2KeyPrefixProvider = tier2KeyPrefixProvider
                ?? throw new ArgumentNullException(nameof(tier2KeyPrefixProvider));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task WriteObjectsAsync(string bucketName, IList<LocationBusinessDateObject> objects)
        {
            var tasks = new List<Task<PutObjectResponse>>();

            foreach (var @object in objects)
            {
                var key = _tier2KeyPrefixProvider.GetKeyPrefix(@object.LocationId, @object.BusinessDate) 
                    + $"/{Guid.NewGuid():N}";

                var request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = key,
                    InputStream = @object.Data
                };

                _logger.LogInformation($"Putting object {key}");

                tasks.Add(_s3.PutObjectAsync(request));
            }

            await Task.WhenAll(tasks);
        }
    }
}
