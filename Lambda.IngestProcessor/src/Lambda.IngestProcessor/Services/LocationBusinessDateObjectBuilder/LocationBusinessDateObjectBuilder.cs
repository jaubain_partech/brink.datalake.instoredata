﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public class LocationBusinessDateObjectBuilder : ILocationBusinessDateObjectBuilder
    {
        private readonly ILogger<LocationBusinessDateObjectBuilder> _logger;

        public LocationBusinessDateObjectBuilder(ILogger<LocationBusinessDateObjectBuilder> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public IList<LocationBusinessDateObject> BuildLocationBusinessDateObjects(IList<IngestObjectEntry> entries)
        {
            var objects = new List<LocationBusinessDateObject>();

            foreach (var locationGrouping in entries.GroupBy(i => i.LocationId))
            {
                foreach (var businessDateGrouping in locationGrouping.GroupBy(i => i.BusinessDate))
                {
                    var @object = new LocationBusinessDateObject
                    {
                        BusinessDate = businessDateGrouping.Key,
                        Data = new MemoryStream(),
                        LocationId = locationGrouping.Key
                    };

                    foreach (var entry in businessDateGrouping)
                    {
                        var bytes = Encoding.UTF8.GetBytes($"{entry.CreatedTime:O},{entry.Data}\n");

                        @object.Data.Write(bytes, 0, bytes.Length);
                    }

                    @object.Data.Position = 0;

                    objects.Add(@object);
                }
            }

            _logger.LogInformation($"Created {objects.Count} tier 2 objects.");

            return objects;
        }
    }
}
