﻿using System;
using System.IO;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public class LocationBusinessDateObject
    {
        public DateTime BusinessDate { get; set; }

        public MemoryStream Data { get; set; }

        public Guid LocationId { get; set; }
    }
}
