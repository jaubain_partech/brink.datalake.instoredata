﻿using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services
{
    public interface ILocationBusinessDateObjectBuilder
    {
        IList<LocationBusinessDateObject> BuildLocationBusinessDateObjects(IList<IngestObjectEntry> entries);
    }
}
