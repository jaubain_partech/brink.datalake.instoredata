﻿using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.UnitTests
{
    public class LocationBusinessDateObjectWriterTests
    {
        private readonly LocationBusinessDateObjectWriter _classUnderTest;

        private readonly Mock<ILogger<LocationBusinessDateObjectWriter>> _loggerMock;
        private readonly Mock<IAmazonS3> _s3Mock;
        private readonly Mock<ITier2KeyPrefixProvider> _tier2KeyPrefixProviderMock;

        public LocationBusinessDateObjectWriterTests()
        {
            _loggerMock = new Mock<ILogger<LocationBusinessDateObjectWriter>>(MockBehavior.Loose);
            _s3Mock = new Mock<IAmazonS3>(MockBehavior.Strict);
            _tier2KeyPrefixProviderMock = new Mock<ITier2KeyPrefixProvider>(MockBehavior.Strict);

            _classUnderTest = new LocationBusinessDateObjectWriter(_s3Mock.Object, _tier2KeyPrefixProviderMock.Object,
                _loggerMock.Object);
        }

        [Fact]
        public async Task WriteObjectsAsync_ShouldWriteObjectsToS3()
        {
            // Arrange
            var bucketName = "bucket";
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;

            var objects = new List<LocationBusinessDateObject>
            {
                new LocationBusinessDateObject
                {
                    BusinessDate = businessDate,
                    Data = new System.IO.MemoryStream(),
                    LocationId = locationId
                }
            };

            var prefix = "prefix";

            _tier2KeyPrefixProviderMock
                .Setup(i => i.GetKeyPrefix(locationId, businessDate))
                .Returns(prefix);

            _s3Mock
                .Setup(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == bucketName
                    && j.Key.StartsWith(prefix) && j.InputStream == objects[0].Data), default))
                .ReturnsAsync(new PutObjectResponse());

            // Act
            await _classUnderTest.WriteObjectsAsync(bucketName, objects);

            // Assert
            _tier2KeyPrefixProviderMock.Verify(i => i.GetKeyPrefix(locationId, businessDate), Times.Once);
            _s3Mock.Verify(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == bucketName
                && j.Key.StartsWith(prefix) && j.InputStream == objects[0].Data), default), Times.Once);
        }
    }
}
