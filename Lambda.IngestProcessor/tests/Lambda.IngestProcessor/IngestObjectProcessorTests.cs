﻿using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using static Amazon.S3.Util.S3EventNotification;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.UnitTests
{
    public class IngestObjectProcessorTests
    {
        private readonly IngestObjectProcessor _classUnderTest;

        private readonly Mock<IBusinessDateTracker> _businessDateTrackerMock;
        private readonly Mock<ILogger<IngestObjectProcessor>> _loggerMock;
        private readonly Mock<ILocationBusinessDateObjectBuilder> _objectBuilderMock;
        private readonly Mock<IIngestObjectParser> _objectParserMock;
        private readonly Mock<ILocationBusinessDateObjectWriter> _objectWriterMock;

        public IngestObjectProcessorTests()
        {
            _businessDateTrackerMock = new Mock<IBusinessDateTracker>(MockBehavior.Strict);
            _loggerMock = new Mock<ILogger<IngestObjectProcessor>>(MockBehavior.Loose);
            _objectBuilderMock = new Mock<ILocationBusinessDateObjectBuilder>(MockBehavior.Strict);
            _objectParserMock = new Mock<IIngestObjectParser>(MockBehavior.Strict);
            _objectWriterMock = new Mock<ILocationBusinessDateObjectWriter>(MockBehavior.Strict);

            _classUnderTest = new IngestObjectProcessor(_objectBuilderMock.Object, _objectParserMock.Object,
                _objectWriterMock.Object, _businessDateTrackerMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task ProcessS3EntityAsync_ShouldReturnEarly_WhenNoObjectsAreParsed()
        {
            // Arrange
            var entity = new S3Entity();

            _objectParserMock
                .Setup(i => i.ParseObjectAsync(entity))
                .ReturnsAsync(new List<IngestObjectEntry>());

            // Act
            await _classUnderTest.ProcessS3EntityAsync(entity);

            // Assert
            _objectParserMock.Verify(i => i.ParseObjectAsync(entity), Times.Once);
        }

        [Fact]
        public async Task ProcessS3EntityAsync_ShouldProcessEntity()
        {
            // Arrange
            var bucketName = "bucket";

            var entity = new S3Entity
            {
                Bucket = new S3BucketEntity
                {
                    Name = bucketName
                }
            };

            var tier1ObjectEntries = new List<IngestObjectEntry>
            {
                new IngestObjectEntry()
            };

            _objectParserMock
                .Setup(i => i.ParseObjectAsync(entity))
                .ReturnsAsync(tier1ObjectEntries);

            var tier2Objects = new List<LocationBusinessDateObject>
            {
                new LocationBusinessDateObject
                {
                    LocationId = Guid.NewGuid(),
                    BusinessDate = DateTime.Now.Date
                }
            };

            _objectBuilderMock
                .Setup(i => i.BuildLocationBusinessDateObjects(tier1ObjectEntries))
                .Returns(tier2Objects);

            _objectWriterMock
                .Setup(i => i.WriteObjectsAsync(bucketName, tier2Objects))
                .Returns(Task.CompletedTask);

            _businessDateTrackerMock
                .Setup(i => i.UpdateLocationBusinessDateTrackingAsync(tier2Objects[0].LocationId,
                    tier2Objects[0].BusinessDate, It.IsAny<DateTimeOffset>()))
                .Returns(Task.CompletedTask);

            // Act
            await _classUnderTest.ProcessS3EntityAsync(entity);

            // Assert
            _objectParserMock.Verify(i => i.ParseObjectAsync(entity), Times.Once);
            _objectBuilderMock.Verify(i => i.BuildLocationBusinessDateObjects(tier1ObjectEntries), Times.Once);
            _objectWriterMock.Verify(i => i.WriteObjectsAsync(bucketName, tier2Objects), Times.Once);
            _businessDateTrackerMock.Verify(i => i.UpdateLocationBusinessDateTrackingAsync(tier2Objects[0].LocationId,
                tier2Objects[0].BusinessDate, It.IsAny<DateTimeOffset>()), Times.Once);
        }

        [Fact]
        public async Task ProcessS3EntityAsync_ShouldCatchAndLogExceptions()
        {
            // Arrange
            var entity = new S3Entity();
            var exception = new Exception();

            _objectParserMock
                .Setup(i => i.ParseObjectAsync(entity))
                .Throws(exception);

            // Act
            await _classUnderTest.ProcessS3EntityAsync(entity);

            // Assert
            _objectParserMock.Verify(i => i.ParseObjectAsync(entity), Times.Once);
            _loggerMock.Verify(i => i.Log(LogLevel.Critical, 0, It.IsAny<object>(), exception,
                It.IsAny<Func<object, Exception, string>>()), Times.Once);
        }
    }
}
