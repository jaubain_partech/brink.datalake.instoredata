using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static Amazon.S3.Util.S3EventNotification;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.UnitTests
{
    public class IngestObjectParserTests
    {
        private readonly IngestObjectParser _classUnderTest;

        private readonly Mock<ILogger<IngestObjectParser>> _loggerMock;
        private readonly List<(LogLevel, string)> _logMessages;
        private readonly Mock<IAmazonS3> _s3Mock;

        public IngestObjectParserTests()
        {
            _loggerMock = new Mock<ILogger<IngestObjectParser>>(MockBehavior.Loose);

            _loggerMock
                .Setup(i => i.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(), It.IsAny<object>(), It.IsAny<Exception>(),
                    It.IsAny<Func<object, Exception, string>>()))
                .Callback((Action<LogLevel, EventId, object, Exception, Func<object, Exception, string>>)CaptureLogOutput);

            _logMessages = new List<(LogLevel, string)>();

            _s3Mock = new Mock<IAmazonS3>(MockBehavior.Strict);

            _classUnderTest = new IngestObjectParser(_s3Mock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task ParseObjectAsync_ShouldParseObject()
        {
            // Arrange
            var entity = new S3Entity
            {
                Bucket = new S3BucketEntity
                {
                    Name = "bucket"
                },
                Object = new S3ObjectEntity
                {
                    Key = "key"
                }
            };

            var lines = new string[]
            {
                $"{Guid.NewGuid():N},{DateTime.Now.Date:yyyy-MM-dd},{DateTimeOffset.Now:O},123",
                $"{Guid.NewGuid():N},{DateTime.Now.Date:yyyy-MM-dd},{DateTimeOffset.Now:O},456"
            };

            var response = new GetObjectResponse
            {
                ResponseStream = new MemoryStream(Encoding.UTF8.GetBytes(string.Join('\n', lines)))
            };

            _s3Mock
                .Setup(i => i.GetObjectAsync(It.Is<GetObjectRequest>(j => j.BucketName == entity.Bucket.Name 
                    && j.Key == entity.Object.Key), default))
                .ReturnsAsync(response);


            // Act
            var entries = await _classUnderTest.ParseObjectAsync(entity);

            // Assert
            Assert.Equal(lines.Length, entries.Count);
        }

        [Fact]
        public async Task ParseObjectAsync_ShouldIgnoreInvalidLines()
        {
            // Arrange
            var entity = new S3Entity
            {
                Bucket = new S3BucketEntity
                {
                    Name = "bucket"
                },
                Object = new S3ObjectEntity
                {
                    Key = "key"
                }
            };

            var lines = new string[]
            {
                $"{Guid.NewGuid():N},{DateTime.Now.Date:yyyy-MM-dd},{DateTimeOffset.Now:O},123",
                $"CLASSPATH: Invalid line!!"
            };

            var response = new GetObjectResponse
            {
                ResponseStream = new MemoryStream(Encoding.UTF8.GetBytes(string.Join('\n', lines)))
            };

            _s3Mock
                .Setup(i => i.GetObjectAsync(It.Is<GetObjectRequest>(j => j.BucketName == entity.Bucket.Name
                    && j.Key == entity.Object.Key), default))
                .ReturnsAsync(response);


            // Act
            var entries = await _classUnderTest.ParseObjectAsync(entity);

            // Assert
            Assert.Equal(1, entries.Count);
        }

        [Fact]
        public void TryParseLine_ShouldReturnFalse_WhenLocationIdCannotBeParsed()
        {
            // Arrange
            var line = "zzzzzzzzyyyyxxxxwwwwvvvvvvvvvvvv,2019-01-01,2019-01-01T00:00:00.0000000+00:00,DATA";

            // Act
            var result = _classUnderTest.TryParseLine(line, 0, out _);

            // Assert
            Assert.False(result);
            Assert.Single(_logMessages, i => i.Item1 == LogLevel.Error && i.Item2.StartsWith("Failed to parse location ID"));
        }

        [Fact]
        public void TryParseLine_ShouldReturnFalse_WhenBusinessDateCannotBeParsed()
        {
            // Arrange
            var line = $"{Guid.Empty:N},xxxx-yy-zz,2019-01-01T00:00:00.0000000+00:00,DATA";

            // Act
            var result = _classUnderTest.TryParseLine(line, 0, out _);

            // Assert
            Assert.False(result);
            Assert.Single(_logMessages, i => i.Item1 == LogLevel.Error && i.Item2.StartsWith("Failed to parse business date"));
        }

        [Fact]
        public void TryParseLine_ShouldReturnFalse_WhenCreatedTimeCannotBeParsed()
        {
            // Arrange
            var line = $"{Guid.Empty:N},2019-01-01,long-enough-but-100%-not-a-datetime-offset,DATA";

            // Act
            var result = _classUnderTest.TryParseLine(line, 0, out _);

            // Assert
            Assert.False(result);
            Assert.Single(_logMessages, i => i.Item1 == LogLevel.Error && i.Item2.StartsWith("Failed to parse created time"));
        }

        [Fact]
        public void TryParseLine_ShouldReturnFalse_WhenLineIsLessThan80Characters()
        {
            // Arrange
            var line = $"12345";

            // Act
            var result = _classUnderTest.TryParseLine(line, 0, out _);

            // Assert
            Assert.False(result);
            Assert.Single(_logMessages, i => i.Item1 == LogLevel.Error && i.Item2.StartsWith("Line does not meet minimum line length"));
        }

        [Fact]
        public void TryParseLine_ShouldReturnTrue_WhenLineIsParsedSuccessfully()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Parse("2019-01-01");
            var createdTime = DateTimeOffset.Now;
            var data = "1";
            var line = $"{locationId:N},{businessDate:yyyy-MM-dd},{createdTime:O},{data}";

            // Act
            var result = _classUnderTest.TryParseLine(line, 0, out var entry);

            // Assert
            Assert.True(result);
            Assert.Equal(locationId, entry.LocationId);
            Assert.Equal(businessDate, entry.BusinessDate);
            Assert.Equal(createdTime, entry.CreatedTime);
            Assert.Equal(data, entry.Data);
        }

        private void CaptureLogOutput(LogLevel logLevel, EventId eventId, object state, Exception exception,
            Func<object, Exception, string> formatter)
        {
            var message = formatter.Invoke(state, exception);

            _logMessages.Add((logLevel, message));
        }
    }
}
