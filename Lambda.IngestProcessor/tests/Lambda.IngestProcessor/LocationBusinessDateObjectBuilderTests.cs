﻿using Brink.DataLake.InStoreData.Lambda.IngestProcessor.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.IngestProcessor.UnitTests
{
    public class LocationBusinessDateObjectBuilderTests
    {
        private LocationBusinessDateObjectBuilder _classUnderTest;
        private Mock<ILogger<LocationBusinessDateObjectBuilder>> _loggerMock;

        public LocationBusinessDateObjectBuilderTests()
        {
            _loggerMock = new Mock<ILogger<LocationBusinessDateObjectBuilder>>(MockBehavior.Loose);

            _classUnderTest = new LocationBusinessDateObjectBuilder(_loggerMock.Object);
        }

        [Fact]
        public void BuildTier2Objects_ShouldBuildObjects()
        {
            // Arrange
            var startingBusinessDate = DateTime.Now.Date;
            var dayCount = 3;
            var locationIds = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            var createdTimes = new[] { DateTimeOffset.Now, DateTimeOffset.Now.AddMinutes(1), DateTimeOffset.Now.AddMinutes(2) };

            var entries = new List<IngestObjectEntry>();

            for (var index = 0; index < dayCount; index++)
            {
                var businessDate = startingBusinessDate.AddDays(index);

                foreach (var locationId in locationIds)
                {
                    foreach (var createdTime in createdTimes)
                    {
                        var tier1ObjectEntry = new IngestObjectEntry
                        {
                            BusinessDate = businessDate,
                            CreatedTime = createdTime,
                            Data = "12345",
                            LocationId = locationId
                        };

                        entries.Add(tier1ObjectEntry);
                    }
                }
            }

            // Act
            var tier2Objects = _classUnderTest.BuildLocationBusinessDateObjects(entries);

            // Assert
            var byLocationGroupings = tier2Objects.GroupBy(i => i.LocationId);

            foreach (var grouping in byLocationGroupings)
            {
                Assert.Equal(dayCount, grouping.Count());

                for (var index = 0; index < dayCount; index++)
                {
                    var businessDate = startingBusinessDate.AddDays(index);

                    Assert.Single(grouping.Where(i => i.BusinessDate == businessDate));
                }

                foreach (var @object in grouping)
                {
                    using (var streamReader = new StreamReader(@object.Data))
                    {
                        var lines = streamReader.ReadToEnd().Split('\n', StringSplitOptions.RemoveEmptyEntries);

                        Assert.Equal(createdTimes.Length, lines.Length);

                        var objectCreatedTimes = lines.Select(i => DateTimeOffset.Parse(i.Split(',')[0])).ToArray();

                        Assert.True(createdTimes.All(i => objectCreatedTimes.Contains(i)));
                    }
                }
            }
        }
    }
}
