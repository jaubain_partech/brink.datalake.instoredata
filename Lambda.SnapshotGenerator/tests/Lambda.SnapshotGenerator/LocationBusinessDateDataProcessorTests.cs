﻿using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class LocationBusinessDateDataProcessorTests
    {
        private readonly Services.SnapshotGenerator _classUnderTest;

        private readonly Mock<ILogger<Services.SnapshotGenerator>> _loggerMock;
        private readonly Mock<ISnapshotObjectBuilder> _objectBuilderMock;
        private readonly Mock<ILocationBusinessDateObjectLister> _objectListerMock;
        private readonly Mock<ILocationBusinessDateObjectTracker> _objectTrackerMock;
        private readonly Mock<ISnapshotObjectWriterFactory> _objectWriterFactoryMock;

        public LocationBusinessDateDataProcessorTests()
        {
            _loggerMock = new Mock<ILogger<Services.SnapshotGenerator>>(MockBehavior.Loose);
            _objectBuilderMock = new Mock<ISnapshotObjectBuilder>(MockBehavior.Strict);
            _objectListerMock = new Mock<ILocationBusinessDateObjectLister>(MockBehavior.Strict);
            _objectTrackerMock = new Mock<ILocationBusinessDateObjectTracker>(MockBehavior.Strict);
            _objectWriterFactoryMock = new Mock<ISnapshotObjectWriterFactory>(MockBehavior.Strict);

            _classUnderTest = new Services.SnapshotGenerator(_objectBuilderMock.Object, _objectListerMock.Object,
                _objectTrackerMock.Object, _objectWriterFactoryMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task ProcessLocationBusinessDateAsync_ShouldReturnEarly_WhenNoTier2ObjectsAreListed()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;

            _objectListerMock
                .Setup(i => i.GetObjectListAsync(locationId, businessDate))
                .ReturnsAsync(new List<S3Object>());

            // Act
            await _classUnderTest.ProcessLocationBusinessDateAsync(locationId, businessDate);

            // Assert
            _objectListerMock.Verify(i => i.GetObjectListAsync(locationId, businessDate), Times.Once);
        }

        [Fact]
        public async Task ProcessLocationBusinessDateAsync_ShouldReturnEarly_WhenNoTier3ObjectsAreBuilt()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;

            var s3Objects = new List<S3Object> { new S3Object() };

            _objectListerMock
                .Setup(i => i.GetObjectListAsync(locationId, businessDate))
                .ReturnsAsync(s3Objects);

            _objectBuilderMock
                .Setup(i => i.BuildTier3ObjectsAsync(s3Objects))
                .ReturnsAsync(new List<SnapshotObject>());

            // Act
            await _classUnderTest.ProcessLocationBusinessDateAsync(locationId, businessDate);

            // Assert
            _objectListerMock.Verify(i => i.GetObjectListAsync(locationId, businessDate), Times.Once);
            _objectBuilderMock.Verify(i => i.BuildTier3ObjectsAsync(s3Objects), Times.Once);
        }

        [Fact]
        public async Task ProcessLocationBusinessDateAsync_ShouldProcessLocationBusinessDate()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;

            var s3Objects = new List<S3Object> { new S3Object() };

            _objectListerMock
                .Setup(i => i.GetObjectListAsync(locationId, businessDate))
                .ReturnsAsync(s3Objects);

            var tier3Objects = new List<SnapshotObject>
            {
                new SnapshotObject { Type = "someType" }
            };

            _objectBuilderMock
                .Setup(i => i.BuildTier3ObjectsAsync(s3Objects))
                .ReturnsAsync(tier3Objects);

            var objectWriterMock = new Mock<ISnapshotObjectWriter>(MockBehavior.Strict);

            _objectWriterFactoryMock
                .Setup(i => i.CreateWriter(tier3Objects[0].Type))
                .Returns(objectWriterMock.Object);

            var key = $"{locationId:N}/{businessDate:yyyy-MM-dd}/{tier3Objects[0].Type}";

            objectWriterMock
                .Setup(i => i.WriteObjectAsync(key, tier3Objects[0]))
                .Returns(Task.CompletedTask);

            _objectTrackerMock
                .Setup(i => i.MarkObjectKeysAsProcessedAsync(locationId, businessDate,
                    It.Is<ISet<string>>(j => j.Single() == s3Objects.Single().Key)))
                .Returns(Task.CompletedTask);

            // Act
            await _classUnderTest.ProcessLocationBusinessDateAsync(locationId, businessDate);

            // Assert
            _objectListerMock.Verify(i => i.GetObjectListAsync(locationId, businessDate), Times.Once);
            _objectBuilderMock.Verify(i => i.BuildTier3ObjectsAsync(s3Objects), Times.Once);
            _objectWriterFactoryMock.Verify(i => i.CreateWriter(tier3Objects[0].Type), Times.Once);
            objectWriterMock.Verify(i => i.WriteObjectAsync(key, tier3Objects[0]), Times.Once);
            _objectTrackerMock.Verify(i => i.MarkObjectKeysAsProcessedAsync(locationId, businessDate,
                It.Is<ISet<string>>(j => j.Single() == s3Objects.Single().Key)), Times.Once);
        }

        [Fact]
        public async Task ProcessLocationBusinessDateAsync_ShouldCatchAndLogExceptions()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;
            var exception = new Exception();

            _objectListerMock
                .Setup(i => i.GetObjectListAsync(locationId, businessDate))
                .Throws(exception);

            // Act
            await _classUnderTest.ProcessLocationBusinessDateAsync(locationId, businessDate);

            // Assert
            _objectListerMock.Verify(i => i.GetObjectListAsync(locationId, businessDate), Times.Once);
            _loggerMock.Verify(i => i.Log(LogLevel.Critical, 0, It.IsAny<object>(), exception,
                It.IsAny<Func<object, Exception, string>>()), Times.Once);
        }
    }
}
