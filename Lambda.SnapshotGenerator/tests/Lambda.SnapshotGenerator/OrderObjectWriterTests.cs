﻿using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class OrderObjectWriterTests
    {
        private readonly OrderObjectWriter _classUnderTest;

        private readonly JsonSerializer _jsonSerializer;
        private readonly Mock<ILogger<OrderObjectWriter>> _loggerMock;
        private readonly SnapshotObjectWriterOptions _options;
        private readonly Mock<IAmazonS3> _s3Mock;

        public OrderObjectWriterTests()
        {
            _jsonSerializer = new JsonSerializer
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            _loggerMock = new Mock<ILogger<OrderObjectWriter>>(MockBehavior.Loose);
            _options = new SnapshotObjectWriterOptions { BucketName = "bucket" };
            _s3Mock = new Mock<IAmazonS3>(MockBehavior.Strict);

            _classUnderTest = new OrderObjectWriter(Options.Create(_options), _s3Mock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task WriteObjectAsync_ShouldWriteOrders()
        {
            // Arrange
            var key = "key";

            _s3Mock
                .Setup(i => i.GetObjectAsync(_options.BucketName, key, default))
                .Throws(new AmazonS3Exception("The specified key does not exist."));

            var orderStream = BuildOrderArrayStream(new[] { BuildOrder(1, DateTimeOffset.Now), BuildOrder(2, DateTimeOffset.Now) });
            var tier3Object = new SnapshotObject { Data = orderStream };
            MemoryStream putStream = null;

            _s3Mock
                .Setup(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                    && j.Key == key && j.InputStream != null), default))
                .Callback((Action<PutObjectRequest, CancellationToken>)((r, c) => { putStream = CopyPutStream(r); }))
                .ReturnsAsync(new PutObjectResponse());

            // Act
            await _classUnderTest.WriteObjectAsync(key, tier3Object);

            // Assert
            _s3Mock.Verify(i => i.GetObjectAsync(_options.BucketName, key, default), Times.Once);
            _s3Mock.Verify(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                && j.Key == key && j.InputStream != null), default), Times.Once);

            var orders = DeserializeOrders(putStream);

            Assert.Equal(2, orders.Length);
            Assert.Single(orders, i => i.Id == 1);
            Assert.Single(orders, i => i.Id == 2);
        }

        [Fact]
        public async Task WriteObjectAsync_ShouldWriteOrders_WhenOrdersObjectAlreadyExists()
        {
            // Arrange
            var key = "key";
            var existingOrderStream = BuildOrderArrayStream(new[] { BuildOrder(1, DateTimeOffset.Now) });
            var getObjectResponse = new GetObjectResponse { ResponseStream = existingOrderStream };

            _s3Mock
                .Setup(i => i.GetObjectAsync(_options.BucketName, key, default))
                .ReturnsAsync(getObjectResponse);

            var orderStream = BuildOrderArrayStream(new[] { BuildOrder(2, DateTimeOffset.Now), BuildOrder(3, DateTimeOffset.Now) });
            var tier3Object = new SnapshotObject { Data = orderStream };
            MemoryStream putStream = null;

            _s3Mock
                .Setup(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                    && j.Key == key && j.InputStream != null), default))
                .Callback((Action<PutObjectRequest, CancellationToken>)((r, c) => { putStream = CopyPutStream(r); }))
                .ReturnsAsync(new PutObjectResponse());

            // Act
            await _classUnderTest.WriteObjectAsync(key, tier3Object);

            // Assert
            _s3Mock.Verify(i => i.GetObjectAsync(_options.BucketName, key, default), Times.Once);
            _s3Mock.Verify(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                && j.Key == key && j.InputStream != null), default), Times.Once);

            var orders = DeserializeOrders(putStream);

            Assert.Equal(3, orders.Length);
            Assert.Single(orders, i => i.Id == 1);
            Assert.Single(orders, i => i.Id == 2);
            Assert.Single(orders, i => i.Id == 3);
        }

        [Fact]
        public async Task WriteObjectAsync_ShouldWriteNewestVersionOfOrder()
        {
            // Arrange
            var key = "key";

            _s3Mock
                .Setup(i => i.GetObjectAsync(_options.BucketName, key, default))
                .Throws(new AmazonS3Exception("The specified key does not exist."));

            var newestModifiedTime = DateTimeOffset.Now.AddMinutes(1);
            var orderStream = BuildOrderArrayStream(new[] { BuildOrder(1, DateTimeOffset.Now), BuildOrder(1, newestModifiedTime) });
            var tier3Object = new SnapshotObject { Data = orderStream };
            MemoryStream putStream = null;

            _s3Mock
                .Setup(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                    && j.Key == key && j.InputStream != null), default))
                .Callback((Action<PutObjectRequest, CancellationToken>)((r, c) => { putStream = CopyPutStream(r); }))
                .ReturnsAsync(new PutObjectResponse());

            // Act
            await _classUnderTest.WriteObjectAsync(key, tier3Object);

            // Assert
            _s3Mock.Verify(i => i.GetObjectAsync(_options.BucketName, key, default), Times.Once);
            _s3Mock.Verify(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                && j.Key == key && j.InputStream != null), default), Times.Once);

            var orders = DeserializeOrders(putStream);

            Assert.Single(orders);
            Assert.Single(orders, i => i.Id == 1 && i.ModifiedTime == newestModifiedTime);
        }

        private MemoryStream BuildOrderArrayStream(IEnumerable<JObject> orders)
        {
            var array = new JArray(orders.ToArray());

            var buffer = Encoding.UTF8.GetBytes(array.ToString());

            return new MemoryStream(buffer);
        }

        private JObject BuildOrder(long id, DateTimeOffset modifiedTime)
        {
            var order = new Order { Id = id, ModifiedTime = modifiedTime };

            return JObject.FromObject(order, _jsonSerializer);
        }

        private MemoryStream CopyPutStream(PutObjectRequest request)
        {
            var copy = new MemoryStream();

            request.InputStream.Position = 0;
            request.InputStream.CopyTo(copy);
            request.InputStream.Flush();

            copy.Position = 0;

            return copy;
        }

        private Order[] DeserializeOrders(Stream stream)
        {
            using (var streamReader = new StreamReader(stream))
            using (var jsonReader = new JsonTextReader(streamReader))
            {
                return _jsonSerializer.Deserialize<Order[]>(jsonReader);
            }
        }

        public class Order
        {
            public long Id { get; set; }

            public DateTimeOffset ModifiedTime { get; set; }
        }
    }
}
