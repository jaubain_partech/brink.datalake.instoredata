﻿using Amazon.S3;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class SnapshotObjectWriterTests
    {
        private readonly SnapshotObjectWriter _classUnderTest;

        private readonly SnapshotObjectWriterOptions _options;
        private readonly Mock<IAmazonS3> _s3Mock;

        public SnapshotObjectWriterTests()
        {
            _options = new SnapshotObjectWriterOptions { BucketName = "bucket" };
            _s3Mock = new Mock<IAmazonS3>(MockBehavior.Strict);

            _classUnderTest = new TestWriter(Options.Create(_options), _s3Mock.Object);
        }

        [Fact]
        public async Task WriteObjectAsync_ShouldThrow_WhenKeyIsInvalid()
        {
            // Act
            var exception = await Record.ExceptionAsync(() => _classUnderTest.WriteObjectAsync(null, new SnapshotObject()));

            // Assert
            Assert.IsType<ArgumentException>(exception);
        }

        [Fact]
        public async Task WriteObjectAsync_ShouldThrow_Tier3ObjectIsNull()
        {
            // Act
            var exception = await Record.ExceptionAsync(() => _classUnderTest.WriteObjectAsync("key", null));

            // Assert
            Assert.IsType<ArgumentNullException>(exception);
        }

        public class TestWriter : SnapshotObjectWriter
        {
            public TestWriter(IOptions<SnapshotObjectWriterOptions> options, IAmazonS3 s3) 
                : base(options, s3)
            {
            }

            protected override Task WriteObjectCoreAsync(string key, SnapshotObject @object)
            {
                throw new NotImplementedException();
            }
        }
    }
}
