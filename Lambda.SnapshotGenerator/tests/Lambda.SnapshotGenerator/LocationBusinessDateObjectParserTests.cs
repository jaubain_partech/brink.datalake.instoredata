﻿using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class LocationBusinessDateObjectParserTests
    {
        private readonly LocationBusinessDateObjectParser _classUnderTest;

        private readonly Mock<ILogger<LocationBusinessDateObjectParser>> _loggerMock;
        private readonly List<(LogLevel, string)> _logMessages;
        private readonly Mock<IAmazonS3> _s3Mock;

        public LocationBusinessDateObjectParserTests()
        {
            _loggerMock = new Mock<ILogger<LocationBusinessDateObjectParser>>(MockBehavior.Loose);
            _loggerMock
                .Setup(i => i.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(), It.IsAny<object>(), It.IsAny<Exception>(),
                    It.IsAny<Func<object, Exception, string>>()))
                .Callback((Action<LogLevel, EventId, object, Exception, Func<object, Exception, string>>)CaptureLogOutput);

            _logMessages = new List<(LogLevel, string)>();
            _s3Mock = new Mock<IAmazonS3>(MockBehavior.Strict);

            _classUnderTest = new LocationBusinessDateObjectParser(_s3Mock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task ParseObjectAsync_ShouldParseObject()
        {
            // Arrange
            var bucketName = "bucket";
            var key = "key";

            var s3Object = new S3Object
            {
                BucketName = bucketName,
                Key = key
            };

            var payload1 = new { Key1 = "Value1", Key2 = "Value2" };
            var payload2 = new { Key3 = "Value3", Key4 = "Value4" };
            var dataString = $"{DateTimeOffset.Now:O},{await BuildGetCompressedString(JsonConvert.SerializeObject(payload1))}\n"
                + $"{DateTimeOffset.Now:O},{await BuildGetCompressedString(JsonConvert.SerializeObject(payload2))}\n";

            var dataStream = new MemoryStream(Encoding.UTF8.GetBytes(dataString));

            _s3Mock
                .Setup(i => i.GetObjectStreamAsync(bucketName, key, null, default))
                .ReturnsAsync(dataStream);

            // Act
            var values = await _classUnderTest.ParseObjectAsync(s3Object);

            // Assert
            // Assert
            Assert.Equal(4, values.Count);
            Assert.Single(values, i => i.Name == "key1");
            Assert.Single(values, i => i.Name == "key2");
            Assert.Single(values, i => i.Name == "key3");
            Assert.Single(values, i => i.Name == "key4");
        }

        [Fact]
        public async Task TryParseLineAsync_ShouldReturnNull_WhenCreatedTimeCannotBeParsed()
        {
            // Arrange
            var line = "not-a-date-time-offset,data";

            // Act
            var values = await _classUnderTest.TryParseLineAsync(line, 1);

            // Assert
            Assert.Null(values);
            Assert.StartsWith("Failed to parse created time", _logMessages.Single().Item2);
        }

        [Fact]
        public async Task TryParseLineAsync_ShouldReturnNull_WhenDataTokenIsInvalid()
        {
            // Arrange
            var line = $"{DateTimeOffset.Now:O},";

            // Act
            var values = await _classUnderTest.TryParseLineAsync(line, 1);

            // Assert
            Assert.Null(values);
            Assert.StartsWith("Empty data value", _logMessages.Single().Item2);
        }

        [Fact]
        public async Task TryParseLineAsync_ShouldReturnNull_WhenDataIsNotGzipCompressed()
        {
            // Arrange
            var line = $"{DateTimeOffset.Now:O},not-gzipped";

            // Act
            var values = await _classUnderTest.TryParseLineAsync(line, 1);

            // Assert
            Assert.Null(values);
            Assert.StartsWith("Error decoding data", _logMessages.Single().Item2);
        }

        [Fact]
        public async Task TryParseLineAsync_ShouldReturnNull_WhenDataIsNotAJsonObject()
        {
            // Arrange
            var uncompressedData = "the-data";
            var compressedData = await BuildGetCompressedString(uncompressedData);

            var line = $"{DateTimeOffset.Now:O},{compressedData}";

            // Act
            var values = await _classUnderTest.TryParseLineAsync(line, 1);

            // Assert
            Assert.Null(values);
            Assert.StartsWith("Error parsing JSON object", _logMessages.Single().Item2);
        }

        [Fact]
        public async Task TryParseLineAsync_ShouldParseLine()
        {
            // Arrange
            var payload = new { Key1 = "Value1", Key2 = "Value2" };
            var uncompressedData = JsonConvert.SerializeObject(payload);
            var compressedData = await BuildGetCompressedString(uncompressedData);

            var line = $"{DateTimeOffset.Now:O},{compressedData}";

            // Act
            var values = await _classUnderTest.TryParseLineAsync(line, 1);

            // Assert
            Assert.Equal(2, values.Count);
            Assert.Single(values, i => i.Name == "key1");
            Assert.Single(values, i => i.Name == "key2");
        }

        private void CaptureLogOutput(LogLevel logLevel, EventId eventId, object state, Exception exception,
            Func<object, Exception, string> formatter)
        {
            var message = formatter.Invoke(state, exception);

            _logMessages.Add((logLevel, message));
        }

        private async Task<string> BuildGetCompressedString(string data)
        {
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
            {
                using (var outputStream = new MemoryStream())
                {
                    using (var gzipStream = new GZipStream(outputStream, CompressionMode.Compress, true))
                    {
                        memoryStream.Position = 0;

                        await memoryStream.CopyToAsync(gzipStream).ConfigureAwait(false);
                    }

                    return Convert.ToBase64String(outputStream.ToArray());
                }
            }
        }
    }
}
