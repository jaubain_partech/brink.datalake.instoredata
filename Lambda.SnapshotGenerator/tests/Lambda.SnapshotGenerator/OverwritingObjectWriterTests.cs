﻿using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class OverwritingObjectWriterTests
    {
        private readonly OverwritingObjectWriter _classUnderTest;

        private readonly Mock<ILogger<OverwritingObjectWriter>> _loggerMock;
        private readonly SnapshotObjectWriterOptions _options;
        private readonly Mock<IAmazonS3> _s3Mock;

        public OverwritingObjectWriterTests()
        {
            _loggerMock = new Mock<ILogger<OverwritingObjectWriter>>(MockBehavior.Loose);
            _options = new SnapshotObjectWriterOptions { BucketName = "bucket" };
            _s3Mock = new Mock<IAmazonS3>(MockBehavior.Strict);

            _classUnderTest = new OverwritingObjectWriter(Options.Create(_options), _s3Mock.Object,
                _loggerMock.Object);
        }

        [Fact]
        public async Task WriteObjectAsync_ShouldPutObject()
        {
            // Arrange
            var key = "key";
            var tier3Object = new SnapshotObject { Data = new MemoryStream() };

            _s3Mock
                .Setup(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                    && j.InputStream == tier3Object.Data && j.Key == key), default))
                .ReturnsAsync(new PutObjectResponse());

            // Act
            await _classUnderTest.WriteObjectAsync(key, tier3Object);

            // Assert
            _s3Mock.Verify(i => i.PutObjectAsync(It.Is<PutObjectRequest>(j => j.BucketName == _options.BucketName
                && j.InputStream == tier3Object.Data && j.Key == key), default), Times.Once);
        }
    }
}
