﻿using Amazon.S3;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class SnapshotObjectWriterFactoryTests
    {
        private readonly SnapshotObjectWriterFactory _classUnderTest;

        public SnapshotObjectWriterFactoryTests()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .Configure<SnapshotObjectWriterOptions>(i => i.BucketName = "bucket")
                .AddSingleton(new Mock<IAmazonS3>().Object)
                .AddTransient<OrderObjectWriter>()
                .AddTransient<OverwritingObjectWriter>()
                .BuildServiceProvider();

            _classUnderTest = new SnapshotObjectWriterFactory(serviceProvider);
        }

        [Fact]
        public void CreateWriter_ShouldReturnOrderWriter_WhenTypeEqualsOrders()
        {
            // Act
            var writer = _classUnderTest.CreateWriter(Constants.Orders);

            // Assert
            Assert.IsType<OrderObjectWriter>(writer);
        }

        [Fact]
        public void CreateWriter_ShouldReturnOverwritingWriter_WhenTypeIsNotEqualToOrders()
        {
            // Act
            var writer = _classUnderTest.CreateWriter("anything-but-orders");

            // Assert
            Assert.IsType<OverwritingObjectWriter>(writer);
        }

        [Fact]
        public void CreateWriter_ShouldThrow_WhenDataTypeIsNull()
        {
            // Act
            var exception = Record.Exception(() => _classUnderTest.CreateWriter(null));

            // Assert
            Assert.IsType<ArgumentException>(exception);
        }
    }
}
