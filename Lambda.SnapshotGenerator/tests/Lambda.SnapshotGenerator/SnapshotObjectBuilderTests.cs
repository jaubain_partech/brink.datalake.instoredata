﻿using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class SnapshotObjectBuilderTests
    {
        private readonly SnapshotObjectBuilder _classUnderTest;

        private readonly Mock<ILogger<SnapshotObjectBuilder>> _loggerMock;
        private readonly Mock<ILocationBusinessDateObjectParser> _objectParserMock;

        public SnapshotObjectBuilderTests()
        {
            _loggerMock = new Mock<ILogger<SnapshotObjectBuilder>>(MockBehavior.Loose);
            _objectParserMock = new Mock<ILocationBusinessDateObjectParser>(MockBehavior.Strict);

            _classUnderTest = new SnapshotObjectBuilder(_objectParserMock.Object, _loggerMock.Object);
        }

        [Fact]
        public void BuildTier3ObjectsFromParsedValues_ShouldSelectNewestValueForNonOrders()
        {
            // Arrange
            var parsedValues = new List<ParsedValue>
            {
                new ParsedValue("data", JToken.Parse("123"), DateTimeOffset.Now),
                new ParsedValue("data", JToken.Parse("456"), DateTimeOffset.Now.AddMinutes(1)),
            };

            // Act
            var tier3Objects = _classUnderTest.BuildTier3ObjectsFromParsedValues(parsedValues);

            // Assert
            Assert.Single(tier3Objects);
            Assert.Equal(parsedValues[1].Name, tier3Objects[0].Type);
            Assert.Equal(parsedValues[1].CreatedTime, tier3Objects[0].CreatedTime);
            Assert.Equal("456", Encoding.UTF8.GetString(tier3Objects[0].Data.ToArray()));
        }

        [Fact]
        public void BuildTier3ObjectsFromParsedValues_ShouldConcatenateOrders()
        {
            // Arrange
            var parsedValues = new List<ParsedValue>
            {
                new ParsedValue(Constants.Orders, JToken.Parse("[1,2,3]"), DateTimeOffset.Now),
                new ParsedValue(Constants.Orders, JToken.Parse("[4,5,6]"), DateTimeOffset.Now.AddMinutes(1)),
            };

            // Act
            var tier3Objects = _classUnderTest.BuildTier3ObjectsFromParsedValues(parsedValues);

            // Assert
            Assert.Single(tier3Objects);
            Assert.Equal(Constants.Orders, tier3Objects[0].Type);
            Assert.Equal(parsedValues[1].CreatedTime, tier3Objects[0].CreatedTime);

            var array = JArray.Parse(Encoding.UTF8.GetString(tier3Objects[0].Data.ToArray())).Values<int>().ToArray();
            Assert.Equal(6, array.Length);
            Assert.Contains(1, array);
            Assert.Contains(2, array);
            Assert.Contains(3, array);
            Assert.Contains(4, array);
            Assert.Contains(5, array);
            Assert.Contains(6, array);
        }
    }
}
