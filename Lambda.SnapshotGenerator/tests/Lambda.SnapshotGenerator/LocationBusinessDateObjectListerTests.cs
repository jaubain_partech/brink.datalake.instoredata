﻿using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.UnitTests
{
    public class LocationBusinessDateObjectListerTests
    {
        private readonly LocationBusinessDateObjectLister _classUnderTest;

        private readonly string _bucketName;
        private readonly Mock<ILogger<LocationBusinessDateObjectLister>> _loggerMock;
        private readonly LocationBusinessDateObjectListerOptions _options;
        private readonly Mock<IAmazonS3> _s3Mock;
        private readonly Mock<ITier2KeyPrefixProvider> _tier2KeyPrefixProviderMock;
        private readonly Mock<ILocationBusinessDateObjectTracker> _tier2ObjectTrackerMock;

        public LocationBusinessDateObjectListerTests()
        {
            _bucketName = "bucket";
            _loggerMock = new Mock<ILogger<LocationBusinessDateObjectLister>>(MockBehavior.Loose);
            _options = new LocationBusinessDateObjectListerOptions { BucketName = _bucketName };
            _s3Mock = new Mock<IAmazonS3>(MockBehavior.Strict);
            _tier2KeyPrefixProviderMock = new Mock<ITier2KeyPrefixProvider>(MockBehavior.Strict);
            _tier2ObjectTrackerMock = new Mock<ILocationBusinessDateObjectTracker>(MockBehavior.Strict);

            _classUnderTest = new LocationBusinessDateObjectLister(Options.Create(_options), _s3Mock.Object,
                _tier2KeyPrefixProviderMock.Object, _tier2ObjectTrackerMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task GetObjectListAsync_ShouldListObjects()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;
            var prefix = "prefix";

            _tier2ObjectTrackerMock
                .Setup(i => i.GetProcessedObjectKeysAsync(locationId, businessDate))
                .ReturnsAsync(new HashSet<string>());

            _tier2KeyPrefixProviderMock
                .Setup(i => i.GetKeyPrefix(locationId, businessDate))
                .Returns(prefix);

            var s3Response = new ListObjectsV2Response
            {
                S3Objects = new List<S3Object>
                {
                    new S3Object { Key = "key1"},
                    new S3Object { Key = "key2"}
                }
            };

            _s3Mock
                .Setup(i => i.ListObjectsV2Async(It.Is<ListObjectsV2Request>(j => j.BucketName == _bucketName
                    && j.Prefix == prefix), default))
                .ReturnsAsync(s3Response);

            // Act
            var objects = await _classUnderTest.GetObjectListAsync(locationId, businessDate);

            // Assert
            Assert.Equal(2, objects.Count);
            Assert.Contains(objects, i => i == s3Response.S3Objects[0]);
            Assert.Contains(objects, i => i == s3Response.S3Objects[1]);
        }

        [Fact]
        public async Task GetObjectListAsync_ShouldNotReturnProcessedObjects()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;
            var prefix = "prefix";
            var processedKey = "key1";

            _tier2ObjectTrackerMock
                .Setup(i => i.GetProcessedObjectKeysAsync(locationId, businessDate))
                .ReturnsAsync(new HashSet<string> { processedKey });

            _tier2KeyPrefixProviderMock
                .Setup(i => i.GetKeyPrefix(locationId, businessDate))
                .Returns(prefix);

            var s3Response = new ListObjectsV2Response
            {
                S3Objects = new List<S3Object>
                {
                    new S3Object { Key = processedKey },
                    new S3Object { Key = "key2" }
                }
            };

            _s3Mock
                .Setup(i => i.ListObjectsV2Async(It.Is<ListObjectsV2Request>(j => j.BucketName == _bucketName
                    && j.Prefix == prefix), default))
                .ReturnsAsync(s3Response);

            // Act
            var objects = await _classUnderTest.GetObjectListAsync(locationId, businessDate);

            // Assert
            Assert.Single(objects);
            Assert.Contains(objects, i => i == s3Response.S3Objects[1]);
        }

        [Fact]
        public async Task GetObjectListAsync_ShouldInvokeS3MultipleTimesWhenNecessary()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            var businessDate = DateTime.Now.Date;
            var prefix = "prefix";

            _tier2ObjectTrackerMock
                .Setup(i => i.GetProcessedObjectKeysAsync(locationId, businessDate))
                .ReturnsAsync(new HashSet<string>());

            _tier2KeyPrefixProviderMock
                .Setup(i => i.GetKeyPrefix(locationId, businessDate))
                .Returns(prefix);

            var s3Response1 = new ListObjectsV2Response
            {
                IsTruncated = true,
                NextContinuationToken = "token",
                S3Objects = new List<S3Object>
                {
                    new S3Object { Key = "key1"},
                    new S3Object { Key = "key2"}
                }
            };

            _s3Mock
                .Setup(i => i.ListObjectsV2Async(It.Is<ListObjectsV2Request>(j => j.BucketName == _bucketName
                    && j.Prefix == prefix && j.ContinuationToken == null), default))
                .ReturnsAsync(s3Response1);

            var s3Response2 = new ListObjectsV2Response
            {
                S3Objects = new List<S3Object>
                {
                    new S3Object { Key = "key3"},
                    new S3Object { Key = "key4"}
                }
            };

            _s3Mock
                .Setup(i => i.ListObjectsV2Async(It.Is<ListObjectsV2Request>(j => j.BucketName == _bucketName
                    && j.Prefix == prefix && j.ContinuationToken == s3Response1.NextContinuationToken), default))
                .ReturnsAsync(s3Response2);

            // Act
            var objects = await _classUnderTest.GetObjectListAsync(locationId, businessDate);

            // Assert
            Assert.Equal(4, objects.Count);
            Assert.Contains(objects, i => i == s3Response1.S3Objects[0]);
            Assert.Contains(objects, i => i == s3Response1.S3Objects[1]);
            Assert.Contains(objects, i => i == s3Response2.S3Objects[0]);
            Assert.Contains(objects, i => i == s3Response2.S3Objects[1]);
        }
    }
}
