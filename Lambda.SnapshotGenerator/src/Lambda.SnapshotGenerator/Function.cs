using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Brink.Aws.Lambda;
using Brink.DataLake.InStoreData.Lambda.Common;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Brink.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Threading.Tasks;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Brink.DataLake.Lambda
{
    public class Function
    {
        public async Task FunctionHandler(Stream input, ILambdaContext context)
        {
            if (!TryParseCommand(input, context, out var command))
            {
                return;
            }

            try
            {
                var host = await LambdaHost.GetOrCreateAsync(
                    () => LambdaHost.CreateBuilder().UseStartup<Startup>().BuildAsync(context));

                host.SetContext(context);

                var globalLoggerScope = host.ServiceProvider.GetRequiredService<IGlobalLoggerScope>();

                globalLoggerScope.Add("LocationId", command.LocationId);
                globalLoggerScope.Add("BusinessDate", command.BusinessDate.ToString("yyyy-MM-dd"));

                var processor = host.ServiceProvider.GetRequiredService<ISnapshotGenerator>();

                await processor.ProcessLocationBusinessDateAsync(command.LocationId, command.BusinessDate);
            }
            catch (Exception e)
            {
                context.Logger.LogLine(e.ToString());

                throw;
            }
        }

        private bool TryParseCommand(Stream inputStream, ILambdaContext context, out ProcessLocationDataCommand command)
        {
            command = null;

            var input = new StreamReader(inputStream).ReadToEnd();

            try
            {
                var jObject = JObject.Parse(input);

                if (jObject.TryGetValue("locationId", out _))
                {
                    command = jObject.ToObject<ProcessLocationDataCommand>();
                }
                else if (jObject.TryGetValue("Records", out _))
                {
                    var sqsEvent = jObject.ToObject<SQSEvent>();

                    command = JsonConvert.DeserializeObject<ProcessLocationDataCommand>(sqsEvent.Records[0].Body);
                }
                else
                {
                    context.Logger.LogLine("Unable to identify input.");
                    context.Logger.LogLine(input);
                }

                return command != null;
            }
            catch (Exception e)
            {
                context.Logger.LogLine("Unhandled exception parsing input: " + e.ToString());

                return false;
            }
        }
    }
}
