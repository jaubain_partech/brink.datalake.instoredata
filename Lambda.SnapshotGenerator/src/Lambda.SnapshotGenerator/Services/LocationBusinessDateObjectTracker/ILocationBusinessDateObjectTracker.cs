﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public interface ILocationBusinessDateObjectTracker
    {
        Task<ISet<string>> GetProcessedObjectKeysAsync(Guid locationId, DateTime businessDate);

        Task MarkObjectKeysAsProcessedAsync(Guid locationId, DateTime businessDate, ISet<string> keys);
    }
}
