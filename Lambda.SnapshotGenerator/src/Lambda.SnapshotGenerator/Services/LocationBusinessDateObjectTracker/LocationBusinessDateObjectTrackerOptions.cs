﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class LocationBusinessDateObjectTrackerOptions
    {
        public string TableName { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(TableName))
            {
                throw new InvalidOperationException("A valid table name must be specified.");
            }
        }
    }
}
