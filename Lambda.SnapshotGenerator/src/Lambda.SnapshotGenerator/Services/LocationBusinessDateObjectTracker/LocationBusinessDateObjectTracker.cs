﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class LocationBusinessDateObjectTracker : DynamoRepository, ILocationBusinessDateObjectTracker
    {
        private const string KeysAttribute = "keys";

        private readonly LocationBusinessDateObjectTrackerOptions _options;

        protected override string TableName => _options.TableName;

        public LocationBusinessDateObjectTracker(IOptions<LocationBusinessDateObjectTrackerOptions> options, IAmazonDynamoDB dynamoDb,
            ILogger<LocationBusinessDateObjectTracker> logger)
            : base(dynamoDb, logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        private Dictionary<string, AttributeValue> GetKeyDictionary(Guid locationId, DateTime businessDate)
        {
            return GetKeyDictionary($"Tier2ObjectTracker_{locationId:N}", businessDate.ToString("yyyy-MM-dd"));
        }

        public async Task<ISet<string>> GetProcessedObjectKeysAsync(Guid locationId, DateTime businessDate)
        {
            var item = await GetItemAsync(GetKeyDictionary(locationId, businessDate));

            return new HashSet<string>(item?[KeysAttribute]?.SS ?? Enumerable.Empty<string>());
        }

        public async Task MarkObjectKeysAsProcessedAsync(Guid locationId, DateTime businessDate, ISet<string> keys)
        {
            var key = GetKeyDictionary(locationId, businessDate);

            var item = await GetItemAsync(key);

            if (item == null)
            {
                item = new Dictionary<string, AttributeValue>(key)
                {
                    [KeysAttribute] = new AttributeValue(keys.ToList())
                };
            }
            else
            {
                item[KeysAttribute].SS = item[KeysAttribute].SS.Concat(keys).Distinct().ToList();
            }

            var request = new PutItemRequest(_options.TableName, item);

            await DynamoDb.PutItemAsync(request);
        }
    }
}
