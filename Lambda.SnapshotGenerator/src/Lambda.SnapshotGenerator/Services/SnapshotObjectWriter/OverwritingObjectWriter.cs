﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class OverwritingObjectWriter : SnapshotObjectWriter
    {
        private readonly ILogger<OverwritingObjectWriter> _logger;

        public OverwritingObjectWriter(IOptions<SnapshotObjectWriterOptions> options, IAmazonS3 s3,
            ILogger<OverwritingObjectWriter> logger)
            : base(options, s3)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected override async Task WriteObjectCoreAsync(string key, SnapshotObject @object)
        {
            var request = new PutObjectRequest
            {
                BucketName = BucketName,
                InputStream = @object.Data,
                Key = key
            };

            await S3.PutObjectAsync(request);
        }
    }
}
