﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public interface ISnapshotObjectWriter
    {
        Task WriteObjectAsync(string key, SnapshotObject @object);
    }
}
