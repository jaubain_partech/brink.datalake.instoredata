﻿using Amazon.S3;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public abstract class SnapshotObjectWriter : ISnapshotObjectWriter
    {
        private readonly SnapshotObjectWriterOptions _options;

        protected string BucketName => _options.BucketName;

        protected IAmazonS3 S3 { get; }

        protected SnapshotObjectWriter(IOptions<SnapshotObjectWriterOptions> options, IAmazonS3 s3)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));

            S3 = s3 ?? throw new ArgumentNullException(nameof(s3));
        }

        public async Task WriteObjectAsync(string key, SnapshotObject @object)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("A valid key must be specified.", nameof(key));
            }

            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }


            await WriteObjectCoreAsync(key, @object);
        }

        protected abstract Task WriteObjectCoreAsync(string key, SnapshotObject @object);
    }
}
