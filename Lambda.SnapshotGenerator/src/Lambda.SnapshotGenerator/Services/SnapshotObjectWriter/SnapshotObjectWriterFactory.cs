﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class SnapshotObjectWriterFactory : ISnapshotObjectWriterFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public SnapshotObjectWriterFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public ISnapshotObjectWriter CreateWriter(string dataType)
        {
            if (string.IsNullOrWhiteSpace(dataType))
            {
                throw new ArgumentException("A valid data type must be specified.");
            }

            if (dataType == Constants.Orders)
            {
                return _serviceProvider.GetRequiredService<OrderObjectWriter>();
            }

            return _serviceProvider.GetRequiredService<OverwritingObjectWriter>();
        }
    }
}
