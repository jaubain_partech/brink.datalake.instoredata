﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class OrderObjectWriter : SnapshotObjectWriter
    {
        private readonly ILogger<OrderObjectWriter> _logger;
        private readonly JsonSerializer _serializer;

        public OrderObjectWriter(IOptions<SnapshotObjectWriterOptions> options, IAmazonS3 s3,
            ILogger<OrderObjectWriter> logger)
            : base(options, s3)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _serializer = new JsonSerializer();
        }

        protected override async Task WriteObjectCoreAsync(string key, SnapshotObject @object)
        {
            GetObjectResponse response = null;

            try
            {
                response = await S3.GetObjectAsync(BucketName, key);

                _logger.LogInformation($"Merging orders with existing object.");
            }
            catch (AmazonS3Exception e) when (e.Message == "The specified key does not exist.")
            {
                _logger.LogInformation($"Object {key} not found. A new object will be created.");
            }

            var orderLookup = new Dictionary<long, JObject>();

            if (response != null)
            {
                using (response.ResponseStream)
                using (var streamReader = new StreamReader(response.ResponseStream))
                using (var jsonReader = new JsonTextReader(streamReader))
                {
                    orderLookup = _serializer.Deserialize<JArray>(jsonReader)
                        .ToDictionary(i => i["id"].ToObject<long>(), i => (JObject)i);
                }

                _logger.LogInformation($"Retrieved {orderLookup.Count} existing orders.");
            }

            JArray newOrders;

            using (var streamReader = new StreamReader(@object.Data))
            using (var jsonReader = new JsonTextReader(streamReader))
            {
                newOrders = _serializer.Deserialize<JArray>(jsonReader);

                _logger.LogInformation($"Retrieved {newOrders.Count} new orders.");
            }

            foreach (var order in newOrders)
            {
                var id = order["id"].ToObject<long>();

                if (orderLookup.TryGetValue(id, out var existingOrder))
                {
                    _logger.LogDebug($"Existing order ({id}) found.");

                    var orderModifiedTime = order["modifiedTime"].ToObject<DateTimeOffset>();
                    var existingOrderModifiedTime = existingOrder["modifiedTime"].ToObject<DateTimeOffset>();

                    if (orderModifiedTime >= existingOrderModifiedTime)
                    {
                        _logger.LogDebug($"Replacing existing order ({id}) with newer version.");

                        orderLookup[id] = (JObject)order;
                    }
                }
                else
                {
                    _logger.LogDebug($"New order ({id}) found.");

                    orderLookup.Add(id, (JObject)order);
                }
            }

            var outputStream = new MemoryStream();
            var streamWriter = new StreamWriter(outputStream);

            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                _serializer.Serialize(jsonWriter, orderLookup.Select(i => i.Value).ToArray());
                streamWriter.Flush();

                var request = new PutObjectRequest
                {
                    BucketName = BucketName,
                    InputStream = outputStream,
                    Key = key
                };

                await S3.PutObjectAsync(request);
            }

            _logger.LogInformation($"Saved {orderLookup.Count} orders.");
        }
    }
}
