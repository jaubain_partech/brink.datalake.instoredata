﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class SnapshotObjectWriterOptions
    {
        public string BucketName { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(BucketName))
            {
                throw new InvalidOperationException("A valid bucket name must be specified.");
            }
        }
    }
}
