﻿namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public interface ISnapshotObjectWriterFactory
    {
        ISnapshotObjectWriter CreateWriter(string dataType);
    }
}
