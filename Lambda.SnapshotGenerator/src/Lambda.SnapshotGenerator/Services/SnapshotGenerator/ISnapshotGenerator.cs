﻿using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public interface ISnapshotGenerator
    {
        Task ProcessLocationBusinessDateAsync(Guid locationId, DateTime businessDate);
    }
}
