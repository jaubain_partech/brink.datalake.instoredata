﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class SnapshotGenerator : ISnapshotGenerator
    {
        private readonly ILogger<SnapshotGenerator> _logger;
        private readonly ISnapshotObjectBuilder _objectBuilder;
        private readonly ILocationBusinessDateObjectLister _objectLister;
        private readonly ILocationBusinessDateObjectTracker _objectTracker;
        private readonly ISnapshotObjectWriterFactory _objectWriterFactory;

        public SnapshotGenerator(ISnapshotObjectBuilder objectBuilder, ILocationBusinessDateObjectLister objectLister,
            ILocationBusinessDateObjectTracker objectTracker, ISnapshotObjectWriterFactory objectWriterFactory, 
            ILogger<SnapshotGenerator> logger)
        {
            _objectBuilder = objectBuilder ?? throw new ArgumentNullException(nameof(objectBuilder));
            _objectLister = objectLister ?? throw new ArgumentNullException(nameof(objectLister));
            _objectTracker = objectTracker ?? throw new ArgumentNullException(nameof(objectTracker));
            _objectWriterFactory = objectWriterFactory ?? throw new ArgumentNullException(nameof(objectWriterFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task ProcessLocationBusinessDateAsync(Guid locationId, DateTime businessDate)
        {
            try
            {
                _logger.LogInformation("Location business date processing started.");

                var objects = await _objectLister.GetObjectListAsync(locationId, businessDate);

                if (objects.Count == 0)
                {
                    return;
                }

                var snapshotObjects = await _objectBuilder.BuildTier3ObjectsAsync(objects);

                if (snapshotObjects.Count == 0)
                {
                    _logger.LogWarning("No tier 3 objects were built.");

                    return;
                }

                var writerTasks = new List<Task>();

                foreach (var snapshotObject in snapshotObjects)
                {
                    var key = $"{locationId:N}/{businessDate:yyyy-MM-dd}/{snapshotObject.Type}";
                    var writer = _objectWriterFactory.CreateWriter(snapshotObject.Type);
                    writerTasks.Add(writer.WriteObjectAsync(key, snapshotObject));
                }

                await Task.WhenAll(writerTasks);

                await _objectTracker.MarkObjectKeysAsProcessedAsync(locationId, businessDate,
                    new HashSet<string>(objects.Select(i => i.Key)));
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Unhandled exception processing location business date.");
            }
        }
    }
}
