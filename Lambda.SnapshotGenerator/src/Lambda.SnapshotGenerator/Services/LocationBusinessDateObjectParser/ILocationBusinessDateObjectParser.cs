﻿using Amazon.S3.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public interface ILocationBusinessDateObjectParser
    {
        Task<IList<ParsedValue>> ParseObjectAsync(S3Object s3Object);
    }
}
