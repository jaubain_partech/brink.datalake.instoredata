﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class LocationBusinessDateObjectParser : ILocationBusinessDateObjectParser
    {
        private readonly ILogger<LocationBusinessDateObjectParser> _logger;
        private readonly IAmazonS3 _s3;
        private readonly JsonSerializer _serializer;

        public LocationBusinessDateObjectParser(IAmazonS3 s3, ILogger<LocationBusinessDateObjectParser> logger)
        {
            _s3 = s3 ?? throw new ArgumentNullException(nameof(s3));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _serializer = JsonSerializer.Create();
        }

        public async Task<MemoryStream> DecodeDataStringAsync(string encodedData)
        {
            var outputStream = new MemoryStream();

            using (var inputStream = new MemoryStream(Convert.FromBase64String(encodedData)))
            using (var decompressionStream = new GZipStream(inputStream, CompressionMode.Decompress))
            {
                await decompressionStream.CopyToAsync(outputStream).ConfigureAwait(false);
            }

            outputStream.Position = 0;

            return outputStream;
        }

        public async Task<IList<ParsedValue>> ParseObjectAsync(S3Object s3Object)
        {
            var parsedValues = new List<ParsedValue>();

            using (var objectStream = await _s3.GetObjectStreamAsync(s3Object.BucketName, s3Object.Key, null))
            using (var reader = new StreamReader(objectStream))
            {
                string line;
                int lineCount = 0;

                do
                {
                    line = await reader.ReadLineAsync();

                    if (line != null)
                    {
                        lineCount++;

                        var lineObjects = await TryParseLineAsync(line, lineCount);

                        if (lineObjects != null)
                        {
                            parsedValues.AddRange(lineObjects);
                        }
                    }
                }
                while (line != null);
            }

            return parsedValues;
        }

        public async Task<List<ParsedValue>> TryParseLineAsync(string line, int lineNumber)
        {
            var tokens = line.Split(',', 2);

            if (!DateTimeOffset.TryParse(tokens[0], out var createdTime))
            {
                _logger.LogError($"Failed to parse created time (line {lineNumber}).");

                return null;
            }

            if (string.IsNullOrWhiteSpace(tokens[1]))
            {
                _logger.LogError($"Empty data value (line {lineNumber}).");

                return null;
            }

            try
            {
                var values = new List<ParsedValue>();

                var jsonData = await DecodeDataStringAsync(tokens[1]);

                using (var textReader = new StreamReader(jsonData))
                using (var jsonReader = new JsonTextReader(textReader))
                {
                    var jObject = _serializer.Deserialize<JObject>(jsonReader);

                    foreach (var property in jObject.Properties())
                    {
                        values.Add(new ParsedValue(property.Name.ToLower(), property.Value, createdTime));
                    }

                    return values;
                }
            }
            catch (JsonReaderException e)
            {
                _logger.LogError(e, $"Error parsing JSON object (line {lineNumber}).");

                return null;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Error decoding data (line {lineNumber}).");

                return null;
            }
        }
    }
}
