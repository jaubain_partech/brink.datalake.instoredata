﻿using Newtonsoft.Json.Linq;
using System;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class ParsedValue
    {
        public DateTimeOffset CreatedTime { get; }

        public JToken Data { get; }

        public string Name { get; }

        public ParsedValue(string name, JToken data, DateTimeOffset createdTime)
        {
            Name = name;
            Data = data;
            CreatedTime = createdTime;
        }
    }
}
