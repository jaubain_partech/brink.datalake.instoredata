﻿using Amazon.S3;
using Amazon.S3.Model;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class LocationBusinessDateObjectLister : ILocationBusinessDateObjectLister
    {
        private readonly ILogger<LocationBusinessDateObjectLister> _logger;
        private readonly LocationBusinessDateObjectListerOptions _options;
        private readonly IAmazonS3 _s3;
        private readonly ITier2KeyPrefixProvider _tier2KeyPrefixProvider;
        private readonly ILocationBusinessDateObjectTracker _tier2ObjectTracker;

        public LocationBusinessDateObjectLister(IOptions<LocationBusinessDateObjectListerOptions> options, IAmazonS3 s3, 
            ITier2KeyPrefixProvider tier2KeyPrefixProvider, ILocationBusinessDateObjectTracker tier2ObjectTracker,
            ILogger<LocationBusinessDateObjectLister> logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _s3 = s3 ?? throw new ArgumentNullException(nameof(s3));
            _tier2KeyPrefixProvider = tier2KeyPrefixProvider 
                ?? throw new ArgumentNullException(nameof(tier2KeyPrefixProvider));
            _tier2ObjectTracker = tier2ObjectTracker ?? throw new ArgumentNullException(nameof(tier2ObjectTracker));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IList<S3Object>> GetObjectListAsync(Guid locationId, DateTime businessDate)
        {
            var trackedKeys = await _tier2ObjectTracker.GetProcessedObjectKeysAsync(locationId, businessDate);

            var objects = new List<S3Object>();

            var request = new ListObjectsV2Request
            {
                BucketName = _options.BucketName,
                Prefix = _tier2KeyPrefixProvider.GetKeyPrefix(locationId, businessDate)
            };

            do
            {
                var response = await _s3.ListObjectsV2Async(request);

                objects.AddRange(response.S3Objects.Where(i => !trackedKeys.Contains(i.Key)));

                request.ContinuationToken = response.IsTruncated ? response.NextContinuationToken : null;
            }
            while (request.ContinuationToken != null);

            _logger.LogInformation($"Found {objects.Count} objects using prefix '{request.Prefix}'.");

            if (_logger.IsEnabled(LogLevel.Debug) && objects.Count > 0)
            {
                _logger.LogDebug("The following objects were found: {Keys}",
                    string.Join(" | ", objects.Select(i => i.Key).ToArray()));
            }

            return objects;
        }
    }
}
