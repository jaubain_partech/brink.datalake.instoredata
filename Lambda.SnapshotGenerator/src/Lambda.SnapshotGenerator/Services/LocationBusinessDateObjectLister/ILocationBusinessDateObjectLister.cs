﻿using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public interface ILocationBusinessDateObjectLister
    {
        Task<IList<S3Object>> GetObjectListAsync(Guid locationId, DateTime businessDate);
    }
}
