﻿using System;
using System.IO;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class SnapshotObject
    {
        public DateTimeOffset CreatedTime { get; set; }

        public MemoryStream Data { get; set; }

        public string Type { get; set; }
    }
}
