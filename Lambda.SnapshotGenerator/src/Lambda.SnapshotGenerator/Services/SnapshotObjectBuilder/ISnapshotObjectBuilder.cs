﻿using Amazon.S3.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public interface ISnapshotObjectBuilder
    {
        Task<IList<SnapshotObject>> BuildTier3ObjectsAsync(IList<S3Object> s3Objects);
    }
}
