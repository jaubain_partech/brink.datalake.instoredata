﻿using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services
{
    public class SnapshotObjectBuilder : ISnapshotObjectBuilder
    {
        private readonly ILogger<SnapshotObjectBuilder> _logger;
        private readonly ILocationBusinessDateObjectParser _objectParser;

        public SnapshotObjectBuilder(ILocationBusinessDateObjectParser objectParser, ILogger<SnapshotObjectBuilder> logger)
        {
            _objectParser = objectParser ?? throw new ArgumentNullException(nameof(objectParser));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IList<SnapshotObject>> BuildTier3ObjectsAsync(IList<S3Object> s3Objects)
        {
            var tasks = s3Objects.Select(_objectParser.ParseObjectAsync).ToArray();

            await Task.WhenAll(tasks);

            var parsedValues = tasks.SelectMany(i => i.Result).ToList();

            return BuildTier3ObjectsFromParsedValues(parsedValues);
        }

        public IList<SnapshotObject> BuildTier3ObjectsFromParsedValues(IList<ParsedValue> parsedValues)
        {
            var tier3Objects = new List<SnapshotObject>();

            foreach (var grouping in parsedValues.GroupBy(i => i.Name))
            {
                if (grouping.Key == Constants.Orders)
                {
                    var values = ConcatenateValues(grouping);

                    var tier3Object = new SnapshotObject
                    {
                        CreatedTime = grouping.OrderByDescending(i => i.CreatedTime).First().CreatedTime,
                        Data = new MemoryStream(Encoding.UTF8.GetBytes(values.ToString())),
                        Type = grouping.Key
                    };

                    tier3Objects.Add(tier3Object);
                }
                else
                {
                    var mostRecentValue = grouping.OrderByDescending(i => i.CreatedTime).First();

                    var tier3Object = new SnapshotObject
                    {
                        CreatedTime = mostRecentValue.CreatedTime,
                        Data = new MemoryStream(Encoding.UTF8.GetBytes(mostRecentValue.Data.ToString())),
                        Type = grouping.Key
                    };

                    tier3Objects.Add(tier3Object);
                }
            }

            return tier3Objects;
        }

        private JArray ConcatenateValues(IGrouping<string, ParsedValue> grouping)
        {
            var array = new JArray();

            foreach (var value in grouping)
            {
                array.Merge(value.Data);
            }

            _logger.LogInformation($"A total of {array.Count} '{grouping.Key}' values were merged.");

            return array;
        }
    }
}
