﻿using Amazon.DynamoDBv2;
using Amazon.S3;
using Brink.Aws.Lambda;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Brink.DataLake.InStoreData.Lambda.SnapshotGenerator.Services;
using Brink.Extensions.Configuration.Bootstrap;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.Lambda.Common.ConfigurationKeys;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotGenerator
{
    public class Startup : ILambdaStartup
    {
        public void ConfigureServices(IServiceCollection services, BootstrapConfiguration bootstrapConfiguration,
            IConfiguration configuration)
        {
            services
                .Configure<LocationBusinessDateObjectListerOptions>(i => i.BucketName = configuration[IngestBucketName])
                .PostConfigure<LocationBusinessDateObjectListerOptions>(i => i.Validate())
                .Configure<LocationBusinessDateObjectTrackerOptions>(i => i.TableName = configuration[DynamoDbTableName])
                .PostConfigure<LocationBusinessDateObjectTrackerOptions>(i => i.Validate())
                .Configure<SnapshotObjectWriterOptions>(i => i.BucketName = configuration[CuratedBucketName])
                .PostConfigure<SnapshotObjectWriterOptions>(i => i.Validate())
                .AddAWSService<IAmazonDynamoDB>()
                .AddAWSService<IAmazonS3>()
                .AddSingleton<ISnapshotGenerator, Services.SnapshotGenerator>()
                .AddSingleton<ITier2KeyPrefixProvider, Tier2KeyPrefixProvider>()
                .AddSingleton<ILocationBusinessDateObjectLister, LocationBusinessDateObjectLister>()
                .AddSingleton<ILocationBusinessDateObjectParser, LocationBusinessDateObjectParser>()
                .AddSingleton<ILocationBusinessDateObjectTracker, LocationBusinessDateObjectTracker>()
                .AddSingleton<ISnapshotObjectBuilder, SnapshotObjectBuilder>()
                .AddSingleton<ISnapshotObjectWriterFactory, SnapshotObjectWriterFactory>()
                .AddSingleton<OrderObjectWriter>()
                .AddSingleton<OverwritingObjectWriter>();
        }

        public async Task<IDictionary<string, string>> GetAdditionalConfigurationAsync(LambdaStartupConfigurationContext context)
        {
            var parameters = new ParameterStoreConfigurationParameters()
                .AddConfigurationKey(context.EnvironmentPrefix, DynamoDbTableName)
                .AddConfigurationKey(context.EnvironmentPrefix, IngestBucketName)
                .AddConfigurationKey(context.EnvironmentPrefix, CuratedBucketName);

            return await context.ParameterStoreConfigurationProvider.GetConfigurationAsync(parameters);
        }
    }
}
