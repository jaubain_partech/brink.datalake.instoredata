﻿using Brink.DataLake.InStoreData.Contracts;
using Brink.DataLake.InStoreData.TerminalComponents;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RegisterSimulator
{
    public class InStoreDataProvider : IInStoreDataProvider
    {
        private readonly IDataSetGenerator _dataSetGenerator;
        private readonly ILogger<InStoreDataProvider> _logger;
        private readonly Guid _locationId;

        public InStoreDataProvider(Guid locationId, IDataSetGenerator dataSetGenerator, ILogger<InStoreDataProvider> logger)
        {
            _dataSetGenerator = dataSetGenerator ?? throw new ArgumentNullException(nameof(dataSetGenerator));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _locationId = locationId;
        }

        public InStoreDataSnapshot GenerateSnapshot(InStoreDataSnapshotOptions options)
        {
            var dataSet = _dataSetGenerator.DataSet;

            var snapshot = new InStoreDataSnapshot
            {
                BusinessDate = dataSet.BusinessDate,
                IsOnlineOrderingEnabled = dataSet.IsOnlineOrderingEnabled
            };

            if (options.DataTypes.HasFlag(InStoreDataTypes.CashDrawers))
            {
                snapshot.CashDrawers = new List<CashDrawer>(dataSet.CashDrawers);
            }

            if (options.DataTypes.HasFlag(InStoreDataTypes.Employees))
            {
                snapshot.Employees = new List<Employee>(dataSet.Employees);
            }

            if (options.DataTypes.HasFlag(InStoreDataTypes.Items))
            {
                snapshot.Items = new List<Item>(dataSet.Items);
            }

            if (options.DataTypes.HasFlag(InStoreDataTypes.Orders))
            {
                IEnumerable<Order> orders = dataSet.Orders;

                if (options.OrdersLastModifiedTime != null)
                {
                    orders = orders.Where(i => i.ModifiedTime > options.OrdersLastModifiedTime);
                }

                snapshot.Orders = new List<Order>(orders);
            }

            if (options.DataTypes.HasFlag(InStoreDataTypes.Parties))
            {
                snapshot.Parties = new List<Party>(dataSet.Parties);
            }

            if (options.DataTypes.HasFlag(InStoreDataTypes.Tills))
            {
                snapshot.Tills = new List<Till>(dataSet.Tills);
            }

            _logger.LogInformation($"Snapshot created. BD:{snapshot.BusinessDate} CD:{snapshot.CashDrawers.Count} "
                + $"E:{snapshot.Employees.Count} IOOE:{snapshot.IsOnlineOrderingEnabled} I:{snapshot.Items.Count} "
                + $"O:{snapshot.Orders.Count} P:{snapshot.Parties.Count} T:{snapshot.Tills.Count}");

            return snapshot;
        }
    }
}
