﻿using Brink.DataLake.InStoreData.Contracts;
using System;
using System.Collections.Generic;

namespace RegisterSimulator
{
    public class DataSet
    {
        public DateTime BusinessDate { get; }

        public List<CashDrawer> CashDrawers { get; } = new List<CashDrawer>();

        public List<Employee> Employees { get; } = new List<Employee>();

        public bool IsOnlineOrderingEnabled { get; set; }

        public List<Item> Items { get; } = new List<Item>();

        public List<Order> Orders { get; } = new List<Order>();

        public List<Party> Parties { get; } = new List<Party>();

        public List<Till> Tills { get; } = new List<Till>();

        public DataSet(DateTime businessDate) => BusinessDate = businessDate;
    }
}
