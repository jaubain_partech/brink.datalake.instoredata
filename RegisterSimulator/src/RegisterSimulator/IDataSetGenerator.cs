﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RegisterSimulator
{
    public interface IDataSetGenerator
    {
        DataSet DataSet { get; }

        void GenerateData();
    }
}
