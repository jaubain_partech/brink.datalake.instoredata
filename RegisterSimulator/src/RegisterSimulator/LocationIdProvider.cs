﻿using Brink.DataLake.InStoreData.TerminalComponents;
using System;

namespace RegisterSimulator
{
    public class LocationIdProvider : ILocationIdProvider
    {
        public Guid LocationId { get; }

        public LocationIdProvider(Guid locationId) => LocationId = locationId;
    }
}
