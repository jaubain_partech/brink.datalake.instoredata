﻿using Brink.DataLake.InStoreData.Contracts;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RegisterSimulator
{
    public class CsvDataSetGenerator : IDataSetGenerator
    {
        private readonly DataSet _internalDataSet;

        public DataSet DataSet { get; }

        public Guid LocationId { get; private set; }

        public CsvDataSetGenerator(string fileName, DateTime startingBusinessDate)
        {
            DataSet = new DataSet(startingBusinessDate);

            _internalDataSet = BuildInternalDataset(fileName, startingBusinessDate);

        }

        private DataSet BuildInternalDataset(string fileName, DateTime startingBusinessDate)
        {
            var dataSet = new DataSet(startingBusinessDate);

            var lines = File.ReadAllLines(fileName);

            foreach (var line in lines.Skip(1))
            {
                var tokens = line.Split(',', 4);

                if (LocationId == Guid.Empty)
                {
                    LocationId = Guid.Parse(tokens[1].Replace("\"", string.Empty));
                }

                var type = int.Parse(tokens[2]);

                switch (type)
                {
                    case 1:
                        dataSet.CashDrawers.AddRange(ParseArray<CashDrawer>(tokens[3]));
                        break;

                    case 2:
                        dataSet.Employees.AddRange(ParseArray<Employee>(tokens[3]));
                        break;

                    case 3:
                        dataSet.Items.AddRange(ParseArray<Item>(tokens[3]));
                        break;

                    case 4:
                        {
                            var orders = ParseArray<Order>(tokens[3]);

                            foreach (var order in orders)
                            {
                                var diff = dataSet.BusinessDate.Date - order.OpenTime.Date;
                                order.OpenTime = order.OpenTime.Add(diff);
                                order.ModifiedTime = order.ModifiedTime.Add(diff);

                                if (order.CloseTime != null)
                                {
                                    order.CloseTime = order.CloseTime.Value.Add(diff);
                                }
                            }

                            dataSet.Orders.AddRange(orders.OrderBy(i => i.ModifiedTime));
                        }
                        break;

                    case 6:
                        dataSet.Tills.AddRange(ParseArray<Till>(tokens[3]));
                        break;

                    default:
                        continue;
                }

            }

            Console.WriteLine($"CSV file loaded. BD:{dataSet.BusinessDate:yyyy-MM-dd} CD:{dataSet.CashDrawers.Count} "
                + $"E:{dataSet.Employees.Count} IOOE:{dataSet.IsOnlineOrderingEnabled} I:{dataSet.Items.Count} "
                + $"O:{dataSet.Orders.Count} P:{dataSet.Parties.Count} T:{dataSet.Tills.Count}");

            return dataSet;
        }

        public void GenerateData()
        {
            var random = new Random();

            addToCollection(_internalDataSet.CashDrawers, DataSet.CashDrawers);
            addToCollection(_internalDataSet.Employees, DataSet.Employees);
            addToCollection(_internalDataSet.Items, DataSet.Items);
            addToCollection(_internalDataSet.Orders, DataSet.Orders);
            addToCollection(_internalDataSet.Tills, DataSet.Tills);

            void addToCollection<T>(List<T> source, List<T> destination)
            {
                if (source.Count == destination.Count)
                {
                    return;
                }

                var difference = source.Count - destination.Count;

                var numberToAdd = random.Next(1, Math.Min(difference, 4));

                var entities = source.Skip(destination.Count).Take(numberToAdd).ToArray();

                destination.AddRange(entities);
            }
        }

        private List<T> ParseArray<T>(string token)
        {
            token = PrepareJsonString(token);

            var entities = new List<T>();

            var array = JArray.Parse(token);

            foreach (var entry in array)
            {
                var entity = entry.ToObject<T>();

                entities.Add(entity);
            }

            return entities;
        }

        private string PrepareJsonString(string originalValue)
        {
            originalValue = originalValue.Replace("\"\"", "\"");

            return originalValue.AsSpan().Slice(1, originalValue.Length - 2).ToString();
        }
    }
}
