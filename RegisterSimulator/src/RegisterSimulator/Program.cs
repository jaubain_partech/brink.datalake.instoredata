﻿using Amazon.KinesisFirehose;
using Amazon.S3;
using Brink.DataLake.InStoreData.TerminalComponents;
using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Brink.Extensions.DependencyInjection;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RegisterSimulator
{
    public class Program
    {
        private readonly CancellationTokenSource _cancellationTokenSource;
        private IDataSetGenerator _dataSetGenerator;
        private IOutboxPopulationManager _populationManager;
        private IDataLakeSendManager _senderManager;
        private ILogger<Program> _logger;
        private IServiceProvider _serviceProvider;
        private ISchemaMigrator _schemaMigrator;

        [Option(Description = "Data file name", ShortName = "df", LongName = "dataFile", ValueName = "Data file name")]
        public string DataFileName { get; set; }

        public Guid LocationId { get; set; }

        [Option(Description = "The location ID", ShortName = "lid", LongName = "locationId", ValueName = "Location ID")]
        public string LocationIdString { get; set; }

        static async Task Main(string[] args)
        {
            await CommandLineApplication.ExecuteAsync<Program>(args);
        }

        public Program()
        {
            _cancellationTokenSource = new CancellationTokenSource();

            Console.CancelKeyPress += (sender, e) => _cancellationTokenSource.Cancel();
        }

        private IServiceProvider BuildServiceProvider()
        {
            const string ConnectionStringName = "InStoreData";

            var configurationBuilder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");

            IDataSetGenerator dataSetGenerator = null;

            if (!string.IsNullOrEmpty(DataFileName))
            {
                Console.WriteLine($"Loading data file: {DataFileName}");

                var csvDataSetGenerator = new CsvDataSetGenerator(DataFileName, DateTime.Now.Date);

                LocationId = csvDataSetGenerator.LocationId;

                dataSetGenerator = csvDataSetGenerator;
            }


            if (LocationId == Guid.Empty && !string.IsNullOrWhiteSpace(LocationIdString))
            {
                LocationId = Guid.Parse(LocationIdString);
            }

            if (LocationId == Guid.Empty)
            {
                var tempConfiguration = configurationBuilder.Build();

                LocationId = Guid.Parse(tempConfiguration["LocationId"]);
            }

            var inMemoryConfiguration = new Dictionary<string, string>
            {
                [$"ConnectionStrings:{ConnectionStringName}"] = $"Data Source=InStoreData.{LocationId}.db;Version=3"
            };

            var configuration = configurationBuilder
                .AddInMemoryCollection(inMemoryConfiguration)
                .Build();

            var deliveryStreamName = configuration["AWS:FirehoseDeliveryStreamName"];
            var ingestBucketName = configuration["AWS:IngestBucketName"];

            var awsOptions = configuration.GetAWSOptions();

            var serviceCollection = new ServiceCollection()
                .AddDefaultAWSOptions(awsOptions)
                .AddAWSService<IAmazonKinesisFirehose>()
                .AddAWSService<IAmazonS3>()
                .AddSingleton<IConfiguration>(configuration)
                .AddLogging(i => i.AddConsole().SetMinimumLevel(LogLevel.Information))
                .AddSingleton<IInStoreDataProvider>(i => ActivatorUtilities.CreateInstance<InStoreDataProvider>(i, LocationId))
                .AddSingleton<ILocationIdProvider>(new LocationIdProvider(LocationId))
                .AddInStoreDataPersistence()
                .AddOutboxPopulator()
                .AddOutboxSender()
                .Configure<InStoreDataPersistenceOptions>(i => i.ConnectionStringName = ConnectionStringName)
                .Configure<CompositeDataOutboxPopulatorOptions>(i => i.PopulationInterval = TimeSpan.FromMinutes(2))
                .Configure<OrderOutboxPopulatorOptions>(i => i.PopulationInterval = TimeSpan.FromMinutes(1))
                .Configure<FirehoseDataLakeRecordSenderOptions>(i => i.DeliveryStreamName = deliveryStreamName)
                .Configure<S3DataLakeRecordSenderOptions>(i => i.BucketName = ingestBucketName);

            if (dataSetGenerator != null)
            {
                serviceCollection.AddSingleton(dataSetGenerator);
            }
            else
            {
                serviceCollection.AddSingleton(i => ActivatorUtilities.CreateInstance<FakeDataSetGenerator>(i, DateTime.Now));

            }

            return serviceCollection.BuildServiceProvider();
        }

        private async Task GenerateDataAsync()
        {
            while (!_cancellationTokenSource.IsCancellationRequested)
            {
                _dataSetGenerator.GenerateData();

                await Task.Delay(TimeSpan.FromSeconds(20), _cancellationTokenSource.Token);
            }
        }

        private async Task OnExecuteAsync()
        {
            _serviceProvider = BuildServiceProvider();

            _logger = _serviceProvider.GetRequiredService<ILogger<Program>>();

            _dataSetGenerator = _serviceProvider.GetRequiredService<IDataSetGenerator>();

            _populationManager = _serviceProvider.GetRequiredService<IOutboxPopulationManager>();

            _senderManager = _serviceProvider.GetRequiredService<IDataLakeSendManager>();

            _schemaMigrator = _serviceProvider.GetRequiredService<ISchemaMigrator>();

            await RunAsync();
        }

        private async Task RunAsync()
        {
            Console.WriteLine($"Location: {LocationId}");

            await _schemaMigrator.EnsureSchemaIsCurrentAsync();

            _populationManager.Start();

            _senderManager.Start();

            await GenerateDataAsync();

            _populationManager.Stop();

            _senderManager.Stop();
        }
    }
}
