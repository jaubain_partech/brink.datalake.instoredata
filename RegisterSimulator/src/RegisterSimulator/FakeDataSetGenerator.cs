﻿using AutoBogus;
using Bogus;
using Brink.DataLake.InStoreData.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace RegisterSimulator
{
    public class FakeDataSetGenerator : IDataSetGenerator
    {
        private const int MaxCashDrawerCount = 20;
        private const int MaxItemCount = 1000;

        private Fakers _fakers;
        private readonly ILogger<FakeDataSetGenerator> _logger;
        private readonly Random _random = new Random();

        public DataSet DataSet { get; private set; }

        public FakeDataSetGenerator(DateTime startingBusinessDate, ILogger<FakeDataSetGenerator> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            DataSet = new DataSet(startingBusinessDate);

            _fakers = new Fakers();
        }

        public void AdvanceBusinessDate()
        {
            DataSet = new DataSet(DataSet.BusinessDate);

            _fakers = new Fakers();
        }

        public void GenerateData()
        {
            _logger.LogInformation("Generating data.");

            generateData(_fakers.CashDrawerFaker, DataSet.CashDrawers, MaxCashDrawerCount, 0, 2);
            generateData(_fakers.ItemFaker, DataSet.Items, MaxItemCount, 0, 5);
            generateData(_fakers.OrderFaker, DataSet.Orders, int.MaxValue, 0, 5);

            void generateData<T>(Faker<T> faker, List<T> collection, int maxCount, int minToGen, int maxToGen)
                where T : class
            {
                if (collection.Count >= maxCount)
                {
                    return;
                }

                collection.AddRange(faker.Generate(_random.Next(minToGen, maxToGen + 1)));
            }

            _logger.LogInformation($"Data generated. BD:{DataSet.BusinessDate} CD:{DataSet.CashDrawers.Count} "
                + $"E:{DataSet.Employees.Count} IOOE:{DataSet.IsOnlineOrderingEnabled} I:{DataSet.Items.Count} "
                + $"O:{DataSet.Orders.Count} P:{DataSet.Parties.Count} T:{DataSet.Tills.Count}");

            if (DataSet.Orders.Count > 0)
            {
                var randomOrderIndex = _random.Next(0, DataSet.Orders.Count);
                var randomOrder = DataSet.Orders[randomOrderIndex];
                randomOrder.ModifiedTime = DateTimeOffset.Now;
            }
        }

        public class Fakers
        {
            private int _nextCashDrawerId = 1;
            private int _nextItemId = 1;
            private long _nextOrderId = 1;

            public Faker<CashDrawer> CashDrawerFaker { get; }

            public Faker<Item> ItemFaker { get; }

            public Faker<Order> OrderFaker { get; }

            public Fakers()
            {
                CashDrawerFaker = new AutoFaker<CashDrawer>()
                    .RuleFor(i => i.Id, () => _nextCashDrawerId++)
                    .RuleFor(i => i.CashInDrawer, i => i.Finance.Amount(0, 5000))
                    .RuleFor(i => i.TillNumber, i => i.Random.Int(1, 100));

                ItemFaker = new AutoFaker<Item>()
                    .RuleFor(i => i.Id, () => _nextItemId++)
                    .RuleFor(i => i.IsUnavailable, i => i.Random.Bool())
                    .RuleFor(i => i.QuantityAvailable, i => i.Random.Int(0, 1000))
                    .RuleFor(i => i.QuantitySold, i => i.Random.Int(0, 1000));

                OrderFaker = new AutoFaker<Order>()
                    .RuleFor(i => i.Id, () => _nextOrderId++)
                    .RuleFor(i => i.ModifiedTime, () => DateTimeOffset.Now);
            }
        }
    }
}
