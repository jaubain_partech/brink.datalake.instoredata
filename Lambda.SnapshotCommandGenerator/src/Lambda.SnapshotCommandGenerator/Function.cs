using Amazon.Lambda.Core;
using Brink.Aws.Lambda;
using Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator;
using Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Brink.DataLake.Lambda
{
    public class Function
    {

        public async Task FunctionHandler(Stream inputStream, ILambdaContext context)
        {
            try
            {
                var host = await LambdaHost.GetOrCreateAsync(
                    () => LambdaHost.CreateBuilder().UseStartup<Startup>().BuildAsync(context));

                host.SetContext(context);

                var generator = host.ServiceProvider.GetRequiredService<ISnapshotCommandGenerator>();

                await generator.GeneratorSnapshotCommandsAsync();
            }
            catch (Exception e)
            {
                context.Logger.LogLine(e.ToString());
            }
        }
    }
}
