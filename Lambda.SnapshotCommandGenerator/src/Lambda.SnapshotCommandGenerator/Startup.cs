﻿using Amazon.DynamoDBv2;
using Amazon.SQS;
using Brink.Aws.Lambda;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services;
using Brink.Extensions.Configuration.Bootstrap;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.Lambda.Common.ConfigurationKeys;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator
{
    public class Startup : ILambdaStartup
    {
        public void ConfigureServices(IServiceCollection services, BootstrapConfiguration bootstrapConfiguration,
            IConfiguration configuration)
        {
            services
                .Configure<BusinessDateTrackerOptions>(i => i.TableName = configuration[DynamoDbTableName])
                .PostConfigure<BusinessDateTrackerOptions>(i => i.Validate())
                .Configure<CommandDispatcherOptions>(i => i.QueueName = configuration[Tier2CommandQueueName])
                .PostConfigure<CommandDispatcherOptions>(i => i.Validate())
                .Configure<Tier2CommandGeneratorTrackerOptions>(i => i.TableName = configuration[DynamoDbTableName])
                .PostConfigure<Tier2CommandGeneratorTrackerOptions>(i => i.Validate())
                .AddAWSService<IAmazonDynamoDB>()
                .AddAWSService<IAmazonSQS>()
                .AddSingleton<IBusinessDateTracker, BusinessDateTracker>()
                .AddSingleton<ISnapshotCommandGenerator, Services.SnapshotCommandGenerator>()
                .AddSingleton<ICommandDispatcher, CommandDispatcher>()
                .AddSingleton<ITier2CommandGeneratorTracker, Tier2CommandGeneratorTracker>()
                .AddSingleton<ITier2KeyPrefixProvider, Tier2KeyPrefixProvider>();
        }

        public async Task<IDictionary<string, string>> GetAdditionalConfigurationAsync(
            LambdaStartupConfigurationContext context)
        {
            var parameters = new ParameterStoreConfigurationParameters()
                .AddConfigurationKey(context.EnvironmentPrefix, DynamoDbTableName)
                .AddConfigurationKey(context.EnvironmentPrefix, Tier2CommandQueueName);

            return await context.ParameterStoreConfigurationProvider.GetConfigurationAsync(parameters);
        }
    }
}
