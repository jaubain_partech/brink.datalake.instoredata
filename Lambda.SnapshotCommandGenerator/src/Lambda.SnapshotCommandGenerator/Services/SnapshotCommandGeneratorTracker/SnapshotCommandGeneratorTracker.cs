﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.Lambda.Common.Services.DynamoDbConstants;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public class Tier2CommandGeneratorTracker : DynamoRepository, ITier2CommandGeneratorTracker
    {
        private const string LastInvokedTimeAttribute = "LastInvokedTime";

        private readonly Dictionary<string, AttributeValue> _lastInvokedKey;
        private readonly Tier2CommandGeneratorTrackerOptions _options;

        protected override string TableName => _options.TableName;

        public Tier2CommandGeneratorTracker(IOptions<Tier2CommandGeneratorTrackerOptions> options,
            IAmazonDynamoDB dynamoDb, ILogger<Tier2CommandGeneratorTracker> logger)
            : base(dynamoDb, logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));

            _lastInvokedKey = new Dictionary<string, AttributeValue>
            {
                [PartitionKey] = new AttributeValue("Tier2CommandGeneratorTracker"),
                [SortKey] = new AttributeValue("LastInvokedTime")
            };
        }

        public async Task<DateTimeOffset> GetLastInvokedTimeAsync()
        {
            var request = new GetItemRequest
            {
                Key = _lastInvokedKey,
                TableName = TableName
            };

            var response = await DynamoDb.GetItemAsync(request);

            return response.IsItemSet
                ? DateTimeOffset.Parse(response.Item[LastInvokedTimeAttribute].S)
                : DateTimeOffset.MinValue;
        }

        public async Task SetLastInvokedTime(DateTimeOffset time)
        {
            var item = new Dictionary<string, AttributeValue>(_lastInvokedKey);
            item[LastInvokedTimeAttribute] = new AttributeValue(DateTimeOffset.UtcNow.ToString("O"));

            var request = new PutItemRequest
            {
                Item = item,
                TableName = TableName
            };

            await DynamoDb.PutItemAsync(request);
        }
    }
}
