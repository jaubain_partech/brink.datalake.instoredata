﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public class Tier2CommandGeneratorTrackerOptions
    {
        public string TableName { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(TableName))
            {
                throw new InvalidOperationException("A valid table name must be specified.");
            }
        }
    }
}
