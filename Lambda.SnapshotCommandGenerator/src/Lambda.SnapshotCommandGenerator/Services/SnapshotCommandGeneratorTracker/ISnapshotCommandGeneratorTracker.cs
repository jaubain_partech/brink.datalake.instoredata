﻿using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public interface ITier2CommandGeneratorTracker
    {
        Task<DateTimeOffset> GetLastInvokedTimeAsync();

        Task SetLastInvokedTime(DateTimeOffset timestamp);
    }
}
