﻿using Brink.DataLake.InStoreData.Lambda.Common.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public class SnapshotCommandGenerator : ISnapshotCommandGenerator
    {
        private readonly IBusinessDateTracker _businessDateTracker;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ITier2CommandGeneratorTracker _commandGeneratorTracker;
        private readonly ILogger<SnapshotCommandGenerator> _logger;

        public SnapshotCommandGenerator(IBusinessDateTracker businessDateTracker, 
            ICommandDispatcher commandDispatcher, ITier2CommandGeneratorTracker commandGeneratorTracker,
            ILogger<SnapshotCommandGenerator> logger)
        {
            _businessDateTracker = businessDateTracker ?? throw new ArgumentNullException(nameof(businessDateTracker));
            _commandDispatcher = commandDispatcher ?? throw new ArgumentNullException(nameof(commandDispatcher));
            _commandGeneratorTracker = commandGeneratorTracker
                ?? throw new ArgumentNullException(nameof(commandGeneratorTracker));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task GeneratorSnapshotCommandsAsync()
        {
            var now = DateTimeOffset.Now;

            var lastInvoked = await _commandGeneratorTracker.GetLastInvokedTimeAsync();

            _logger.LogInformation($"Last invoked: {lastInvoked:O}");

            var locationBusinessDates = await _businessDateTracker.GetLocationBusinessDateTrackingAsync(lastInvoked);

            var dispatcherTasks = locationBusinessDates
                .Select(i => _commandDispatcher.SendCommandAsync(i.LocationId, i.BusinessDate))
                .ToArray();

            await Task.WhenAll(dispatcherTasks);

            await _commandGeneratorTracker.SetLastInvokedTime(now);
        }
    }
}
