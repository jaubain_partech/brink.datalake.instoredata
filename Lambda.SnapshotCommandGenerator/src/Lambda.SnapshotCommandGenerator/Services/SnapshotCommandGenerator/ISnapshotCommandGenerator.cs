﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public interface ISnapshotCommandGenerator
    {
        Task GeneratorSnapshotCommandsAsync();
    }
}
