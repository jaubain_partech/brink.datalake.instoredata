﻿using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public interface ICommandDispatcher
    {
        Task SendCommandAsync(Guid locationId, DateTime businessDate);
    }
}
