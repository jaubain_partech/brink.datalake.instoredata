﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Brink.DataLake.InStoreData.Lambda.Common;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public class CommandDispatcher : ICommandDispatcher
    {
        private readonly Lazy<string> _queueUrl;
        private readonly ILogger<CommandDispatcher> _logger;
        private readonly CommandDispatcherOptions _options;
        private readonly IAmazonSQS _sqs;

        private string QueueUrl => _queueUrl.Value;

        public CommandDispatcher(IOptions<CommandDispatcherOptions> options, IAmazonSQS sqs,
            ILogger<CommandDispatcher> logger)
        {
            _sqs = sqs ?? throw new ArgumentNullException(nameof(sqs));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));

            _queueUrl = new Lazy<string>(GetQueueUrl);
        }

        private string GetQueueUrl()
        {
            var queueUrl = _sqs.GetQueueUrlAsync(new GetQueueUrlRequest { QueueName = _options.QueueName })
                .GetAwaiter().GetResult().QueueUrl;

            if (queueUrl == null)
            {
                throw new InvalidOperationException($"Unable to determine URL for queue '{_options.QueueName}'.");
            }

            return queueUrl;
        }

        public async Task SendCommandAsync(Guid locationId, DateTime businessDate)
        {
            _logger.LogInformation($"Sending command for {locationId} | {businessDate:yyyy-MM-dd}");

            try
            {
                var message = new ProcessLocationDataCommand
                {
                    BusinessDate = businessDate,
                    LocationId = locationId
                };

                var request = new SendMessageRequest
                {
                    MessageBody = JsonConvert.SerializeObject(message),
                    QueueUrl = QueueUrl
                };

                await _sqs.SendMessageAsync(request);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Unhandled exception sending command.");
            }
        }
    }
}
