﻿using System;

namespace Brink.DataLake.InStoreData.Lambda.SnapshotCommandGenerator.Services
{
    public class CommandDispatcherOptions
    {      
        public string QueueName { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(QueueName))
            {
                throw new InvalidOperationException("A valid queue name must be specified.");
            }
        }
    }
}
