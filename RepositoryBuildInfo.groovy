// Returns a list of ignored project file names.
def getIgnoredProjectFileNames() {
    return null
}

// Returns a map of regions (paths) to build jobs. 
def getRegionToBuildJobMap() {
    def regionToBuildJobMap = [:]
    regionToBuildJobMap['.*Contracts/.*'] = 'Contracts'
    regionToBuildJobMap['.*TerminalComponents/.*'] = 'TerminalComponents'
    regionToBuildJobMap['.*Lambda.Common/.*'] = 'Lambda.Common'
    regionToBuildJobMap['.*Lambda.IngestProcessor/.*'] = 'Lambda.IngestProcessor'
    regionToBuildJobMap['.*Lambda.SnapshotCommandGenerator/.*'] = 'Lambda.SnapshotCommandGenerator'
    regionToBuildJobMap['.*Lambda.SnapshotGenerator/.*'] = 'Lambda.SnapshotGenerator'

    return regionToBuildJobMap
}

return this