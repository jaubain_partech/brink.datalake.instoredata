﻿using Amazon.KinesisFirehose;
using Amazon.KinesisFirehose.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class FirehoseDataLakeRecordSenderTests
    {
        private readonly FirehoseDataLakeRecordSender _classUnderTest;

        private readonly Mock<IAmazonKinesisFirehose> _firehoseMock;
        private readonly Mock<ILogger<FirehoseDataLakeRecordSender>> _loggerMock;
        private readonly FirehoseDataLakeRecordSenderOptions _options;

        public FirehoseDataLakeRecordSenderTests()
        {
            _firehoseMock = new Mock<IAmazonKinesisFirehose>(MockBehavior.Strict);
            _loggerMock = new Mock<ILogger<FirehoseDataLakeRecordSender>>(MockBehavior.Loose);
            _options = new FirehoseDataLakeRecordSenderOptions { DeliveryStreamName = "TheStream" };

            _classUnderTest = new FirehoseDataLakeRecordSender(Options.Create(_options), _firehoseMock.Object,
                _loggerMock.Object);
        }

        [Fact]
        public async Task SendRecordAsync_ShouldThrow_WhenRecordValueIsNull()
        {
            // Act
            var exception = await Xunit.Record.ExceptionAsync(() => _classUnderTest.SendRecordAsync(null));

            // Assert
            Assert.IsType<ArgumentNullException>(exception);
        }

        [Fact]
        public async Task SendRecordAsync_SendRecordToKinesis()
        {
            // Arrange
            var record = new DataLakeRecord(new Persistence.OutboxRecord { Id = 1 }, new byte[] { 1, 2, 3 });

            _firehoseMock
                .Setup(i => i.PutRecordAsync(It.Is<PutRecordRequest>(j => j.DeliveryStreamName == _options.DeliveryStreamName
                    && j.Record.Data != null), default))
                .ReturnsAsync(new PutRecordResponse());

            // Act
            await _classUnderTest.SendRecordAsync(record);

            // Assert
            _firehoseMock
                .Verify(i => i.PutRecordAsync(It.Is<PutRecordRequest>(j => j.DeliveryStreamName == _options.DeliveryStreamName
                    && j.Record.Data != null), default), Times.Once);
        }

    }
}
