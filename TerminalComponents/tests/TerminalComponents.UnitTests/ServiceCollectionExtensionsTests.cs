using Brink.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence.UnitTests
{
    public class ServiceCollectionExtensionsTests
    {
        private const string ConnectionStringName = "TestConnection";

        [Fact]
        public void ShouldResolve_ConnectionFactory()
        {
            // Act
            BuildServiceProvider().GetRequiredService<ISqliteConnectionFactory>();
        }

        [Fact]
        public void ShouldResolve_OutboxPopulationManager()
        {
            // Act
            BuildServiceProvider().GetRequiredService<IOutboxPopulationManager>();
        }

        [Fact]
        public void ShouldResolve_OutboxRepository()
        {
            // Act
            BuildServiceProvider().GetRequiredService<IOutboxRepository>();
        }

        [Fact]
        public void ShouldResolve_SchemaMigrator()
        {
            // Act
            BuildServiceProvider().GetRequiredService<ISchemaMigrator>();
        }

        [Fact]
        public void ShouldThrow_WhenConnectionStringIsMissingOrInvalid()
        {
            // Arrange
            var serviceProvider = BuildServiceProvider(false);

            // Act
            var exception = Record.Exception(() => serviceProvider.GetRequiredService<ISqliteConnectionFactory>());

            // Assert
            Assert.IsType<InvalidOperationException>(exception);
        }

        private IServiceProvider BuildServiceProvider(bool includeConnectionString = true)
        {
            var collection = new ServiceCollection()
                .AddInStoreDataPersistence(ConnectionStringName)
                .AddLogging()
                .AddSingleton(new Mock<IInStoreDataProvider>().Object)
                .AddOutboxPopulator();

            var configurationBuilder = new ConfigurationBuilder();

            if (includeConnectionString)
            {
                var configDictionary = new Dictionary<string, string>
                {
                    [$"ConnectionStrings:{ConnectionStringName}"] = "someConnectionString"
                };

                configurationBuilder.AddInMemoryCollection(configDictionary);
            }

            collection.AddSingleton<IConfiguration>(configurationBuilder.Build());

            return collection.BuildServiceProvider();
        }
    }
}
