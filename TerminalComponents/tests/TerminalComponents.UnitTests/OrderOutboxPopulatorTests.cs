﻿using Brink.DataLake.InStoreData.Contracts;
using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class OrderOutboxPopulatorTests
    {
        private readonly OrderOutboxPopulator _classUnderTest;

        private readonly Mock<IOutboxDataEncoder> _dataEncoderMock;
        private readonly Mock<IInStoreDataProvider> _dataProviderMock;
        private readonly Mock<ILogger<OrderOutboxPopulator>> _loggerMock;
        private OrderOutboxPopulatorOptions _options;
        private readonly Mock<IOutboxRepository> _repositoryMock;

        public OrderOutboxPopulatorTests(ITestOutputHelper outputHelper)
        {
            _dataEncoderMock = new Mock<IOutboxDataEncoder>(MockBehavior.Strict);
            _dataProviderMock = new Mock<IInStoreDataProvider>(MockBehavior.Strict);
            _loggerMock = new Mock<ILogger<OrderOutboxPopulator>>(MockBehavior.Loose).SetOutputHelper(outputHelper);
            _options = new OrderOutboxPopulatorOptions();
            _repositoryMock = new Mock<IOutboxRepository>(MockBehavior.Strict);

            _classUnderTest = new OrderOutboxPopulator(Options.Create(_options), _dataProviderMock.Object,
                _repositoryMock.Object, _dataEncoderMock.Object, _loggerMock.Object);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task PopulateOutboxAsync_ShouldRetrieveAndSetLastModifiedTime(bool previousRecordExists)
        {
            // Arrange
            var previousRecord = previousRecordExists
                ? new OutboxRecord { CreatedTime = DateTimeOffset.Now }
                : null;

            SetupRepositoryGet(previousRecord);

            if (previousRecordExists)
            {
                _dataProviderMock
                    .Setup(i => i.GenerateSnapshot(It.Is<InStoreDataSnapshotOptions>(j => j.DataTypes == InStoreDataTypes.Orders
                        && j.OrdersLastModifiedTime == previousRecord.CreatedTime)))
                    .Returns(new InStoreDataSnapshot());
            }
            else
            {
                _dataProviderMock
                    .Setup(i => i.GenerateSnapshot(It.Is<InStoreDataSnapshotOptions>(j => j.DataTypes == InStoreDataTypes.Orders
                        && j.OrdersLastModifiedTime == null)))
                    .Returns(new InStoreDataSnapshot());
            }

            // Act
            var result = await _classUnderTest.PopulateOutboxAsync();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task PopulateOutboxAsync_ShouldReturnTrue_WhenNoUpdateOccurs()
        {
            // Arrange
            SetupRepositoryGet(null);
            SetupDataProvider(new InStoreDataSnapshot());

            // Act
            var result = await _classUnderTest.PopulateOutboxAsync();

            // Assert
            Assert.True(result);

            _dataProviderMock.Verify(i => i.GenerateSnapshot(It.IsAny<InStoreDataSnapshotOptions>()), Times.Once);
        }

        [Fact]
        public async Task PopulateOutboxAsync_ShouldAddRecordAndReturnTrue_WhenUpdateOccurs()
        {
            // Arrange
            SetupRepositoryGet(null);

            var snapshot = new InStoreDataSnapshot
            {
                BusinessDate = DateTime.Now.Date,
                Orders = new List<Order>
                {
                    new Order { ModifiedTime = DateTimeOffset.Now },
                    new Order { ModifiedTime = DateTimeOffset.Now.AddMinutes(-1) }
                }
            };

            SetupDataProvider(snapshot);

            var encodedData = "encodedData";
            _dataEncoderMock.Setup(i => i.EncodeDataAsync(It.IsAny<object>())).ReturnsAsync(encodedData);

            _repositoryMock
                          .Setup(i => i.InsertRecordAsync(It.Is<OutboxRecord>(j => j.BusinessDate == snapshot.BusinessDate
                    && j.CreatedTime == snapshot.Orders[0].ModifiedTime && j.Data == encodedData && j.DataType == OutboxDataType.Orders
                    && !string.IsNullOrWhiteSpace(j.Data))))
                .Returns(Task.CompletedTask);

            // Act
            var result = await _classUnderTest.PopulateOutboxAsync();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task PopulateOutboxAsync_ShouldReturnFalseAndLogException_WhenAnExceptionIsThrown()
        {
            // Arrange
            var exception = new Exception();

            _repositoryMock
                .Setup(i => i.GetMostRecentRecordAsync(OutboxDataType.Orders))
                .Throws(exception);

            // Act
            var result = await _classUnderTest.PopulateOutboxAsync();

            // Assert
            Assert.False(result);

            _loggerMock.Verify(i => i.Log(LogLevel.Critical, 0, It.IsAny<object>(), exception,
                It.IsAny<Func<object, Exception, string>>()), Times.Once);
        }

        private void SetupDataProvider(InStoreDataSnapshot snapshot)
        {
            _dataProviderMock
                .Setup(i => i.GenerateSnapshot(It.Is<InStoreDataSnapshotOptions>(j => j.DataTypes == InStoreDataTypes.Orders
                    && j.OrdersLastModifiedTime == null)))
                .Returns(snapshot);
        }

        private void SetupRepositoryGet(OutboxRecord record)
        {
            _repositoryMock
                .Setup(i => i.GetMostRecentRecordAsync(OutboxDataType.Orders))
                .ReturnsAsync(record);
        }
    }
}
