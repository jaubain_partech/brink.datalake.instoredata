﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class OutboxPopulationManagerTests
    {
        private readonly OutboxPopulationManager _classUnderTest;
        private readonly Mock<ILogger<OutboxPopulationManager>> _loggerMock;
        private readonly OutboxPopulationManagerOptions _options;
        private readonly Mock<IOutboxPopulator> _populatorMock;
        private readonly Mock<ISystemClock> _systemClockMock;

        public OutboxPopulationManagerTests(ITestOutputHelper outputHelper)
        {
            _loggerMock = new Mock<ILogger<OutboxPopulationManager>>(MockBehavior.Loose).SetOutputHelper(outputHelper);
            _options = new OutboxPopulationManagerOptions();
            _populatorMock = new Mock<IOutboxPopulator>(MockBehavior.Strict);
            _systemClockMock = new Mock<ISystemClock>(MockBehavior.Strict);

            _classUnderTest = new OutboxPopulationManager(Options.Create(_options),
                new[] { _populatorMock.Object }, _systemClockMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task UpdateOutboxAsync_ShouldNotInvokePopulatorBeforeNextScheduledUpdate_WhenForceFlagIsFalse()
        {
            // Arrange
            var now = DateTimeOffset.UtcNow;
            _systemClockMock.Setup(i => i.UtcNow).Returns(now);
            _classUnderTest.OutboxPopulators.Single().NextUpdate = now.AddMinutes(1);

            // Act
            await _classUnderTest.UpdateOutboxAsync(false);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task UpdateOutboxAsync_ShouldInvokePopulator_WhenNextUpdateIsScheduledAndForceFlagIsFalse(bool populatorResult)
        {
            // Arrange
            var now = DateTimeOffset.UtcNow;
            var populationInterval = TimeSpan.FromMinutes(1);
            _systemClockMock.Setup(i => i.UtcNow).Returns(now);
            _classUnderTest.OutboxPopulators.Single().NextUpdate = now.AddMinutes(-1);
            _populatorMock.Setup(i => i.PopulationInterval).Returns(populationInterval);
            _populatorMock.Setup(i => i.PopulateOutboxAsync()).ReturnsAsync(populatorResult);

            // Act
            var result = await _classUnderTest.UpdateOutboxAsync(false);

            // Assert
            Assert.Equal(populatorResult, result);

            if (result)
            {
                Assert.Equal(now.Add(populationInterval), _classUnderTest.OutboxPopulators.Single().NextUpdate);
                _loggerMock.Verify(i => i.Log(LogLevel.Information, 0, It.IsAny<object>(), null,
                    It.IsAny<Func<object, Exception, string>>()), Times.Once);
            }
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task UpdateOutboxAsync_ShouldInvokePopulatorBeforeNextScheduledUpdate_WhenForceFlagIsTrue(bool populatorResult)
        {
            // Arrange
            var now = DateTimeOffset.UtcNow;
            var populationInterval = TimeSpan.FromMinutes(1);
            _systemClockMock.Setup(i => i.UtcNow).Returns(now);
            _classUnderTest.OutboxPopulators.Single().NextUpdate = now.AddMinutes(10);
            _populatorMock.Setup(i => i.PopulationInterval).Returns(populationInterval);
            _populatorMock.Setup(i => i.PopulateOutboxAsync()).ReturnsAsync(populatorResult);

            // Act
            var result = await _classUnderTest.UpdateOutboxAsync(true);

            // Assert
            Assert.Equal(populatorResult, result);

            if (result)
            {
                Assert.Equal(now.Add(populationInterval), _classUnderTest.OutboxPopulators.Single().NextUpdate);
                _loggerMock.Verify(i => i.Log(LogLevel.Information, 0, It.IsAny<object>(), null,
                    It.IsAny<Func<object, Exception, string>>()), Times.Once);
            }
        }

        [Fact]
        public async Task UpdateOutboxAsync_ShouldCatchAndLogPopulatorExceptions()
        {
            // Arrange
            var now = DateTimeOffset.UtcNow;
            var populationInterval = TimeSpan.FromMinutes(1);
            var exception = new Exception();
            _systemClockMock.Setup(i => i.UtcNow).Returns(now);
            _classUnderTest.OutboxPopulators.Single().NextUpdate = now.AddMinutes(-1);
            _populatorMock.Setup(i => i.PopulateOutboxAsync()).Throws(exception);

            // Act
            var result = await _classUnderTest.UpdateOutboxAsync(false);

            // Assert
            Assert.False(result);
            _loggerMock.Verify(i => i.Log(LogLevel.Critical, 0, It.IsAny<object>(), exception,
                It.IsAny<Func<object, Exception, string>>()), Times.Once);

        }
    }
}
