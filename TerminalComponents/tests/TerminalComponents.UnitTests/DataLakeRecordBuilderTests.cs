﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Moq;
using System;
using System.Text;
using Xunit;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class DataLakeRecordBuilderTests
    {
        private readonly DataLakeRecordBuilder _classUnderTest;

        private readonly Mock<ILocationIdProvider> _locationIdProviderMock;

        public DataLakeRecordBuilderTests()
        {
            _locationIdProviderMock = new Mock<ILocationIdProvider>(MockBehavior.Strict);

            _classUnderTest = new DataLakeRecordBuilder(_locationIdProviderMock.Object);
        }

        [Fact]
        public void BuildRecord_ShouldThrow_WhenRecordIsNull()
        {
            // Act
            var exception = Record.Exception(() => _classUnderTest.BuildRecord(null));

            // Assert
            Assert.IsType<ArgumentNullException>(exception);
        }

        [Fact]
        public void BuildRecord_ShouldBuildRecord()
        {
            // Arrange
            var locationId = Guid.NewGuid();
            _locationIdProviderMock.Setup(i => i.LocationId).Returns(locationId);

            var businessDate = DateTime.Now.Date;
            var createdTime = DateTimeOffset.Now;
            var data = "1234";

            var outboxRecord = new OutboxRecord
            {
                BusinessDate = businessDate,
                CreatedTime = createdTime,
                Data = data
            };

            // Act
            var dataLakeRecord = _classUnderTest.BuildRecord(outboxRecord);

            // Assert
            Assert.Equal(outboxRecord, dataLakeRecord.Source);

            var dataLakeRecordAsString = Encoding.UTF8.GetString(dataLakeRecord.Value);

            var tokens = dataLakeRecordAsString.Split(',', 4);

            Assert.Equal(locationId.ToString("N"), tokens[0]);
            Assert.Equal(businessDate.ToString("yyyy-MM-dd"), tokens[1]);
            Assert.Equal(createdTime.ToString("O"), tokens[2]);
            Assert.Equal(data + '\n', tokens[3]);
        }
    }
}
