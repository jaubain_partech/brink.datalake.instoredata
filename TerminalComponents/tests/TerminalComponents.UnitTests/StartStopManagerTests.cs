﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Xunit;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class StartStopManagerTests : IDisposable
    {
        private StartStopManager _classUnderTest;

        private readonly ILogger _logger;
        private readonly TimeSpan _workDelay;
        private int _workCounter;

        public StartStopManagerTests(ITestOutputHelper outputHelper)
        {
            _workDelay = TimeSpan.FromMilliseconds(50);
            _logger = new XunitLogger(outputHelper, nameof(StartStopManagerTests));

            _classUnderTest = new StartStopManager(DoWorkAsync, _logger);
        }

        [Fact]
        public void Start_ShouldReturnTrue_WhenSuccessfullyStarted()
        {
            // Act
            var started = _classUnderTest.Start();

            // Assert
            Assert.True(started);
        }

        [Fact]
        public void Start_ShouldReturnFalse_WhenAlreadyStarted()
        {
            // Arrange
            var started1 = _classUnderTest.Start();

            // Act
            var started2 = _classUnderTest.Start();

            // Assert
            Assert.True(started1);
            Assert.False(started2);
        }

        [Fact]
        public void Stop_ShouldReturnTrue_WhenSuccessfullyStopped()
        {
            // Arrange
            _classUnderTest.Start();

            // Act
            var stopped = _classUnderTest.Stop();

            // Assert
            Assert.True(stopped);
        }

        [Fact]
        public void Stop_ShouldReturnFalse_WhenAlreadyStopped()
        {
            // Act
            var stopped = _classUnderTest.Stop();

            // Assert
            Assert.False(stopped);
        }

        [Fact]
        public void ShouldRestartSuccessfully()
        {
            //Arrange
            _classUnderTest.Start();
            _classUnderTest.Stop();

            while (_classUnderTest.IsStarted)
            {
                Thread.Sleep(_workDelay);
            }

            // Act
            var started = _classUnderTest.Start();

            while (_workCounter <= 1)
            {
                Thread.Sleep(_workDelay);
            }

            // Assert
            Assert.True(started);
            Assert.Equal(2, _workCounter);
        }

        private async Task DoWorkAsync(CancellationToken cancellationToken)
        {
            _workCounter++;

            do
            {
                await Task.Delay(_workDelay);
            }
            while (!cancellationToken.IsCancellationRequested);

            _logger.LogInformation($"Returning from {nameof(DoWorkAsync)}.");
        }

        public void Dispose()
        {
            _classUnderTest?.Stop();
        }
    }
}
