﻿using Brink.DataLake.InStoreData.Contracts;
using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class CompositeDataOutboxPopulatorTests
    {
        private CompositeDataOutboxPopulator _classUnderTest;

        private readonly Mock<IOutboxDataEncoder> _dataEncoderMock;
        private readonly Mock<IInStoreDataProvider> _dataProviderMock;
        private readonly Mock<ILogger<CompositeDataOutboxPopulator>> _loggerMock;
        private readonly CompositeDataOutboxPopulatorOptions _options;
        private readonly Mock<IOutboxRepository> _repositoryMock;
        private readonly Mock<ISystemClock> _systemClockMock;

        public CompositeDataOutboxPopulatorTests(ITestOutputHelper outputHelper)
        {
            _dataEncoderMock = new Mock<IOutboxDataEncoder>(MockBehavior.Strict);
            _dataProviderMock = new Mock<IInStoreDataProvider>(MockBehavior.Strict);
            _loggerMock = new Mock<ILogger<CompositeDataOutboxPopulator>>(MockBehavior.Loose).SetOutputHelper(outputHelper);
            _options = new CompositeDataOutboxPopulatorOptions();
            _repositoryMock = new Mock<IOutboxRepository>(MockBehavior.Strict);
            _systemClockMock = new Mock<ISystemClock>(MockBehavior.Strict);

            _classUnderTest = new CompositeDataOutboxPopulator(Options.Create(_options), _dataProviderMock.Object,
                _repositoryMock.Object, _dataEncoderMock.Object, _systemClockMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task PopulateOutboxAsync_ShouldAddRecordAndReturnTrue_WhenUpdateOccurs()
        {
            // Arrange
            var snapshot = new InStoreDataSnapshot
            {
                BusinessDate = DateTime.Now.Date,
                CashDrawers = new List<CashDrawer>
                {
                    new CashDrawer()
                }
            };

            SetupDataProvider(snapshot);

            var now = DateTimeOffset.UtcNow;
            _systemClockMock.Setup(i => i.UtcNow).Returns(now);

            var encodedData = "encodedData";
            _dataEncoderMock.Setup(i => i.EncodeDataAsync(It.IsAny<object>())).ReturnsAsync(encodedData);

            _repositoryMock
                .Setup(i => i.InsertRecordAsync(It.Is<OutboxRecord>(j => j.BusinessDate == snapshot.BusinessDate
                    && j.Data == encodedData && j.DataType == OutboxDataType.Composite && j.CreatedTime == now)))
                .Returns(Task.CompletedTask);

            // Act
            var result = await _classUnderTest.PopulateOutboxAsync();

            // Assert
            Assert.True(result);

            _repositoryMock
                .Verify(i => i.InsertRecordAsync(It.Is<OutboxRecord>(j => j.BusinessDate == snapshot.BusinessDate
                    && j.DataType == OutboxDataType.Composite && j.CreatedTime == now)), Times.Once);
        }

        [Fact]
        public async Task PopulateOutboxAsync_ShouldReturnTrue_WhenNoUpdateOccurs()
        {
            // Arrange
            SetupDataProvider(new InStoreDataSnapshot());

            // Act
            var result = await _classUnderTest.PopulateOutboxAsync();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task PopulateOutboxAsync_ShouldReturnFalseAndLogException_WhenAnExceptionIsThrown()
        {
            // Arrange
            var exception = new Exception();

            _dataProviderMock
                .Setup(i => i.GenerateSnapshot(It.IsAny<InStoreDataSnapshotOptions>()))
                .Throws(exception);

            // Act
            var result = await _classUnderTest.PopulateOutboxAsync();

            // Assert
            Assert.False(result);

            _loggerMock.Verify(i => i.Log(LogLevel.Critical, 0, It.IsAny<object>(), exception,
                It.IsAny<Func<object, Exception, string>>()), Times.Once);
        }

        private void SetupDataProvider(InStoreDataSnapshot snapshot)
        {
            var types = InStoreDataTypes.CashDrawers | InStoreDataTypes.Employees
                | InStoreDataTypes.Items | InStoreDataTypes.Parties | InStoreDataTypes.Tills;

            _dataProviderMock
                .Setup(i => i.GenerateSnapshot(It.Is<InStoreDataSnapshotOptions>(j => j.DataTypes == types)))
                .Returns(snapshot);
        }
    }
}
