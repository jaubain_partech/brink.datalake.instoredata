﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class OutboxQueryServiceTests
    {
        private readonly OutboxQueryService _classUnderTest;

        private Mock<ILogger<OutboxQueryService>> _loggerMock;
        private Mock<IOutboxRepository> _repositoryMock;

        public OutboxQueryServiceTests()
        {
            _loggerMock = new Mock<ILogger<OutboxQueryService>>(MockBehavior.Loose);
            _repositoryMock = new Mock<IOutboxRepository>(MockBehavior.Strict);

            _classUnderTest = new OutboxQueryService(_repositoryMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task ShouldReturnNull_WhenNoRecordOfAnyTypeIsAvailable()
        {
            // Arrange
            var types = (OutboxDataType[])Enum.GetValues(typeof(OutboxDataType));

            foreach (var type in types)
            {
                _repositoryMock.Setup(i => i.GetNextRecordAsync(type)).ReturnsAsync((OutboxRecord)null);
            }

            // Act
            var record = await _classUnderTest.GetNextRecordAsync();

            // Assert
            Assert.Null(record);

            foreach (var type in types)
            {
                _repositoryMock.Verify(i => i.GetNextRecordAsync(type), Times.Once);
            }
        }

        [Fact]
        public async Task ShouldInvokeRepositoryWithDifferentTypes_UntilRecordIsReturned()
        {
            // Arrange
            var record = new OutboxRecord();
            _repositoryMock.Setup(i => i.GetNextRecordAsync(OutboxDataType.Composite)).ReturnsAsync((OutboxRecord)null);
            _repositoryMock.Setup(i => i.GetNextRecordAsync(OutboxDataType.Orders)).ReturnsAsync(record);

            // Act
            var retrievedRecord = await _classUnderTest.GetNextRecordAsync();

            // Assert
            Assert.Equal(record, retrievedRecord);

            _repositoryMock.Verify(i => i.GetNextRecordAsync(OutboxDataType.Composite), Times.Once);
            _repositoryMock.Verify(i => i.GetNextRecordAsync(OutboxDataType.Orders), Times.Once);
        }

        [Fact]
        public async Task ShouldInvokeRepositoryWithSameType_WhenPriorCallToRepositoryReturedRecord()
        {
            // Arrange
            var record = new OutboxRecord();
            _repositoryMock.Setup(i => i.GetNextRecordAsync(OutboxDataType.Composite)).ReturnsAsync((OutboxRecord)null);
            _repositoryMock.Setup(i => i.GetNextRecordAsync(OutboxDataType.Orders)).ReturnsAsync(record);

            // Act
            var retrievedRecord = await _classUnderTest.GetNextRecordAsync();
            retrievedRecord = await _classUnderTest.GetNextRecordAsync();

            // Assert
            Assert.Equal(record, retrievedRecord);

            _repositoryMock.Verify(i => i.GetNextRecordAsync(OutboxDataType.Composite), Times.Once);
            _repositoryMock.Verify(i => i.GetNextRecordAsync(OutboxDataType.Orders), Times.Exactly(2));
        }
    }
}
