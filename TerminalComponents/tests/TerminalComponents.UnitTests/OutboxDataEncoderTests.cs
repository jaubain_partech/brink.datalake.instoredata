﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class OutboxDataEncoderTests
    {
        private OutboxDataEncoder _classUnderTest = new OutboxDataEncoder();

        [Fact]
        public async Task DecodeDataAsync_ShouldThrow_WhenCompressedDataIsNull()
        {
            // Act
            var exception = await Record.ExceptionAsync(() => _classUnderTest.DecodeDataAsync(null));

            // Assert
            Assert.IsType<ArgumentNullException>(exception);
        }

        [Fact]
        public async Task EncodeDataAsync_ShouldThrow_WhenCompressedDataIsNull()
        {
            // Act
            var exception = await Record.ExceptionAsync(() => _classUnderTest.EncodeDataAsync(null));

            // Assert
            Assert.IsType<ArgumentNullException>(exception);
        }

        [Fact]
        public async Task DataShouldRoundTrip()
        {
            // Arrange
            var originalObject = new
            {
                IntValue = 1,
                StringValue = "the string"
            };

            // Act
            var encodedData = await _classUnderTest.EncodeDataAsync(originalObject);
            var decodedObject = await _classUnderTest.DecodeDataAsync(encodedData);

            // Assert
            Assert.Equal(originalObject.IntValue, decodedObject["intValue"].ToObject<int>());
            Assert.Equal(originalObject.StringValue, decodedObject["stringValue"].ToObject<string>());
        }
    }
}
