﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public class DataLakeSendManagerTests
    {
        private readonly DataLakeSendManager _classUnderTest;

        private readonly Mock<IOutboxCommandService> _commandServiceMock;
        private readonly Mock<IDataLakeRecordBuilder> _dataLakeRecordBuilderMock;
        private readonly Mock<ILogger<DataLakeSendManager>> _loggerMock;
        private readonly DataLakeSendManagerOptions _options;
        private readonly Mock<IOutboxQueryService> _queryServiceMock;
        private readonly Mock<IDataLakeRecordSenderFactory> _senderFactoryMock;

        public DataLakeSendManagerTests()
        {
            _commandServiceMock = new Mock<IOutboxCommandService>(MockBehavior.Strict);
            _dataLakeRecordBuilderMock = new Mock<IDataLakeRecordBuilder>(MockBehavior.Strict);
            _loggerMock = new Mock<ILogger<DataLakeSendManager>>(MockBehavior.Loose);
            _options = new DataLakeSendManagerOptions();
            _queryServiceMock = new Mock<IOutboxQueryService>(MockBehavior.Strict);
            _senderFactoryMock = new Mock<IDataLakeRecordSenderFactory>(MockBehavior.Strict);

            _classUnderTest = new DataLakeSendManager(Options.Create(_options), _dataLakeRecordBuilderMock.Object,
                _senderFactoryMock.Object, _queryServiceMock.Object, _commandServiceMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task SendRecordAsync_ShouldReturnFalse_WhenExceptionIsCaught()
        {
            // Arrange
            var record = new OutboxRecord();
            var exception = new Exception();

            _dataLakeRecordBuilderMock.Setup(i => i.BuildRecord(record)).Throws(exception);
            _loggerMock.Setup(i => i.Log(LogLevel.Critical, 0, It.IsAny<object>(), exception,
                It.IsAny<Func<object, Exception, string>>()));

            // Act
            var result = await _classUnderTest.SendRecordAsync(record);

            // Assert
            Assert.False(result);
            _loggerMock.Verify(i => i.Log(LogLevel.Critical, 0, It.IsAny<object>(), exception,
                It.IsAny<Func<object, Exception, string>>()), Times.Once);
        }

        [Fact]
        public async Task SendRecordAsync_ShouldReturnTrue_whenRecordIsSent()
        {
            // Arrange
            var outboxRecord = new OutboxRecord();
            var lakeRecord = new DataLakeRecord(outboxRecord, new byte[0]);

            _dataLakeRecordBuilderMock.Setup(i => i.BuildRecord(outboxRecord)).Returns(lakeRecord);

            var senderMock = new Mock<IDataLakeRecordSender>(MockBehavior.Strict);
            _senderFactoryMock.Setup(i => i.CreateSender(lakeRecord)).Returns(senderMock.Object);

            senderMock.Setup(i => i.SendRecordAsync(lakeRecord)).Returns(Task.CompletedTask);

            _commandServiceMock.Setup(i => i.MarkRecordSentAsync(outboxRecord)).Returns(Task.CompletedTask);

            // Act
            var result = await _classUnderTest.SendRecordAsync(outboxRecord);

            // Assert
            Assert.True(result);

            _dataLakeRecordBuilderMock.Verify(i => i.BuildRecord(outboxRecord), Times.Once);
            _senderFactoryMock.Verify(i => i.CreateSender(lakeRecord), Times.Once);
            senderMock.Verify(i => i.SendRecordAsync(lakeRecord), Times.Once);
            _commandServiceMock.Verify(i => i.MarkRecordSentAsync(outboxRecord), Times.Once);
        }
    }
}
