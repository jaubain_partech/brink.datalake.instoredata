﻿using Microsoft.Extensions.Logging;
using Moq;
using System;
using Xunit.Abstractions;

namespace Brink.DataLake.InStoreData.TerminalComponents.UnitTests
{
    public static class LoggerMockExtensions
    {
        public static Mock<ILogger<T>> SetOutputHelper<T>(this Mock<ILogger<T>> loggerMock, ITestOutputHelper outputHelper)
        {
            Action<LogLevel, EventId, object, Exception, Func<object, Exception, string>> callback = (l, e, o, x, f) =>
                WriteLogMessage(l, e, o, x, f, outputHelper);

            loggerMock.Setup(i => i.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(), It.IsAny<object>(),
                    It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()))
                .Callback(callback);

            return loggerMock;
        }

        private static void WriteLogMessage(LogLevel level, EventId eventId, object state, Exception exception,
            Func<object, Exception, string> formatter, ITestOutputHelper outputHelper)
        {
            var message = $"[{level}] {formatter.Invoke(state, exception)}";

            if (exception != null)
            {
                message += $"{Environment.NewLine}{exception.ToString()}";
            }

            outputHelper.WriteLine(message);
        }
    }
}
