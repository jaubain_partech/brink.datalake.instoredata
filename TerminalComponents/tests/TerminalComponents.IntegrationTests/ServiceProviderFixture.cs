﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Brink.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.TerminalComponents.IntegrationTests
{
    public class ServiceProviderFixture
    {
        private const string ConnectionStringName = "PersistenceIntegrationTests";

        public IConfiguration Configuration { get; }

        public IServiceProvider ServiceProvider { get; }

        public ServiceProviderFixture()
        {
            Configuration = BuildConfiguration();
            ServiceProvider = BuildServiceProvider();

            ServiceProvider.GetRequiredService<ISchemaMigrator>()
                .EnsureSchemaIsCurrentAsync().GetAwaiter().GetResult();
        }

        private IConfiguration BuildConfiguration()
        {
            var configurationDictionary = new Dictionary<string, string>
            {
                [$"ConnectionStrings:{ConnectionStringName}"] = "Data Source=InStoreData.db;Version=3"
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configurationDictionary)
                .Build();

            return configuration;
        }

        private IServiceProvider BuildServiceProvider()
        {
            var collection = new ServiceCollection()
                .AddSingleton(Configuration)
                .AddInStoreDataPersistence(ConnectionStringName);

            return collection.BuildServiceProvider();
        }

        public void ClearOutboxTable()
        {
            using (var connection = ServiceProvider.GetRequiredService<ISqliteConnectionFactory>().Create())
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = $"DELETE FROM {OutboxRepository.TableName}";
                command.ExecuteNonQuery();
            }
        }
    }
}
