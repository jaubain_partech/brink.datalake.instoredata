﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.TerminalComponents.IntegrationTests
{
    public class LifoRecordDataGenerator : List<object[]>
    {
        public LifoRecordDataGenerator()
        {
            var businessDate1 = DateTime.Now.Date;
            var businessDate2 = DateTime.Now.Date.AddDays(1);
            var businessDate3 = DateTime.Now.Date.AddDays(2);

            var test1 = new object[]
            {
                new OutboxRecord[]
                {
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "1", DateTimeOffset.Now, DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "2", DateTimeOffset.Now.AddMinutes(1), DateTimeOffset.Now),
                    new OutboxRecord(businessDate2, OutboxDataType.Composite, "3", DateTimeOffset.Now),
                    new OutboxRecord(businessDate2, OutboxDataType.Composite, "4", DateTimeOffset.Now.AddMinutes(1)), // Correct
                    new OutboxRecord(businessDate3, OutboxDataType.Composite, "5", DateTimeOffset.Now),
                    new OutboxRecord(businessDate3, OutboxDataType.Composite, "6", DateTimeOffset.Now.AddMinutes(1))
                },
                3
            };

            var test2 = new object[]
            {
                new OutboxRecord[]
                {
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "1", DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "2", DateTimeOffset.Now.AddMinutes(1)) // Correct
                },
                1
            };

            var test3 = new object[]
            {
                new OutboxRecord[]
                {
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "1", DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "2", DateTimeOffset.Now.AddMinutes(1), DateTimeOffset.Now),
                    new OutboxRecord(businessDate2, OutboxDataType.Composite, "3", DateTimeOffset.Now),
                    new OutboxRecord(businessDate2, OutboxDataType.Composite, "4", DateTimeOffset.Now.AddMinutes(1), DateTimeOffset.Now),
                    new OutboxRecord(businessDate3, OutboxDataType.Composite, "5", DateTimeOffset.Now),
                    new OutboxRecord(businessDate3, OutboxDataType.Composite, "6", DateTimeOffset.Now.AddMinutes(1))  // Correct
                },
                5
            };

            var test4 = new object[]
            {
                new OutboxRecord[]
                {
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "1", DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "2", DateTimeOffset.Now.AddMinutes(1)),  // Correct
                    new OutboxRecord(businessDate2, OutboxDataType.Composite, "3", DateTimeOffset.Now),
                    new OutboxRecord(businessDate2, OutboxDataType.Composite, "4", DateTimeOffset.Now.AddMinutes(1)),
                    new OutboxRecord(businessDate3, OutboxDataType.Composite, "5", DateTimeOffset.Now),
                    new OutboxRecord(businessDate3, OutboxDataType.Composite, "6", DateTimeOffset.Now.AddMinutes(1))
                },
                1
            };

            var test5 = new object[]
            {
                new OutboxRecord[]
                {
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "1", DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "2", DateTimeOffset.Now.AddMinutes(1), DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "3", DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Composite, "4", DateTimeOffset.Now.AddMinutes(1)) // Correct
                },
                3
            };

            var test6 = new object[]
            {
                new OutboxRecord[]
                {
                    new OutboxRecord(businessDate1, OutboxDataType.Orders, "1", DateTimeOffset.Now),
                    new OutboxRecord(businessDate1, OutboxDataType.Orders, "2", DateTimeOffset.Now.AddMinutes(1)),
                    new OutboxRecord(businessDate2, OutboxDataType.Orders, "3", DateTimeOffset.Now.AddMinutes(3)),
                    new OutboxRecord(businessDate2, OutboxDataType.Composite, "4", DateTimeOffset.Now.AddMinutes(2)) // Correct
                },
                3
            };

            Add(test1);
            Add(test2);
            Add(test3);
            Add(test4);
            Add(test5);
            Add(test6);
        }
    }
}
