using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using KellermanSoftware.CompareNetObjects;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Brink.DataLake.InStoreData.TerminalComponents.IntegrationTests
{
    public class OutboxRepositoryTests : IClassFixture<ServiceProviderFixture>, IDisposable
    {
        private readonly OutboxRepository _classUnderTest;

        private readonly IServiceProvider _serviceProvider;
        private readonly ServiceProviderFixture _serviceProviderFixture;

        public OutboxRepositoryTests(ServiceProviderFixture serviceProviderFixture)
        {
            _serviceProviderFixture = serviceProviderFixture;
            _serviceProvider = serviceProviderFixture.ServiceProvider;
            _classUnderTest = (OutboxRepository)_serviceProvider.GetRequiredService<IOutboxRepository>();
        }

        public void Dispose()
        {
            _serviceProviderFixture.ClearOutboxTable();
        }

        [Fact]
        public async Task GetMostRecentRecordAsync_ShouldReturnMostRecentRecord()
        {
            // Arrange
            var records = BuildOutboxRecords(2);
            records[0].DataType = OutboxDataType.Composite;
            records[1].DataType = OutboxDataType.Orders;
            await _classUnderTest.InsertRecordAsync(records[0]);
            await _classUnderTest.InsertRecordAsync(records[1]);

            // Act
            var mostRecordRecord = await _classUnderTest.GetMostRecentRecordAsync(OutboxDataType.Composite);

            // Assert
            Assert.True(records[1].CreatedTime > records[0].CreatedTime);
            AssertEqual(records[0], mostRecordRecord);
        }

        [Fact]
        public async Task GetMostRecentRecordAsync_ShouldReturnNullWhenNoRecordsExist()
        {
            // Arrange
            var record = BuildOutboxRecords(1)[0];
            record.DataType = OutboxDataType.Composite;
            await _classUnderTest.InsertRecordAsync(record);

            // Act
            var mostRecordRecord = await _classUnderTest.GetMostRecentRecordAsync(OutboxDataType.Orders);

            // Assert
            Assert.Null(mostRecordRecord);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task GetNextFifoRecordAsync_ShouldReturnNextRecord(bool hasSentRecords)
        {
            // Arrange
            var records = BuildOutboxRecords(2);

            if (hasSentRecords)
            {
                records[0].SentTime = DateTimeOffset.Now;
            }

            await _classUnderTest.InsertRecordAsync(records[0]);
            await _classUnderTest.InsertRecordAsync(records[1]);

            // Act
            var nextRecord = await _classUnderTest.GetNextFifoRecordAsync(OutboxDataType.Composite);

            // Assert
            AssertEqual(hasSentRecords ? records[1] : records[0], nextRecord);
        }

        [Fact]
        public async Task GetNextFifoRecordAsync_ShouldReturnNullWhenAllRecordsAreSent()
        {
            // Arrange
            var record = BuildOutboxRecords(1)[0];
            record.SentTime = DateTimeOffset.Now;
            await _classUnderTest.InsertRecordAsync(record);

            // Act
            var nextRecord = await _classUnderTest.GetNextFifoRecordAsync(OutboxDataType.Composite);

            // Assert
            Assert.Null(nextRecord);
        }

        [Fact]
        public async Task GetNextFifoRecordAsync_ShouldReturnNullWhenNoRecordsExist()
        {
            // Act
            var nextRecord = await _classUnderTest.GetNextFifoRecordAsync(OutboxDataType.Composite);

            // Assert
            Assert.Null(nextRecord);
        }

        [Theory]
        [ClassData(typeof(LifoRecordDataGenerator))]
        public async Task GetNextLifoRecordAsync_ShouldReturnCorrectRecord(OutboxRecord[] records, int targetRecordIndex)
        {
            // Arrange
            await InsertRecordsAsync(records);

            // Act
            var retrievedRecord = await _classUnderTest.GetNextLifoRecordAsync(OutboxDataType.Composite);

            // Assert
            AssertEqual(records[targetRecordIndex], retrievedRecord);
        }

        [Fact]
        public async Task GetNextLifoRecordAsync_ShouldReturnNull_WhenMostRecentRecordIsSent()
        {
            // Arrange
            var businessDate = DateTime.Now.Date;

            var records = new[]
            {
                new OutboxRecord(businessDate, OutboxDataType.Composite, "1", DateTimeOffset.Now),
                new OutboxRecord(businessDate, OutboxDataType.Composite, "2", DateTimeOffset.Now.AddMinutes(1), DateTimeOffset.Now)
            };

            await InsertRecordsAsync(records);

            // Act
            var retrievedRecord = await _classUnderTest.GetNextLifoRecordAsync(OutboxDataType.Composite);

            // Assert
            Assert.Null(retrievedRecord);
        }

        [Fact]
        public async Task InsertRecordAsync_ShouldCreateRecord()
        {
            // Arrange
            var originalRecord = BuildOutboxRecords(1)[0];

            // Act
            await _classUnderTest.InsertRecordAsync(originalRecord);

            // Assert
            var retrievedRecord = await _classUnderTest.GetNextRecordAsync(OutboxDataType.Composite);
            AssertEqual(originalRecord, retrievedRecord);
        }

        [Fact]
        public async Task MarkRecordSentAsync_ShouldUpdateRecord()
        {
            // Arrange
            var record = BuildOutboxRecords(1)[0];
            await _classUnderTest.InsertRecordAsync(record);
            var retrievedRecord = await _classUnderTest.GetNextRecordAsync(OutboxDataType.Composite);

            // Act
            await _classUnderTest.MarkRecordSentAsync(retrievedRecord.Id, DateTimeOffset.Now);

            // Assert
            retrievedRecord = await _classUnderTest.GetNextRecordAsync(OutboxDataType.Composite);
            Assert.Null(retrievedRecord);
        }

        [Fact]
        public async Task UpdateRecordAsync_ShouldUpdateRecord()
        {
            // Arrange
            var record = BuildOutboxRecords(1)[0];
            await _classUnderTest.InsertRecordAsync(record);

            var retrievedRecord1 = await _classUnderTest.GetNextRecordAsync(OutboxDataType.Composite);
            retrievedRecord1.Data = "Updated data";

            // Act
            await _classUnderTest.UpdateRecordAsync(retrievedRecord1);

            // Assert
            var retrievedRecord2 = await _classUnderTest.GetNextRecordAsync(OutboxDataType.Composite);
            AssertEqual(retrievedRecord1, retrievedRecord2);
        }

        private void AssertEqual(OutboxRecord expected, OutboxRecord actual, bool compareId = false)
        {
            var config = new ComparisonConfig();

            if (!compareId)
            {
                config.MembersToIgnore.Add(nameof(expected.Id));
            }

            var result = new CompareLogic(config).Compare(expected, actual);

            Assert.True(result.AreEqual, result.DifferencesString);
        }

        private OutboxRecord[] BuildOutboxRecords(int count)
        {
            var records = new OutboxRecord[count];

            for (var index = 0; index < count; index++)
            {
                var record = new OutboxRecord
                {
                    BusinessDate = DateTime.Now.Date,
                    CreatedTime = DateTimeOffset.Now.AddMinutes(index),
                    Data = "DATA" + index,
                    DataType = OutboxDataType.Composite
                };

                records[index] = record;
            }

            return records;
        }

        private async Task InsertRecordsAsync(IEnumerable<OutboxRecord> records)
        {
            foreach(var record in records)
            {
                await _classUnderTest.InsertRecordAsync(record);
            }
        }
    }
}
