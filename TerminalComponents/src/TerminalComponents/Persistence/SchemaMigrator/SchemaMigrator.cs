﻿using Dapper;
using System;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.TerminalComponents.Persistence.OutboxColumnNames;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class SchemaMigrator : ISchemaMigrator
    {
        private readonly ISqliteConnectionFactory _sqliteConnectionFactory;

        public SchemaMigrator(ISqliteConnectionFactory sqliteConnectionFactory)
        {
            _sqliteConnectionFactory = sqliteConnectionFactory
                ?? throw new ArgumentNullException(nameof(sqliteConnectionFactory));
        }

        public async Task EnsureSchemaIsCurrentAsync()
        {
            using (var connection = _sqliteConnectionFactory.Create())
            {
                await connection.OpenAsync();

                await connection.ExecuteAsync(new CommandDefinition(
                    $"CREATE TABLE IF NOT EXISTS [{OutboxRepository.TableName}] ("
                    + $"[{Id}] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                    + $"[{BusinessDate}] DATE NOT NULL,"
                    + $"[{CreatedTime}] NVARCHAR(40) NOT NULL,"
                    + $"[{Data}] TEXT NOT NULL,"
                    + $"[{DataType}] NVARCHAR(100) NOT NULL,"
                    + $"[{SentTime}] NVARCHAR(40) NULL)"
                ));
            }
        }
    }
}
