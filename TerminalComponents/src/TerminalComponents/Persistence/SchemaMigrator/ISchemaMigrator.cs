﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public interface ISchemaMigrator
    {
        Task EnsureSchemaIsCurrentAsync();
    }
}
