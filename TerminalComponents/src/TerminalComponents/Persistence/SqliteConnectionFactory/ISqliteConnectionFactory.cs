﻿using System.Data.SQLite;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public interface ISqliteConnectionFactory
    {
        SQLiteConnection Create();
    }
}