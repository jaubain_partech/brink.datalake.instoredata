﻿using Microsoft.Extensions.Options;
using System;
using System.Data.SQLite;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class SqliteConnectionFactory : ISqliteConnectionFactory
    {
        private readonly SqliteConnectionFactoryOptions _options;

        public SqliteConnectionFactory(IOptions<SqliteConnectionFactoryOptions> options)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public SQLiteConnection Create() => new SQLiteConnection(_options.ConnectionString);
    }
}
