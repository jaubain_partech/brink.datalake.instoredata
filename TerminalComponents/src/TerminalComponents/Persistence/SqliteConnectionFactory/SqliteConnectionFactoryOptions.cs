﻿namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class SqliteConnectionFactoryOptions
    {
        public string ConnectionString { get; set; }
    }
}
