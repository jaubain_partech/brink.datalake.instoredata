﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class SqliteConnectionFactoryOptionsConfigurer : IConfigureOptions<SqliteConnectionFactoryOptions>,
        IPostConfigureOptions<SqliteConnectionFactoryOptions>
    {
        private readonly IConfiguration _configuration;
        private readonly InStoreDataPersistenceOptions _options;

        public SqliteConnectionFactoryOptionsConfigurer(IConfiguration configuration,
            IOptions<InStoreDataPersistenceOptions> options)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public void Configure(SqliteConnectionFactoryOptions options)
        {
            var connectionString = _configuration.GetConnectionString(_options.ConnectionStringName);

            if (!string.IsNullOrWhiteSpace(connectionString))
            {
                options.ConnectionString = connectionString;
            }
        }

        public void PostConfigure(string name, SqliteConnectionFactoryOptions options)
        {
            if (string.IsNullOrWhiteSpace(options.ConnectionString))
            {
                throw new InvalidOperationException("Invalid connection string.");
            }
        }
    }
}
