﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class OutboxRecord
    {
        public DateTime BusinessDate { get; set; }

        public DateTimeOffset CreatedTime { get; set; }

        public string Data { get; set; }

        public OutboxDataType DataType { get; set; }

        public long Id { get; set; }

        public DateTimeOffset? SentTime { get; set; }

        public OutboxRecord()
        {
        }

        public OutboxRecord(DateTime businessDate, OutboxDataType dataType, string data, 
            DateTimeOffset? createdTime = null, DateTimeOffset? sentTime = null)
        {
            BusinessDate = businessDate;
            DataType = dataType;
            Data = data;
            CreatedTime = createdTime ?? DateTimeOffset.Now;
            SentTime = sentTime;
        }
    }
}
