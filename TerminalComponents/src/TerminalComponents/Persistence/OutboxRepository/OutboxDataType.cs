﻿namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public enum OutboxDataType
    {
        Composite,
        Orders
    }
}
