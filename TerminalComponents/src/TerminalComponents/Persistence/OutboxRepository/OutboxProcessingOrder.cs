﻿namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public enum OutboxProcessingOrder
    {
        Fifo,
        Lifo
    }
}
