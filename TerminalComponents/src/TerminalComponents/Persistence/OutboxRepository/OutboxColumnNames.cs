﻿using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public static class OutboxColumnNames
    {
        public const string BusinessDate = nameof(BusinessDate);

        public const string CreatedTime = nameof(CreatedTime);

        public const string Data = nameof(Data);

        public const string DataType = nameof(DataType);

        public const string Id = nameof(Id);

        public const string SentTime = nameof(SentTime);

        public static IReadOnlyList<string> AllColumns { get; }

        static OutboxColumnNames()
        {
            AllColumns = new List<string>
            {
                BusinessDate,
                CreatedTime,
                Data,
                DataType,
                Id,
                SentTime
            };
        }
    }
}
