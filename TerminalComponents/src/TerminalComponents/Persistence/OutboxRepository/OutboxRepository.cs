﻿using Dapper;
using System;
using System.Data.SQLite;
using System.Threading.Tasks;
using static Brink.DataLake.InStoreData.TerminalComponents.Persistence.OutboxColumnNames;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class OutboxRepository : IOutboxRepository
    {
        public const string TableName = "Outbox";

        private readonly ISqliteConnectionFactory _connectionFactory;

        public OutboxRepository(ISqliteConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
        }

        private object BuildCommandParameters(OutboxRecord record)
        {
            return new
            {
                BusinessDate = record.BusinessDate.Date,
                CreatedTime = record.CreatedTime.ToString("O"),
                record.Data,
                DataType = record.DataType.ToString(),
                record.Id,
                SentTime = record?.SentTime?.ToString("O")
            };
        }

        public Task<OutboxRecord> GetMostRecentRecordAsync(OutboxDataType dataType)
        {
            var commandText = $"SELECT * FROM [{TableName}] WHERE {DataType} = @{DataType} ORDER BY {CreatedTime} DESC LIMIT 1";

            return QueryRecordAsync(commandText,
                command => command.Parameters.AddWithValue(DataType, dataType.ToString()));
        }

        public Task<OutboxRecord> GetNextFifoRecordAsync(OutboxDataType dataType)
        {
            var commandText = $"SELECT * FROM [{TableName}] "
                + $"WHERE {DataType} = @{DataType} AND {SentTime} IS NULL "
                + $"ORDER BY {CreatedTime} LIMIT 1";

            return QueryRecordAsync(commandText,
                command => command.Parameters.AddWithValue(DataType, dataType.ToString()));
        }

        public Task<OutboxRecord> GetNextLifoRecordAsync(OutboxDataType dataType)
        {
            // For each business date, if the newest record has not been sent return it; otherwise return null.
            // Out of that list of records, return the one with the earliest business date.

            var commandText = $"SELECT o.{Id}, MAX(o.{CreatedTime}) AS {CreatedTime}, o.{BusinessDate}, o.{DataType}, o.{Data}, o.{SentTime} FROM {TableName} o "
                + $"JOIN (SELECT MAX(y.{CreatedTime}) as MaxCreatedTime, y.{BusinessDate} from {TableName} y WHERE y.{DataType} = @{DataType} GROUP by y.{BusinessDate}) z on z.{BusinessDate} = o.{BusinessDate} AND z.MaxCreatedTime = o.{CreatedTime} "
                + $"WHERE o.SentTime IS NULL AND o.DataType = @{DataType} "
                + $"GROUP BY o.{BusinessDate}, o.{CreatedTime} "
                + $"ORDER BY o.{BusinessDate} LIMIT 1";

            return QueryRecordAsync(commandText,
                command => command.Parameters.AddWithValue(DataType, dataType.ToString()));
        }

        public Task<OutboxRecord> GetNextRecordAsync(OutboxDataType dataType)
        {
            var processingMethod = GetProcessingOrder(dataType);

            switch (processingMethod)
            {
                case OutboxProcessingOrder.Fifo:
                    return GetNextFifoRecordAsync(dataType);

                case OutboxProcessingOrder.Lifo:
                    return GetNextLifoRecordAsync(dataType);

                default:
                    throw new NotSupportedException(processingMethod.ToString());
            }
        }

        public OutboxProcessingOrder GetProcessingOrder(OutboxDataType dataType)
        {
            switch (dataType)
            {
                case OutboxDataType.Composite:
                    return OutboxProcessingOrder.Lifo;

                case OutboxDataType.Orders:
                    return OutboxProcessingOrder.Fifo;

                default:
                    throw new NotSupportedException(dataType.ToString());
            }
        }

        public async Task InsertRecordAsync(OutboxRecord record)
        {
            var commandParameters = BuildCommandParameters(record);
            var commandText = $"INSERT INTO [{TableName}] ({BusinessDate}, {CreatedTime}, {Data}, {DataType}, {SentTime}) "
                + $"VALUES (@{BusinessDate}, @{CreatedTime}, @{Data}, @{DataType}, @{SentTime})";
            var commandDefinition = new CommandDefinition(commandText, commandParameters);

            using (var connection = _connectionFactory.Create())
            {
                await connection.ExecuteAsync(commandDefinition);
            }
        }

        public async Task MarkRecordSentAsync(long recordId, DateTimeOffset sentTime)
        {
            var commandParameters = new { SentTime = sentTime.ToString("O"), Id = recordId };
            var commandText = $"UPDATE [{TableName}] SET {SentTime} = @{SentTime} WHERE {Id} = @{Id}";
            var commandDefinition = new CommandDefinition(commandText, commandParameters);

            using (var connection = _connectionFactory.Create())
            {
                await connection.ExecuteAsync(commandDefinition);
            }
        }

        private async Task<OutboxRecord> QueryRecordAsync(string commandText, Action<SQLiteCommand> configureCommand = null)
        {
            using (var connection = _connectionFactory.Create())
            using (var command = connection.CreateCommand())
            {
                await connection.OpenAsync();

                command.CommandText = commandText;
                configureCommand?.Invoke(command);

                using (var reader = await command.ExecuteReaderAsync())
                {
                    if (!await reader.ReadAsync())
                    {
                        return null;
                    }

                    var record = new OutboxRecord
                    {
                        BusinessDate = (DateTime)reader[BusinessDate],
                        CreatedTime = DateTimeOffset.Parse((string)reader[CreatedTime]),
                        Data = (string)reader[Data],
                        DataType = (OutboxDataType)Enum.Parse(typeof(OutboxDataType), (string)reader[DataType]),
                        Id = (long)reader[Id]
                    };

                    var sentTime = reader[SentTime];

                    if (sentTime != DBNull.Value)
                    {
                        record.SentTime = DateTimeOffset.Parse((string)sentTime);
                    }

                    return record;
                }
            }
        }

        public async Task UpdateRecordAsync(OutboxRecord record)
        {
            if (record.Id == 0)
            {
                throw new InvalidOperationException("ID is required for upsert.");
            }

            var commandParameters = BuildCommandParameters(record);
            var commandText = $"UPDATE [{TableName}] SET "
                + $"{BusinessDate} = @{BusinessDate}, "
                + $"{CreatedTime} = @{CreatedTime}, "
                + $"{Data} = @{Data}, "
                + $"{DataType} = @{DataType}, "
                + $"{SentTime} = @{SentTime} "
                + $"WHERE {Id} = @{Id}";
            var commandDefinition = new CommandDefinition(commandText, commandParameters);

            using (var connection = _connectionFactory.Create())
            {
                await connection.ExecuteAsync(commandDefinition);
            }
        }
    }
}
