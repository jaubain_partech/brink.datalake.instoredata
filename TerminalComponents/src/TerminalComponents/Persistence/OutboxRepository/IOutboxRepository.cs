﻿using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public interface IOutboxRepository
    {
        Task<OutboxRecord> GetMostRecentRecordAsync(OutboxDataType dataType);

        Task<OutboxRecord> GetNextRecordAsync(OutboxDataType dataType);

        Task InsertRecordAsync(OutboxRecord record);

        Task MarkRecordSentAsync(long recordId, DateTimeOffset sentTime);

        Task UpdateRecordAsync(OutboxRecord record);
    }
}
