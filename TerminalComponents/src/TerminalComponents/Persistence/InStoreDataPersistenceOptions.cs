﻿namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class InStoreDataPersistenceOptions
    {
        public string ConnectionStringName { get; set; }
    }
}
