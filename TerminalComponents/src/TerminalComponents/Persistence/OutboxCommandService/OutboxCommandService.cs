﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class OutboxCommandService : IOutboxCommandService
    {
        private readonly ILogger<OutboxCommandService> _logger;
        private readonly IOutboxRepository _repository;

        public OutboxCommandService(IOutboxRepository repository, ILogger<OutboxCommandService> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task MarkRecordSentAsync(OutboxRecord record)
        {
            _logger.LogDebug($"Marking record sent (ID: {record.Id}).");

            await _repository.MarkRecordSentAsync(record.Id, DateTimeOffset.Now).ConfigureAwait(false);
        }
    }
}
