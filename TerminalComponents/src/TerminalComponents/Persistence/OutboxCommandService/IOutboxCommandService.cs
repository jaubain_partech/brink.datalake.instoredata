﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public interface IOutboxCommandService
    {
        Task MarkRecordSentAsync(OutboxRecord record);
    }
}
