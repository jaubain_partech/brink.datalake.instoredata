﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public class OutboxQueryService : IOutboxQueryService
    {
        private readonly LinkedList<OutboxDataType> _dataTypes;
        private LinkedListNode<OutboxDataType> _lastDataType;
        private readonly ILogger<OutboxQueryService> _logger;
        private readonly IOutboxRepository _repository;

        public OutboxQueryService(IOutboxRepository repository, ILogger<OutboxQueryService> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _dataTypes = InitializeDataTypes();
            _lastDataType = _dataTypes.First;
        }

        public async Task<OutboxRecord> GetNextRecordAsync()
        {
            var startingDataType = _lastDataType.Value;

            do
            {
                _logger.LogDebug($"Querying for record of type '{_lastDataType.Value}'");

                var record = await _repository.GetNextRecordAsync(_lastDataType.Value);

                if (record != null)
                {
                    _logger.LogDebug($"Record of type '{_lastDataType.Value}' found (ID:{{RecordId}}).", record.Id);

                    return record;
                }

                _logger.LogDebug($"No record of type '{_lastDataType.Value}' found.");

                _lastDataType = _lastDataType.Next ?? _dataTypes.First;
            }
            while (_lastDataType.Value != startingDataType);

            return null;
        }

        private LinkedList<OutboxDataType> InitializeDataTypes()
        {
            var types = (OutboxDataType[])Enum.GetValues(typeof(OutboxDataType));

            return new LinkedList<OutboxDataType>(types);
        }
    }
}
