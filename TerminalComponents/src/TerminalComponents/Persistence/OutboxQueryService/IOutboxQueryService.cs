﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents.Persistence
{
    public interface IOutboxQueryService
    {
        Task<OutboxRecord> GetNextRecordAsync();
    }
}
