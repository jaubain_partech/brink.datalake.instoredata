﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface ISystemClock
    {
        DateTimeOffset UtcNow { get; }
    }
}
