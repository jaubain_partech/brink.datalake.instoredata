﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class StartStopManager
    {
        private const string InvalidStartRequestMessage = "Start requested but when already started.";
        private const string InvalidStopRequestMessage = "Stop requested when not started.";

        private readonly ILogger _logger;
        private readonly object _startLock;
        private readonly Func<CancellationToken, Task> _workDelegate;

        public CancellationTokenSource CancellationTokenSource { get; private set; }

        public bool IsStarted => CancellationTokenSource != null;

        public StartStopManager(Func<CancellationToken, Task> workDelegate, ILogger logger)
        {
            _workDelegate = workDelegate ?? throw new ArgumentNullException(nameof(workDelegate));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _startLock = new object();
        }

        public bool Start()
        {
            if (IsStarted)
            {
                _logger.LogWarning(InvalidStartRequestMessage);

                return false;
            }

            lock (_startLock)
            {
                if (IsStarted)
                {
                    _logger.LogWarning(InvalidStartRequestMessage);

                    return false;
                }

                _logger.LogInformation("Starting work delegate.");

                CancellationTokenSource = new CancellationTokenSource();

                Task.Run(() => _workDelegate(CancellationTokenSource.Token))
                    .ContinueWith(
                        i =>
                        {
                            CancellationTokenSource.Dispose();
                            CancellationTokenSource = null;

                            _logger.LogInformation("Work delegate stopped.");
                        }
                    );

                return true;
            }
        }

        public bool Stop()
        {
            if (!IsStarted)
            {
                _logger.LogWarning(InvalidStopRequestMessage);

                return false;
            }

            lock (_startLock)
            {
                if (!IsStarted)
                {
                    _logger.LogWarning(InvalidStopRequestMessage);

                    return false;
                }

                _logger.LogInformation("Stopping work delegate.");

                CancellationTokenSource.Cancel();

                return true;
            }
        }
    }
}
