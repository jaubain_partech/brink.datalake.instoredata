﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IOutboxPopulationManager
    {
        void Start();

        void Stop();

        Task<bool> UpdateOutboxAsync();
    }
}
