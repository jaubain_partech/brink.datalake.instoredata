﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class OutboxPopulationManager : IOutboxPopulationManager
    {
        private readonly ILogger<OutboxPopulationManager> _logger;
        private readonly OutboxPopulationManagerOptions _options;
        private readonly StartStopManager _startStopManager;
        private readonly ISystemClock _systemClock;

        public IReadOnlyList<OutboxPopulatorTuple> OutboxPopulators { get; }

        public OutboxPopulationManager(IOptions<OutboxPopulationManagerOptions> options, IEnumerable<IOutboxPopulator> outboxPopulators,
            ISystemClock systemClock, ILogger<OutboxPopulationManager> logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));

            if (outboxPopulators == null)
            {
                throw new ArgumentNullException(nameof(outboxPopulators));
            }

            OutboxPopulators = outboxPopulators.Select(i => new OutboxPopulatorTuple(i)).ToList();
            _systemClock = systemClock ?? throw new ArgumentNullException(nameof(systemClock));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _startStopManager = new StartStopManager(PopulateOutboxLoopAsync, _logger);
        }

        private async Task PopulateOutboxLoopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Waiting on initial delay of {_options.InitialDelay}.");

            await Task.Delay(_options.InitialDelay);

            while (!cancellationToken.IsCancellationRequested)
            {
                await UpdateOutboxAsync(false);

                await Task.Delay(TimeSpan.FromSeconds(5));
            }
        }

        public void Start() => _startStopManager.Start();

        public void Stop() => _startStopManager.Stop();

        public Task<bool> UpdateOutboxAsync() => UpdateOutboxAsync(true);

        public async Task<bool> UpdateOutboxAsync(bool forceUpdate)
        {
            var result = true;
            var now = _systemClock.UtcNow;

            foreach (var tuple in OutboxPopulators)
            {
                var shouldUpdate = forceUpdate ? true : now >= tuple.NextUpdate;

                if (!shouldUpdate)
                {
                    continue;
                }

                try
                {
                    var populatorResult = await tuple.Populator.PopulateOutboxAsync();

                    if (!populatorResult)
                    {
                        result = false;

                        continue;
                    }

                    tuple.NextUpdate = now + tuple.PopulationInterval;

                    _logger.LogInformation($"Next update for {tuple.Populator.GetType().Name} at {tuple.NextUpdate}.");
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, "Outbox population failure.");

                    result = false;
                }
            }

            return result;
        }

        public class OutboxPopulatorTuple
        {
            public DateTimeOffset NextUpdate { get; set; }

            public TimeSpan PopulationInterval => Populator.PopulationInterval;

            public IOutboxPopulator Populator { get; }

            public OutboxPopulatorTuple(IOutboxPopulator populator)
            {
                Populator = populator ?? throw new ArgumentNullException(nameof(populator));
            }
        }
    }
}
