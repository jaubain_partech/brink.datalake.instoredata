﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class OutboxPopulationManagerOptions
    {
        public TimeSpan InitialDelay { get; set; } = TimeSpan.FromSeconds(15);

        public void Validate()
        {
            if (InitialDelay < TimeSpan.Zero)
            {
                throw new InStoreDataException($"{nameof(InitialDelay)} cannot less than zero.");
            }
        }
    }
}
