﻿using Brink.DataLake.InStoreData.TerminalComponents;
using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Brink.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInStoreDataPersistence(this IServiceCollection serviceCollection,
            string connectionStringName = "InStoreDataOutbox")
        {
            return serviceCollection
                .AddOptions()
                .Configure<InStoreDataPersistenceOptions>(i => i.ConnectionStringName = connectionStringName)
                .AddSingleton<IConfigureOptions<SqliteConnectionFactoryOptions>, SqliteConnectionFactoryOptionsConfigurer>()
                .AddSingleton<IPostConfigureOptions<SqliteConnectionFactoryOptions>, SqliteConnectionFactoryOptionsConfigurer>()
                .AddSingleton<ISqliteConnectionFactory, SqliteConnectionFactory>()
                .AddSingleton<ISchemaMigrator, SchemaMigrator>()
                .AddSingleton<IOutboxRepository, OutboxRepository>()
                .AddSingleton<IOutboxQueryService, OutboxQueryService>()
                .AddSingleton<IOutboxCommandService, OutboxCommandService>();
        }

        public static IServiceCollection AddOutboxPopulator(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddOptions()
                .PostConfigure<OutboxPopulationManagerOptions>(i => i.Validate())
                .AddSingleton<IOutboxDataEncoder, OutboxDataEncoder>()
                .AddSingleton<IOutboxPopulationManager, OutboxPopulationManager>()
                .AddSingleton<IOutboxPopulator, CompositeDataOutboxPopulator>()
                .AddSingleton<IOutboxPopulator, OrderOutboxPopulator>()
                .AddSingleton<ISystemClock, SystemClock>();
        }
        public static IServiceCollection AddOutboxSender(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddOptions()
                .PostConfigure<DataLakeSendManagerOptions>(i => i.Validate())
                .PostConfigure<FirehoseDataLakeRecordSenderOptions>(i => i.Validate())
                .PostConfigure<S3DataLakeRecordSenderOptions>(i => i.Validate())
                .AddTransient<FirehoseDataLakeRecordSender>()
                .AddTransient<S3DataLakeRecordSender>()
                .AddSingleton<IDataLakeRecordBuilder, DataLakeRecordBuilder>()
                .AddSingleton<IDataLakeRecordSenderFactory, DataLakeRecordSenderFactory>()
                .AddSingleton<IDataLakeSendManager, DataLakeSendManager>();
        }
    }
}
