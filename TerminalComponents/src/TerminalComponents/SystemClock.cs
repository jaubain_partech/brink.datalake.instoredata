﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class SystemClock : ISystemClock
    {
        public DateTimeOffset UtcNow => DateTimeOffset.UtcNow;
    }
}
