﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class InStoreDataSnapshotOptions
    {
        public InStoreDataTypes DataTypes { get; set; }

        public DateTimeOffset? OrdersLastModifiedTime { get; set; }
    }
}
