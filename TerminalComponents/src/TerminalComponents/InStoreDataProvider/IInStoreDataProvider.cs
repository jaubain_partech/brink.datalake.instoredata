﻿namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IInStoreDataProvider
    {
        InStoreDataSnapshot GenerateSnapshot(InStoreDataSnapshotOptions options);
    }
}
