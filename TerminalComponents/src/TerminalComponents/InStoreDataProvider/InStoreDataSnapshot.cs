﻿using Brink.DataLake.InStoreData.Contracts;
using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class InStoreDataSnapshot
    {
        public DateTime BusinessDate { get; set; }

        public List<CashDrawer> CashDrawers { get; set; } = new List<CashDrawer>();

        public List<Employee> Employees { get; set; } = new List<Employee>();

        public bool IsOnlineOrderingEnabled { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        public List<Order> Orders { get; set; } = new List<Order>();

        public List<Party> Parties { get; set; } = new List<Party>();

        public List<Till> Tills { get; set; } = new List<Till>();
    }
}
