﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    [Flags]
    public enum InStoreDataTypes
    {
        CashDrawers,
        Employees,
        Items,
        Orders,
        Parties,
        Tills
    }
}
