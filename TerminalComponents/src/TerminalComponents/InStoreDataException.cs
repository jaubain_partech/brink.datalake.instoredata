﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class InStoreDataException : Exception
    {
        public InStoreDataException(string message)
            : base(message)
        {
        }

        public InStoreDataException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
