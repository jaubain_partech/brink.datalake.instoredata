﻿namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IDataLakeSendManager
    {
        void Start();

        void Stop();
    }
}
