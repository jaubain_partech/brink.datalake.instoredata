﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class DataLakeSendManager : IDataLakeSendManager
    {
        private readonly IOutboxCommandService _commandService;
        private readonly IDataLakeRecordBuilder _dataLakeRecordBuilder;
        private readonly ILogger<DataLakeSendManager> _logger;
        private readonly DataLakeSendManagerOptions _options;
        private readonly IOutboxQueryService _queryService;
        private readonly IDataLakeRecordSenderFactory _senderFactory;
        private readonly StartStopManager _startStopManager;

        public DataLakeSendManager(IOptions<DataLakeSendManagerOptions> options,
            IDataLakeRecordBuilder dataLakeRecordBuilder, IDataLakeRecordSenderFactory senderFactory,
            IOutboxQueryService queryService, IOutboxCommandService commandService, ILogger<DataLakeSendManager> logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _dataLakeRecordBuilder = dataLakeRecordBuilder
                ?? throw new ArgumentNullException(nameof(dataLakeRecordBuilder));
            _senderFactory = senderFactory ?? throw new ArgumentNullException(nameof(senderFactory));
            _queryService = queryService ?? throw new ArgumentNullException(nameof(queryService));
            _commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _startStopManager = new StartStopManager(SendOutboxLoopAsync, _logger);
        }

        private async Task SendOutboxLoopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Waiting on initial delay of {_options.InitialDelay}.");

            await Task.Delay(_options.InitialDelay).ConfigureAwait(false);

            while (!cancellationToken.IsCancellationRequested)
            {
                var record = await _queryService.GetNextRecordAsync().ConfigureAwait(false);

                if (record != null)
                {
                    _logger.LogDebug("Record retrieved ({RecordId}).", record.Id);

                    var result = await SendRecordAsync(record).ConfigureAwait(false);

                    // TODO: Track consecutive failure - add back off using Polly.

                    continue;
                }

                _logger.LogDebug("No record retrieved.");

                await Task.Delay(TimeSpan.FromSeconds(5)).ConfigureAwait(false);
            }
        }

        public async Task<bool> SendRecordAsync(OutboxRecord record)
        {
            _logger.LogDebug("Sending record ({RecordId}).", record.Id);

            try
            {
                var dataLakeRecord = _dataLakeRecordBuilder.BuildRecord(record);

                var sender = _senderFactory.CreateSender(dataLakeRecord);

                await sender.SendRecordAsync(dataLakeRecord).ConfigureAwait(false);

                await _commandService.MarkRecordSentAsync(record).ConfigureAwait(false);

                _logger.LogInformation("Sent record ({RecordId}).", record.Id);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Error sending record ({RecordId}).", record.Id);

                return false;
            }
        }

        public void Start() => _startStopManager.Start();

        public void Stop() => _startStopManager.Stop();
    }
}
