﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class CompositeDataOutboxPopulatorOptions : PopulatorOptions
    {
        public CompositeDataOutboxPopulatorOptions()
        {
            PopulationInterval = TimeSpan.FromMinutes(5);
        }
    }
}
