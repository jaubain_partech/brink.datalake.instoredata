﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class CompositeDataOutboxPopulator : IOutboxPopulator
    {
        private static readonly InStoreDataTypes _dataTypes;

        private readonly IOutboxDataEncoder _dataEncoder;
        private readonly IInStoreDataProvider _dataProvider;
        private readonly ILogger<CompositeDataOutboxPopulator> _logger;
        private readonly IOutboxRepository _repository;
        private readonly ISystemClock _systemClock;

        public TimeSpan PopulationInterval { get; }

        static CompositeDataOutboxPopulator()
        {
            _dataTypes = InStoreDataTypes.CashDrawers | InStoreDataTypes.Employees
                | InStoreDataTypes.Items | InStoreDataTypes.Parties | InStoreDataTypes.Tills;
        }

        public CompositeDataOutboxPopulator(IOptions<CompositeDataOutboxPopulatorOptions> options,
            IInStoreDataProvider dataProvider, IOutboxRepository repository, IOutboxDataEncoder dataEncoder,
            ISystemClock systemClock, ILogger<CompositeDataOutboxPopulator> logger)
        {
            if (options?.Value == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            PopulationInterval = options.Value.PopulationInterval;

            _dataProvider = dataProvider ?? throw new ArgumentNullException(nameof(dataProvider));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _dataEncoder = dataEncoder ?? throw new ArgumentNullException(nameof(dataEncoder));
            _systemClock = systemClock ?? throw new ArgumentNullException(nameof(systemClock));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        private Dictionary<string, object> BuildOutboxData(InStoreDataSnapshot snapshot)
        {
            var outboxData = new Dictionary<string, object>();

            if (snapshot.CashDrawers.Count > 0)
            {
                outboxData["cashDrawers"] = snapshot.CashDrawers;
            }

            if (snapshot.Employees.Count > 0)
            {
                outboxData["employees"] = snapshot.Employees;
            }

            if (snapshot.Items.Count > 0)
            {
                outboxData["items"] = snapshot.Items;
            }

            if (snapshot.Parties.Count > 0)
            {
                outboxData["parties"] = snapshot.Parties;
            }

            if (snapshot.Tills.Count > 0)
            {
                outboxData["tills"] = snapshot.Tills;
            }

            return outboxData;
        }

        public async Task<bool> PopulateOutboxAsync()
        {
            try
            {
                var snapshot = _dataProvider.GenerateSnapshot(
                    new InStoreDataSnapshotOptions { DataTypes = _dataTypes });

                var outboxData = BuildOutboxData(snapshot);

                if (outboxData.Count == 0)
                {
                    _logger.LogInformation("No composite data to save.");

                    return true;
                }

                _logger.LogInformation("Saving composite data to outbox.");

                var encodedData = await _dataEncoder.EncodeDataAsync(outboxData).ConfigureAwait(false);

                var record = new OutboxRecord(snapshot.BusinessDate, OutboxDataType.Composite,
                    encodedData, _systemClock.UtcNow);

                await _repository.InsertRecordAsync(record).ConfigureAwait(false);

                _logger.LogInformation("Composite data saved to outbox successfully.");

                return true;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Error saving composite data to outbox.");

                return false;
            }
        }
    }
}
