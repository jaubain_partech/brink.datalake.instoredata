﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class OutboxDataEncoder : IOutboxDataEncoder
    {
        private static readonly Encoding _encoding = new UTF8Encoding(false);
        private static readonly JsonSerializer _serializer;

        static OutboxDataEncoder()
        {
            _serializer = new JsonSerializer
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public async Task<JObject> DecodeDataAsync(string encodedData)
        {
            if (encodedData == null)
            {
                throw new ArgumentNullException(nameof(encodedData));
            }

            using (var outputStream = new MemoryStream())
            {
                using (var inputStream = new MemoryStream(Convert.FromBase64String(encodedData)))
                using (var decompressionStream = new GZipStream(inputStream, CompressionMode.Decompress))
                {
                    await decompressionStream.CopyToAsync(outputStream).ConfigureAwait(false);
                }

                outputStream.Position = 0;

                using (var reader = new StreamReader(outputStream, _encoding))
                {
                    using (var jsonReader = new JsonTextReader(reader))
                    {
                        return _serializer.Deserialize<JObject>(jsonReader);
                    }
                }
            }
        }

        public async Task<string> EncodeDataAsync(object data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream, _encoding, 1024, true))
                using (var jsonWriter = new JsonTextWriter(streamWriter))
                {
                    _serializer.Serialize(jsonWriter, data);
                }

                using (var outputStream = new MemoryStream())
                {
                    using (var gzipStream = new GZipStream(outputStream, CompressionMode.Compress, true))
                    {
                        memoryStream.Position = 0;

                        await memoryStream.CopyToAsync(gzipStream).ConfigureAwait(false);
                    }

                    return Convert.ToBase64String(outputStream.ToArray());
                }
            }
        }
    }
}
