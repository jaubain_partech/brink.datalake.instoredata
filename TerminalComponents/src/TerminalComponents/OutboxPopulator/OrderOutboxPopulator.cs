﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class OrderOutboxPopulator : IOutboxPopulator
    {
        private readonly IOutboxDataEncoder _dataEncoder;
        private readonly IInStoreDataProvider _dataProvider;
        private readonly ILogger<OrderOutboxPopulator> _logger;
        private readonly IOutboxRepository _repository;

        public TimeSpan PopulationInterval { get; }

        public OrderOutboxPopulator(IOptions<OrderOutboxPopulatorOptions> options,
            IInStoreDataProvider dataProvider, IOutboxRepository repository, IOutboxDataEncoder dataEncoder,
            ILogger<OrderOutboxPopulator> logger)
        {
            if (options?.Value == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            PopulationInterval = options.Value.PopulationInterval;

            _dataProvider = dataProvider ?? throw new ArgumentNullException(nameof(dataProvider));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _dataEncoder = dataEncoder ?? throw new ArgumentNullException(nameof(dataEncoder));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> PopulateOutboxAsync()
        {
            var snapshotOptions = new InStoreDataSnapshotOptions
            {
                DataTypes = InStoreDataTypes.Orders
            };

            try
            {
                var mostRecentRecord = await _repository.GetMostRecentRecordAsync(OutboxDataType.Orders);

                if (mostRecentRecord != null)
                {
                    snapshotOptions.OrdersLastModifiedTime = mostRecentRecord.CreatedTime;
                }

                var snapshot = _dataProvider.GenerateSnapshot(snapshotOptions);

                if (snapshot.Orders.Count == 0)
                {
                    _logger.LogInformation("No orders to save.");

                    return true;
                }

                var outboxData = new Dictionary<string, object>
                {
                    ["orders"] = snapshot.Orders
                };

                var createdTime = snapshot.Orders.Max(i => i.ModifiedTime);

                _logger.LogInformation($"Saving {snapshot.Orders.Count} orders to outbox.");

                var encodedData = await _dataEncoder.EncodeDataAsync(outboxData).ConfigureAwait(false);

                var record = new OutboxRecord(snapshot.BusinessDate, OutboxDataType.Orders, encodedData, createdTime);

                await _repository.InsertRecordAsync(record).ConfigureAwait(false);

                _logger.LogInformation("Orders saved to outbox successfully.");

                return true;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Error saving orders to outbox.");

                return false;
            }
        }
    }
}
