﻿using System;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IOutboxPopulator
    {
        TimeSpan PopulationInterval { get; }

        Task<bool> PopulateOutboxAsync();
    }
}
