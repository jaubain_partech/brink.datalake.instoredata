﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class OrderOutboxPopulatorOptions : PopulatorOptions
    {
        public OrderOutboxPopulatorOptions()
        {
            PopulationInterval = TimeSpan.FromMinutes(1);
        }
    }
}
