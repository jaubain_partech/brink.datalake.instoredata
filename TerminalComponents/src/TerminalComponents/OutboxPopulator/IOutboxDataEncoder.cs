﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IOutboxDataEncoder
    {
        Task<string> EncodeDataAsync(object data);
    }
}
