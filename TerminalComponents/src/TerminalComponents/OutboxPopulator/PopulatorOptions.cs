﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class PopulatorOptions
    {
        public TimeSpan PopulationInterval { get; set; }
    }
}
