﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class DataLakeRecordSenderFactory : IDataLakeRecordSenderFactory
    {
        private const int NineHundredFiftyKilobytes = 972_800;

        private readonly IServiceProvider _serviceProvider;

        public DataLakeRecordSenderFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public IDataLakeRecordSender CreateSender(DataLakeRecord record)
        {
            if (record.Value.Length > NineHundredFiftyKilobytes)
            {
                return _serviceProvider.GetRequiredService<S3DataLakeRecordSender>();
            }

            return _serviceProvider.GetRequiredService<FirehoseDataLakeRecordSender>();
        }
    }
}
