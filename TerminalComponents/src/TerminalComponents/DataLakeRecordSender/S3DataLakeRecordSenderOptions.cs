﻿namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class S3DataLakeRecordSenderOptions
    {
        public string BucketName { get; set; }

        public string KeyPrefix { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(BucketName))
            {
                throw new InStoreDataException($"A valid bucket name must be specified.");
            }
        }
    }
}
