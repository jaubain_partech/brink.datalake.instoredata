﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class DataLakeRecord
    {
        public OutboxRecord Source { get; }

        public byte[] Value { get; }

        public DataLakeRecord(OutboxRecord source, byte[] value)
        {
            Source = source;
            Value = value;
        }
    }
}
