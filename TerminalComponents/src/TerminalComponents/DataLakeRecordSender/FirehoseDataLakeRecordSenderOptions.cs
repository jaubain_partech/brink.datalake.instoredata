﻿namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class FirehoseDataLakeRecordSenderOptions
    {
        public string DeliveryStreamName { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(DeliveryStreamName))
            {
                throw new InStoreDataException($"A valid delivery stream name must be specified.");
            }
        }
    }
}
