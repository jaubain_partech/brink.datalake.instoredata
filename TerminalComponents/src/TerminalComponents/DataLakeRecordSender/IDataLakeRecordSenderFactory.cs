﻿namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IDataLakeRecordSenderFactory
    {
        IDataLakeRecordSender CreateSender(DataLakeRecord record);
    }
}
