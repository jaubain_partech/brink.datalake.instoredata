﻿using Amazon.KinesisFirehose;
using Amazon.KinesisFirehose.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class FirehoseDataLakeRecordSender : IDataLakeRecordSender
    {
        private readonly IAmazonKinesisFirehose _firehose;
        private readonly ILogger<FirehoseDataLakeRecordSender> _logger;
        private readonly FirehoseDataLakeRecordSenderOptions _options;

        public FirehoseDataLakeRecordSender(IOptions<FirehoseDataLakeRecordSenderOptions> options,
            IAmazonKinesisFirehose firehose, ILogger<FirehoseDataLakeRecordSender> logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _firehose = firehose ?? throw new ArgumentNullException(nameof(firehose));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task SendRecordAsync(DataLakeRecord record)
        {
            if (record?.Value == null)
            {
                throw new ArgumentNullException(nameof(record.Value));
            }

            _logger.LogDebug("Putting record {RecordId}.", record.Source.Id);

            var request = new PutRecordRequest
            {
                DeliveryStreamName = _options.DeliveryStreamName,
                Record = new Record { Data = new MemoryStream(record.Value) }
            };

            await _firehose.PutRecordAsync(request);

            _logger.LogDebug("Put record {RecordId} successfully.", record.Source.Id);
        }
    }
}

