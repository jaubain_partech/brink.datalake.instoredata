﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class S3DataLakeRecordSender : IDataLakeRecordSender
    {
        private readonly ILogger<S3DataLakeRecordSender> _logger;
        private readonly S3DataLakeRecordSenderOptions _options;
        private readonly IAmazonS3 _s3;
        private readonly ISystemClock _systemClock;

        public S3DataLakeRecordSender(IOptions<S3DataLakeRecordSenderOptions> options, IAmazonS3 s3,
            ISystemClock systemClock, ILogger<S3DataLakeRecordSender> logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _s3 = s3 ?? throw new ArgumentNullException(nameof(s3));
            _systemClock = systemClock ?? throw new ArgumentNullException(nameof(systemClock));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task SendRecordAsync(DataLakeRecord record)
        {
            if (record?.Value == null)
            {
                throw new ArgumentNullException(nameof(record.Value));
            }

            _logger.LogDebug("Putting record {RecordId}.", record.Source.Id);

            var now = _systemClock.UtcNow;

            var key = $"tier1/{now:yyyy/MM/dd/HH}/{nameof(S3DataLakeRecordSender)}-{now:yyyy-MM-dd-HH-mm-ss}-{Guid.NewGuid():N}";

            var request = new PutObjectRequest
            {
                BucketName = _options.BucketName,
                Key = key,
                InputStream = new MemoryStream(record.Value)
            };

            await _s3.PutObjectAsync(request);

            _logger.LogDebug("Put record {RecordId} successfully.", record.Source.Id);
        }
    }
}
