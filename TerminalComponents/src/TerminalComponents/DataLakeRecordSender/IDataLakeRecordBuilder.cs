﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IDataLakeRecordBuilder
    {
        DataLakeRecord BuildRecord(OutboxRecord record); 
    }
}
