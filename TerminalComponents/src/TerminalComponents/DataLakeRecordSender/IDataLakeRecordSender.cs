﻿using System.Threading.Tasks;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface IDataLakeRecordSender
    {
        Task SendRecordAsync(DataLakeRecord record);
    }
}
