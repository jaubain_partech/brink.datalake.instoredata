﻿using Brink.DataLake.InStoreData.TerminalComponents.Persistence;
using System;
using System.Text;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public class DataLakeRecordBuilder : IDataLakeRecordBuilder
    {
        private readonly ILocationIdProvider _locationIdProvider;

        public DataLakeRecordBuilder(ILocationIdProvider locationIdProvider)
        {
            _locationIdProvider = locationIdProvider ?? throw new ArgumentNullException(nameof(locationIdProvider));
        }

        public DataLakeRecord BuildRecord(OutboxRecord record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            var recordPrefix = $"{_locationIdProvider.LocationId:N},{record.BusinessDate:yyyy-MM-dd},"
                + $"{record.CreatedTime:O},";

            var value = Encoding.UTF8.GetBytes(recordPrefix + record.Data + '\n');

            return new DataLakeRecord(record, value);
        }
    }
}
