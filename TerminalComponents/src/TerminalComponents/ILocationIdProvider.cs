﻿using System;

namespace Brink.DataLake.InStoreData.TerminalComponents
{
    public interface ILocationIdProvider
    {
        Guid LocationId { get; }
    }
}
