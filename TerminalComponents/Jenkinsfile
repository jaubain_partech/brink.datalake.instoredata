pipeline {
    agent { label 'BRINKBLD' }
    environment {
        buildRootDirectory = './TerminalComponents'
    }
    options {
        disableConcurrentBuilds()
    }
    parameters {
        booleanParam(name: 'COMMIT_CHANGES', defaultValue: true, description: 'Specifies whether changes made due to versioning or package updates should be committed to master.')
        booleanParam(name: 'PUSH_PACKAGE', defaultValue: true, description: 'Specifies whether the resultant NuGet package should be pushed to the NuGet server.')
        booleanParam(name: 'UPDATE_PACKAGES', defaultValue: true, description: 'Specifies whether Brink NuGet package references should be updated before building.')
    }
    triggers {
        upstream(upstreamProjects: 'Contracts', threshold: hudson.model.Result.SUCCESS)
    }
    stages {
        stage('Pull') {
            steps {
                deleteDirSafe()
                checkoutAndCacheCredentials()
            }
        }

        stage('Update Packages') {
            when {
                expression { return params.UPDATE_PACKAGES }
            }
            steps {
                dotnetOutdated()
            }
        }

        stage('Build') {
            steps {
                dotnetBuildSln()
            }
        }

        stage('Test') {
            steps {
                dotnetRunTests([], true, true)
            }
        }

        /*stage('Push Package') {
            when {
                expression { return params.PUSH_PACKAGE }
            }
            steps {
                nugetPushPackages()
            }
        }*/

        stage('Commit Changes') {
            when {
                expression { return params.COMMIT_CHANGES }
            }
            steps {
                pushVersionChanges()
            }
        }
    }
}
