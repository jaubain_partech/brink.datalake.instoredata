﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class CashDrawer
    {
        public decimal CashInDrawer { get; set; }

        public int Id { get; set; }

        public int? TillNumber { get; set; }
    }
}
