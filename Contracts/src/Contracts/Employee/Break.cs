﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class Break
    {
        public DateTimeOffset? EndTime { get; set; }

        public bool IsPaid { get; set; }

        public byte Number { get; set; }

        public DateTimeOffset StartTime { get; set; }

        public int TypeId { get; set; }
    }
}
