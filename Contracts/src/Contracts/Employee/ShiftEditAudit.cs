﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class ShiftEditAudit
    {
        public int EditedByEmployeeId { get; set; }

        public Guid? EditedByUserId { get; set; }

        public DateTimeOffset EditTime { get; set; }

        public string Field { get; set; }

        public string NewValue { get; set; }

        public string OriginalValue { get; set; }

        public string Reason { get; set; }
    }
}
