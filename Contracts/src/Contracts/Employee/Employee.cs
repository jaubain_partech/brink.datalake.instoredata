﻿using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class Employee
    {
        public Guid? CurrentShiftId { get; set; }

        public int Id { get; set; }

        public bool IsClockedIn { get; set; }

        public bool IsOnBreak { get; set; }

        public List<Shift> Shifts { get; set; } = new List<Shift>();

        public int? TillNumber { get; set; }
    }
}
