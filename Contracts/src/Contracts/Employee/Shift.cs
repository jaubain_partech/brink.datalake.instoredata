﻿using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class Shift
    {
        public List<Break> Breaks { get; set; } = new List<Break>();

        public DateTime BusinessDate { get; set; }

        public DateTime? ClockOutBusinessDate { get; set; }

        public decimal DeclaredTips { get; set; }

        public List<ShiftEditAudit> EditAudits { get; set; } = new List<ShiftEditAudit>();

        public DateTimeOffset? EndTime { get; set; }

        public Guid Id { get; set; }

        public int JobId { get; set; }

        public DateTimeOffset ModifiedTime { get; set; }

        public byte Number { get; set; }

        public Guid? PayPeriodId { get; set; }

        public DateTimeOffset StartTime { get; set; }
    }
}
