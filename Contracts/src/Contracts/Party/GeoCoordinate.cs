﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class GeoCoordinate
    {
        public int Id { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
