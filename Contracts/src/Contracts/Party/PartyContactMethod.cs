﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum PartyContactMethod
    {
        None,
        Pager,
        Sms
    }
}
