﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class Party
    {
        public DateTimeOffset? ArrivalTime { get; set; }

        public string ContactData { get; set; }

        public DateTimeOffset? ContactedTime { get; set; }

        public PartyContactMethod ContactMethod { get; set; }

        public GeoCoordinate Coordinate { get; set; }

        public DateTimeOffset CreatedTime { get; set; }

        public DateTimeOffset? DepartureTime { get; set; }

        public int FromEmployeeId { get; set; }

        public long Id { get; set; }

        public int LaneId { get; set; }

        public int LaneNumber { get; set; }

        public short MinutesEstimated { get; set; }

        public short MinutesQuoted { get; set; }

        public string Name { get; set; }

        public string Notes { get; set; }

        public short OriginalMinutesQuoted { get; set; }

        public int OwnerEmployeeId { get; set; }

        public DateTimeOffset? SeatedTime { get; set; }

        public decimal Size { get; set; }

        public int TableId { get; set; }

        public int ToEmployeeId { get; set; }
    }
}