﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class Scalars
    {
        public DateTime BusinessDate { get; }

        public bool IsOnlineOrderingEnabled { get; set; }
    }
}
