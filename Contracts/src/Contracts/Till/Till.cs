﻿using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class Till
    {
        public DateTimeOffset AssignedTime { get; set; }

        public int? CashDrawerId { get; set; }

        public int? CashTenderId { get; set; }

        public int? CheckoutRequestedByEmployeeId { get; set; }

        public DateTimeOffset? CheckoutTime { get; set; }

        public decimal DeclaredCash { get; set; }

        public bool IsCheckedOut { get; set; }

        public bool IsPublic { get; set; }

        public bool IsVirtual { get; set; }

        public int NoSaleCount { get; set; }

        public int Number { get; set; }

        public decimal OverShort { get; set; }

        public int? OwnerEmployeeId { get; set; }

        public decimal StartingBank { get; set; }

        public List<TillTransactionAudit> TransactionAudits { get; set; } = new List<TillTransactionAudit>();
    }
}
