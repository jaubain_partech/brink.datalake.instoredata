﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class TillTransactionAudit
    {
        public decimal Amount { get; set; }

        public string Description { get; set; }

        public int EmployeeId { get; set; }

        public int Number { get; set; }

        public int PettyAccountId { get; set; }

        public PettyAccountType PettyAccountType { get; set; }

        public DateTimeOffset TransactionTime { get; set; }
    }
}
