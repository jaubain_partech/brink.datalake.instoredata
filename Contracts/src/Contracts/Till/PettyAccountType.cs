﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum PettyAccountType
    {
        CashIn,
        CashOut
    }
}
