﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderDeliveryDetail
    {
        public CustomerAddress Address { get; set; }
    }
}
