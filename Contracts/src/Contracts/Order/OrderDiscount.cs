﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderDiscount
    {
        public decimal Amount { get; set; }

        public int? ApproverEmployeeId { get; set; }

        public Guid? CustomerId { get; set; }

        public int DiscountId { get; set; }

        public int EmployeeId { get; set; }

        public string ExternalLoyaltyAccount { get; set; }

        public int? ForEmployeeId { get; set; }

        public int Id { get; set; }

        public int? LoyaltyAwardId { get; set; }

        public string Name { get; set; }

        public OrderDiscountSource Source { get; set; }
    }
}
