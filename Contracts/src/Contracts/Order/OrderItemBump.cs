﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItemBump
    {
        public DateTimeOffset BumpTime { get; set; }

        public DateTimeOffset? FireTime { get; set; }

        public long Id { get; set; }

        public int KitchenQueueId { get; set; }

        public int OrderItemId { get; set; }

        public int SendNumber { get; set; }

        public DateTimeOffset? SendTime { get; set; }
    }
}
