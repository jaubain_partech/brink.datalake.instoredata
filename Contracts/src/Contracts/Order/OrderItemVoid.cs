﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItemVoid
    {
        public int? ApproverEmployeeId { get; set; }

        public int EmployeeId { get; set; }

        public int VoidReasonId { get; set; }

        public DateTimeOffset VoidTime { get; set; }
    }
}
