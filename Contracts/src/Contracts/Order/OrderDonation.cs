﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderDonation
    {
        public decimal Amount { get; set; }

        public int? ApproverEmployeeId { get; set; }

        public int CharityId { get; set; }

        public int EmployeeId { get; set; }

        public int Id { get; set; }

        public bool IsSystemApplied { get; set; }

        public string Name { get; set; }
    }
}
