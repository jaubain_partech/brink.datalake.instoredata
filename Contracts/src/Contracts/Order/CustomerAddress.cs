﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class CustomerAddress
    {
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string Company { get; set; }

        public string Country { get; set; }

        public int Id { get; set; }

        public string Notes { get; set; }

        public string PostalCode { get; set; }

        public string State { get; set; }

        public CustomerAddressType Type { get; set; }
    }
}
