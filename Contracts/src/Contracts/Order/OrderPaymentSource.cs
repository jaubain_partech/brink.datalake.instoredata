﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum OrderPaymentSource
    {
        Default,
        PaymentDevice
    }
}
