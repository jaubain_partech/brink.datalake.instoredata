﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItemDonation
    {
        public decimal Amount { get; set; }

        public int Id { get; set; }

        public int OrderDonationId { get; set; }
    }
}
