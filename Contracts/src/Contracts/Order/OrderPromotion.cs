﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderPromotion
    {
        public decimal Amount { get; set; }

        public int? ApproverEmployeeId { get; set; }

        public Guid? CustomerId { get; set; }

        public int? EmployeeId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public int PromotionId { get; set; }
    }
}
