﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderBump
    {
        public DateTimeOffset BumpTime { get; set; }

        public long Id { get; set; }

        public int KitchenQueueId { get; set; }

        public int SendNumber { get; set; }

        public DateTimeOffset SendTime { get; set; }
    }
}
