﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderSurchargeTax
    {
        public decimal Amount { get; set; }

        public int Id { get; set; }

        public int TaxId { get; set; }
    }
}
