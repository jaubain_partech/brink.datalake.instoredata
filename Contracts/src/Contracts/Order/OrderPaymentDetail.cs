﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderPaymentDetail
    {
        public string AcquirerReferenceData { get; set; }

        public string AdjustmentType { get; set; }

        public decimal Amount { get; set; }

        public string AuthorizationCode { get; set; }

        public string CardToken { get; set; }

        public int EmployeeId { get; set; }

        public int Id { get; set; }

        public DateTimeOffset PaymentTime { get; set; }

        public int ProcessTimeMilliseconds { get; set; }

        public string ReferenceNumber { get; set; }

        public int TillNumber { get; set; }

        public decimal TipAmount { get; set; }

        public int? TipPaidOutTillNumber { get; set; }
    }
}
