﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class NameValuePair
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
