﻿using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderPayment
    {
        public List<NameValuePair> AdditionalData { get; set; } = new List<NameValuePair>();

        public DateTime BusinessDate { get; set; }

        public string CardHolderName { get; set; }

        public string CardNumber { get; set; }

        public OrderPaymentCardType? CardType { get; set; }

        public string CheckNumber { get; set; }

        public List<OrderPaymentDetail> Details { get; set; } = new List<OrderPaymentDetail>();

        public int? HouseAccountId { get; set; }

        public int Id { get; set; }

        public OrderPaymentMerchantAccountType? MerchantAccountType { get; set; }

        public OrderPaymentSource Source { get; set; }

        public int TenderId { get; set; }

        public bool? WasCardPresent { get; set; }
    }
}
