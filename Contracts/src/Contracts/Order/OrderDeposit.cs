﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderDeposit
    {
        public decimal Amount { get; set; }

        public DateTime BusinessDateAccepted { get; set; }

        public int EmployeeId { get; set; }

        public int Id { get; set; }

        public int? OriginalOrderDepositId { get; set; }

        public OrderPayment Payment { get; set; }

        public string State { get; set; }

        public int TenderId { get; set; }
    }
}
