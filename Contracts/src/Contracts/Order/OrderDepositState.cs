﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    [Flags]
    public enum OrderDepositState
    {
        Default = 0,
        Applied = 1,
        Deleted = 2,
        Surrendered = 4,
    }
}
