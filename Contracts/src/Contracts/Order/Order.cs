﻿using System;
using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class Order
    {
        public List<NameValuePair> AdditionalData { get; set; } = new List<NameValuePair>();

        public decimal Balance { get; set; }

        public List<OrderBump> Bumps { get; set; } = new List<OrderBump>();

        public int? ClosedByEmployeeId { get; set; }

        public DateTimeOffset? CloseTime { get; set; }

        public decimal CoverCount { get; set; }

        public Guid? CustomerId { get; set; }

        public OrderDeliveryDetail Delivery { get; set; }

        public List<OrderDeposit> Deposits { get; set; } = new List<OrderDeposit>();

        public int DestinationId { get; set; }

        public List<OrderDiscount> Discounts { get; set; } = new List<OrderDiscount>();

        public List<OrderDonation> Donations { get; set; } = new List<OrderDonation>();

        public DateTimeOffset? FirstSendTime { get; set; }

        public FutureOrderDetail FutureOrder { get; set; }

        public decimal GrossSales { get; set; }

        public bool HasTaxFreeItems { get; set; }

        public long Id { get; set; }

        public bool IsClosed { get; set; }

        public bool IsCustomerAssigned { get; set; }

        public bool IsFutureOrder { get; set; }

        public bool IsRefund { get; set; }

        public bool IsSent { get; set; }

        public bool IsTaxExempt { get; set; }

        public bool IsTraining { get; set; }

        public List<OrderItem> Items { get; set; } = new List<OrderItem>();

        public int LaneId { get; set; }

        public DateTimeOffset? LastSendTime { get; set; }

        public List<int> MarketingCampaignIds { get; set; } = new List<int>();

        public DateTimeOffset ModifiedTime { get; set; }

        public string Name { get; set; }

        public int Number { get; set; }

        public DateTimeOffset OpenTime { get; set; }

        public decimal Overpayment { get; set; }

        public int OwnerEmployeeId { get; set; }

        public long PartyId { get; set; }

        public List<OrderPayment> Payments { get; set; } = new List<OrderPayment>();

        public List<OrderPromotion> Promotions { get; set; } = new List<OrderPromotion>();

        public int? RefundReasonId { get; set; }

        public decimal Rounding { get; set; }

        public int? SectionId { get; set; }

        public Guid ShiftId { get; set; }

        public string Source { get; set; }

        public decimal SubTotal { get; set; }

        public List<OrderSurcharge> Surcharges { get; set; } = new List<OrderSurcharge>();

        public decimal Tax { get; set; }

        public string TaxExemptId { get; set; }

        public int TerminalId { get; set; }

        public int TillNumber { get; set; }

        public decimal Total { get; set; }
    }
}
