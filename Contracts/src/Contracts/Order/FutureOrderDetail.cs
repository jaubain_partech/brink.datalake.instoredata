﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class FutureOrderDetail
    {
        public DateTimeOffset? CancelledTime { get; set; }

        public DateTime CreatedBusinessDate { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerPhone { get; set; }

        public DateTimeOffset PickupTime { get; set; }

        public bool SendReminderEmail { get; set; }
    }
}
