﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum CustomerAddressType
    {
        Business,
        Home,
        Other
    }
}
