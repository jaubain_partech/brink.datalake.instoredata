﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItemDiscount
    {
        public decimal Amount { get; set; }

        public int Id { get; set; }

        public int OrderDiscountId { get; set; }
    }
}
