﻿using System;

namespace Brink.DataLake.InStoreData.Contracts
{
    [Flags]
    public enum OrderItemState
    {
        Default = 0,
        Sent = 1,
        Cleared = 2,
        Deleted = 4,
        Voided = 8,
        Held = 16,
        Released = 32,
        Selected = 512,
        Refunded = 1024,
        VoidPrinted = 2048,
        Modifier = 4096,
    }
}