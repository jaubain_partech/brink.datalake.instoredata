﻿using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderSurcharge
    {
        public decimal Amount { get; set; }

        public int? ApproverEmployeeId { get; set; }

        public int? EmployeeId { get; set; }

        public int Id { get; set; }

        public bool IsSystemApplied { get; set; }

        public string Name { get; set; }

        public int SurchargeId { get; set; }

        public List<OrderSurchargeTax> Taxes { get; set; } = new List<OrderSurchargeTax>();
    }
}
