﻿using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItem : OrderItemBase
    {
        public string AuthCode { get; set; }

        public List<NameValuePair> AdditionalData { get; set; } = new List<NameValuePair>();

        public List<OrderItemBump> Bumps { get; set; } = new List<OrderItemBump>();
        public int? CompositeComponentId { get; set; }

        public int? CompositeOrderItemId { get; set; }

        public int DayPartId { get; set; }

        public byte Denominator { get; set; }

        public int DestinationId { get; set; }

        public List<OrderItemDiscount> Discounts { get; set; } = new List<OrderItemDiscount>();

        public List<OrderItemDonation> Donations { get; set; } = new List<OrderItemDonation>();

        public GiftCard GiftCard { get; set; }

        public bool IsCleared { get; set; }

        public bool IsVoided { get; set; }

        public int? MenuItemId { get; set; }

        public string Note { get; set; }

        public byte Person { get; set; }

        public int Quantity { get; set; }

        public int RevenueCenterId { get; set; }

        public short? SplitItemId { get; set; }

        public OrderItemState State { get; set; }

        public decimal Units { get; set; }

        public OrderItemVoid Void { get; set; }
    }
}
