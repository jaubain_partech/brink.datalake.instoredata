﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItemModifier : OrderItemBase
    {
        public bool IsExceptionModifier { get; set; }

        public int ModifierCodeId { get; set; }

        public int ModifierGroupId { get; set; }

        public int ParentItemId { get; set; }
    }
}
