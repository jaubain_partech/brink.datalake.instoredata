﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum OrderPaymentMerchantAccountType
    {
        Standard,
        ECommerce
    }
}
