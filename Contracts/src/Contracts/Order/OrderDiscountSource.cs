﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum OrderDiscountSource
    {
        Other,
        LoyaltyCard,
        LoyaltyAward
    }
}
