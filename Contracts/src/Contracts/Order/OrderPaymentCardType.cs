﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum OrderPaymentCardType
    {
        None,
        MasterCard,
        Visa,
        AmericanExpress,
        Diners,
        Discover,
        enRoute,
        JCB,
        Private,

        // Is this new?
        Standard
    }
}
