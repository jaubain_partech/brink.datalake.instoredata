﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItemTax
    {
        public decimal Amount { get; set; }

        public int Id { get; set; }

        public bool IsInclusive { get; set; }

        public int TaxId { get; set; }
    }
}
