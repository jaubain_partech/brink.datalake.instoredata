﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public enum OrderItemType
    {
        Item,
        ModifierItem,
        GiftCardItem
    }
}
