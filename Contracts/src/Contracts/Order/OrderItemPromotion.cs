﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class OrderItemPromotion
    {
        public decimal Amount { get; set; }

        public int Id { get; set; }

        public int OrderPromotionId { get; set; }
    }
}
