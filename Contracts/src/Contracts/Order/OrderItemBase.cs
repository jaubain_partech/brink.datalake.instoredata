﻿using System.Collections.Generic;

namespace Brink.DataLake.InStoreData.Contracts
{
    public abstract class OrderItemBase
    {
        public string Description { get; set; }

        public int Id { get; set; }

        public bool IsNonRevenueItem { get; set; }

        public bool IsTaxFree { get; set; }

        public int ItemId { get; set; }

        public List<OrderItemModifier> Modifiers { get; set; } = new List<OrderItemModifier>();

        public decimal Price { get; set; }

        public List<OrderItemPromotion> Promotions { get; set; } = new List<OrderItemPromotion>();

        public List<OrderItemTax> Taxes { get; set; } = new List<OrderItemTax>();

        public OrderItemType Type { get; set; }
    }
}
