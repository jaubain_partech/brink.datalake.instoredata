﻿namespace Brink.DataLake.InStoreData.Contracts
{
    public class Item
    {
        public int Id { get; set; }

        public bool IsUnavailable { get; set; }

        public int? QuantityAvailable { get; set; }

        public int QuantitySold { get; set; }
    }
}
